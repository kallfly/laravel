@extends('layouts.master')

@section('content')
<div class="container-fluid">
	
    {{-- @ if($leadCount->count()>0) --}}
	@if(true)
    <div class="row">      
	    <div class="col-md-12">
	    	<form action="/list/{{ $listsData->list_id }}/reset" method="post">
	    		<button id="btn-reset-leads" type="submit" class="btn btn-primary pull-right">Reset to Call Leads Again</button>
	    		{{ csrf_field() }}
	    	</form>
	    </div>
	</div>
	@endif



    <div class="row">      
	    
	    <div class="col-md-7">

			@if(Session::has('flash_message_list'))
			<div class="alert alert-success">
			    <div class="container-fluid">
			        <div class="alert-icon">
			            <i class="material-icons">check</i>
			        </div>
			        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			            <span aria-hidden="true"><i class="material-icons">clear</i></span>
			        </button>
			        <b>Success:</b> {!! session('flash_message_list') !!}
			    </div>
			</div>
			@endif

	        @include('list.form')

	    </div>


	    <div class="col-md-5">
            {{-- @if($leadCount->count()>0) --}}
			@if(true)
	        	@include('list.list-details')
	        @else
	        	<div class="alert alert-warning">
	        		<h3>Oops! No Leads Found.</h3>
	        		No Leads found at the moment. Try again later.
	        	</div>
			@endif
	  	</div>

	</div>
</div>
@endsection





@section('vue_templates')
<template id="lead-list">
    <button class="btn btn-info btn-xs">@{{ lead.status }} - @{{ lead.count }}</button>
</template>
@endsection





@section('custom_scripts')
<script>
$(function(){
	$('#btn-reset-leads').click(function(e){
        e.preventDefault();

        var form = $(this).parent();


        swal({
          title: "Reset Leads?",
          text: "Are you sure you want to reset this list?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then(function(willDelete){

            if(!willDelete) return

            form.trigger('submit');

        });
    });
});





new Vue({
    el: '.main-panel',
    data: {
        retrievingList: false,
        leadList: []
    },





    components: {
        'lead-list': {
            template: '#lead-list',
            props: ['lead']
        }
    },





    mounted(){
        this.retrievingList = true;
        $.getJSON('/list/{{ $listsData->list_id }}/ajax/lead-list', function(data){
            this.retrievingList = false;
            this.leadList = data;

            pieChartData = [];
            data.forEach(function(element, index){
                pieChartData.push({ name: element.status, y: parseInt(element.count) });
            })


            Highcharts.chart('lead-summary-graph', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Lead Disposition Summary of List ID 914001'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Count',
                    colorByPoint: true,
                    data: pieChartData
                }],
                credits: {
                  enabled: false
                }
            });
        }.bind(this));
    }
});
</script>
@endsection