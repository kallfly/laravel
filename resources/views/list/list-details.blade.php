<div class="card">
  <div class="card-header" data-background-color="blue">
      <h4 class="title">Lead Disposition</h4>
      <p class="category">Summary of List ID {{ $listsData->list_id }}</p>
  </div>

  <div class="card-content table-responsive">
    <a href="/list/{{ $listsData->list_id }}/download-lead-summary" class="pull-right text-info"><i class="fa fa-download"></i> Download List</a>
    <div class="clearfix"></div>

    <lead-list v-for="lead in leadList" :lead="lead"></lead-list>

    <div class="text-center" v-show="retrievingList">
      <br><br>
      Retrieving Leads
      <i class="fa fa-spinner fa-spin"></i>
    </div>
    
    <div id="lead-summary-graph" class="ct-chart" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
  </div>
</div>