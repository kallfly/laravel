<div class="card">
    <div class="card-header" data-background-color="blue">
        <h4 class="title">Lists</h4>
        <p class="category">List</p>
    </div>

    <div class="card-content table-responsive">
        <table class="table datatable">
            <thead class="text-danger">
                <tr>
                    <th>List ID</th>
                    <th>List Name</th>
                    <th>Leads Count</th>
                    <th>Campaign</th>
                    <th>Active</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
               <tr 
                is="row-list" 
                v-for="listItem in lists"
                :list_item="listItem"></tr>
            </tbody>
        </table>
    </div>
</div>