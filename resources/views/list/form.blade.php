<div class="card">
    <div class="card-header" data-background-color="blue">
        <h4 class="title">List</h4>
        <p class="category">Complete the list details below</p>
    </div>
    <div class="card-content">
    	@include('layouts.form-errors')
        <form action="{{ $form['action'] }}" method="POST">
            <div class="row">
            	<div class="col-md-12">
                    <div class="form-group label-floating">
                        <label class="control-label">List Name</label>
                        <input 
                            class="form-control" 
                            type="text" 
                            name="list_name"
                            value="{{ $listsData->list_name }}">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">List Descrition</label>
                        <input 
                            class="form-control" 
                            type="text" 
                            name="list_description"
                            value="{{ $listsData->list_description }}">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Campaign</label>
                        <select
                            name="campaign_id"
                            class="form-control">
                            @foreach($campaignLists as $campaign)
                            <option 
                                value="{{ $campaign->campaign_id }}"
                                {{ ($campaign->campaign_id == $listsData->campaign_id) ? 'selected' : '' }}>
                                {{ $campaign->campaign_name }}
                            </option>
                            @endforeach
                            <option>test</option>
                        </select>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Full Name</label>
                        <select
                            name="active"
                            class="form-control">
                            @foreach($activeStatuses as $key => $status)
                            <option
                                value="{{ $key }}"
                                {{ ($key == $listsData->active) ? 'selected' : '' }}>
                                {{ $status }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            @if($editMode)
                <button type="submit" class="btn btn-info pull-right">Update</button>
                <a href="/list" class="btn btn-default pull-right">Cancel</a>
                {!! method_field('patch') !!}
            @else
                <button type="submit" class="btn btn-info pull-right">Add</button>
            @endif
            <div class="clearfix"></div>

            {{ csrf_field() }}
        </form>
    </div>
</div>