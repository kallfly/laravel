@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
    	
		<div class="col-md-8">
    		@include('list.list')
    	</div>
		<div class="col-md-4">
    		@include('list.form')
    	</div>
	</div>
</div>
@endsection





@section('vue_templates')
<template id="row-list">
	<tr>
		<td>
			<a :href="'/list/'+list_item.list_id+'/edit'">
				@{{ list_item.list_id }}
			</a>
		</td>
		<td>
			@{{ list_item.list_name }}
		</td>
		<td v-html="list_item.leads_count"></td>
		<td>
			@{{ list_item.campaign_id }} 
			@{{ list_item.campaign_name }}
		</td>
		<td>
			@{{ list_item.active }}
		</td>
		<td>
			<a :href="'/list/'+list_item.list_id+'/edit'">
				<i class="material-icons text-warning">edit</i>
			</a>
		</td>
	</tr>
</template>
@endsection



@section('custom_scripts')
<script>
new Vue({
	el: '.main-panel',
	data: {
		lists: {!! $lists !!}
	},





	components: {
		'row-list': {
			template: '#row-list',
			props: ['list_item']
		}
	},





	mounted(){
		var self = this;
        this.lists.forEach(function(element, index){
            self.lists[index].leads_count = '<i class="fa fa-spinner fa-spin"></i>';
            $.getJSON('/list/' + element.list_id + '/ajax/total-count', function(listItem){
                self.lists[index].leads_count = listItem.total;
            });
        });
	}
});
</script>
@endsection