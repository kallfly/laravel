<table>
	<thead>
		<tr>
			<th>phone code</th>
			<th>phone number</th>
			<th>vendor lead code</th>
			<th>first name</th>
			<th>last name</th>
			<th>email</th>
			<th>address1</th>
			<th>address2</th>
			<th>address3</th>
			<th>city</th>
			<th>postal code</th>
			<th>state</th>
			<th>comments</th>
			<th>called count</th>
			<th>user</th>
			<th>last local call time</th>
		</tr>
	</thead>
	<tbody>
		@foreach($leads as $lead)
		<tr>
			<td>{{ $lead->phone_code }}</td>
			<td>{{ $lead->phone_number }}</td>
			<td>{{ $lead->vendor_lead_code }}</td>
			<td>{{ $lead->first_name }}</td>
			<td>{{ $lead->last_name }}</td>
			<td>{{ $lead->email }}</td>
			<td>{{ $lead->address1 }}</td>
			<td>{{ $lead->address2 }}</td>
			<td>{{ $lead->address3 }}</td>
			<td>{{ $lead->city }}</td>
			<td>{{ $lead->postal_code }}</td>
			<td>{{ $lead->state }}</td>
			<td>{{ $lead->comments }}</td>
			<td>{{ $lead->called_count }}</td>
			<td>{{ $lead->user }}</td>
			<td>{{ $lead->last_local_call_time }}</td>
		</tr>
		@endforeach
	</tbody>
</table>