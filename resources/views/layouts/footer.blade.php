<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul>
                <li>
                    <a href="#">
                        Dashboard
                    </a>
                </li>
                <li>
                    <a href="#">
                        My Account
                    </a>
                </li>
                <li>
                    <a href="#">
                        Dialer
                    </a>
                </li>
                <li>
                    <a href="#">
                        Invoice
                    </a>
                </li>
            </ul>
        </nav>
        <p class="copyright pull-right">
            &copy; 2018 Kallfly Admin
        </p>
    </div>
</footer>