    <div class="sidebar" data-color="blue" data-image="/img/sidebar.jpg">
    <!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

Tip 2: you can also add an image using data-image tag
-->
    <div class="logo">
        <a href="/" class="simple-text">
            <i class="fa fa-cloud"></i> Kallfly
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{ App\Http\Helpers::set_active(['/', 'home']) }}">
                <a href="/">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>



            <li class="{{ App\Http\Helpers::set_active(['user/admin', 'user/agent']) }}">
                <a data-toggle="collapse" href="#pageUsers" class="collapsed">
                    <i class="material-icons">person</i>
                    <p> Users
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ App\Http\Helpers::set_active(['user/admin', 'user/agent'], 'in') }}" id="pageUsers">
                    <ul class="nav">
                        <li class="{{ App\Http\Helpers::set_active('user/admin') }}">
                            <a href="/user/admin">
                                <span class="sidebar-normal">Admin</span>
                            </a>
                        </li>
                        <li class="{{ App\Http\Helpers::set_active('user/agent') }}">
                            <a href="/user/agent">
                                <span class="sidebar-normal">Agents</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>



            <li class="{{ App\Http\Helpers::set_active(['campaigns']) }}">
                <a href="/campaigns">
                    <i class="fa fa-cogs"></i>
                    <p>Campaigns</p>
                </a>
            </li>



            <li class="{{ App\Http\Helpers::set_active(['list', 'leads/search', 'leads-loader']) }}">
                <a data-toggle="collapse" href="#pageLists" class="collapsed">
                    <i class="fa fa-list"></i>
                    <p> Lists
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ App\Http\Helpers::set_active(['list', 'leads/search', 'leads-loader'], 'in') }}" id="pageLists">
                    <ul class="nav">
                        <li class="{{ App\Http\Helpers::set_active('list') }}">
                            <a href="/list">
                                <span class="sidebar-normal">All Lists</span>
                            </a>
                        </li>
                        <li class="{{ App\Http\Helpers::set_active('leads/search') }}">
                            <a href="/leads/search">
                                <span class="sidebar-normal">Lead Search</span>
                            </a>
                        </li>
                        <li class="{{ App\Http\Helpers::set_active('leads-loader') }}">
                            <a href="/leads-loader">
                                <span class="sidebar-normal">Lead Loader</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>



            <li class="{{ App\Http\Helpers::set_active('reports') }}">
                <a href="/reports">
                    <i class="fa fa-bar-chart"></i>
                    <p>Reports</p>
                </a>
            </li>



            <li class="{{ App\Http\Helpers::set_active('top-up') }}">
                <a href="/top-up">
                    <i class="fa fa-money"></i>
                    <p>Top Up</p>
                </a>
            </li>



            <li>
                <a href="/logout">
                    <i class="material-icons">backspace</i>
                    <p>Logout</p>
                </a>
            </li>
        </ul>
    </div>
</div>