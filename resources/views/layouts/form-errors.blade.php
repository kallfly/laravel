@if($errors->all())
<div class="alert alert-danger">
    <div class="container-fluid">
        <div class="alert-icon">
            <i class="material-icons">error_outline</i>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"><i class="material-icons">clear</i></span>
        </button>
        <ul class="list-unstyled">
            @foreach($errors->all() as $error)
                <ul>{{ $error }}</ul>
            @endforeach
        </ul>
    </div>
</div>
@endif


@if(Session::has('flash_message'))
<div class="alert alert-success">
    <div class="container-fluid">
        <div class="alert-icon">
            <i class="material-icons">check</i>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"><i class="material-icons">clear</i></span>
        </button>
        <b>Success:</b> {!! session('flash_message') !!}
    </div>
</div>
@endif