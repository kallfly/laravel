@extends('layouts.master')


@section('content')
@include('layouts.form-errors')

@if(Session::has('flash_error'))
<div class="alert alert-danger">
    <div class="container-fluid">
        <div class="alert-icon">
            <i class="fa fa-exclamation-triangle"></i>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"><i class="material-icons">clear</i></span>
        </button>
        {{ session('flash_error') }}
    </div>
</div>
@endif

<div class="row">
	<div class="col-md-4">

		<div class="row">
			<div class="col-md-12">
				<h4 style="margin-right: 15px; margin-top: 36px;" class="pull-left">
					<strong>Pay with</strong>
				</h4>
				
				<ul class="nav nav-pills nav-pills-icons nav-pills-info" role="tablist">
					<li class="active">
						<a href="#credit-card" role="tab" data-toggle="tab">
						<i class="fa fa-credit-card"></i>
						Credit Card
					</a>
					</li>
					<li>
						<a href="#paypal" role="tab" data-toggle="tab">
						<i class="fa fa-paypal"></i>
						Paypal
					</a>
					</li>
				</ul>
			</div>
		</div>

		<div id="report-content" class="tab-content">
		   <div id="credit-card" class="tab-pane active">
		      <div class="card">
		         <div class="card-content">
	                <h3><i class="fa fa-credit-card"></i> Credit Card</h3>

	                <form action="/top-up/stripe" method="post">
	                	{{ csrf_field() }}

						<div class="row">
		                    <div class="col-md-12">
		                        <div class="form-group label-floating">
		                            <label class="control-label">Amount</label>
		                            <select name="amount" class="form-control">
		                            	<option>50</option>
		                            	<option>100</option>
		                            	<option>200</option>
		                            	<option>500</option>
		                            </select>
		                        	<span class="material-input"></span>
		                        </div>
		                    </div>
		            	</div>


		            	<div class="row">
		                    <div class="col-md-12">
		                        <div class="form-group label-floating">
		                            <label class="control-label">Card Number</label>
		                            <input name="card" type="text" required maxlength=16 class="form-control">
		                        	<span class="material-input"></span>
		                        </div>
		                    </div>
		            	</div>


		            	<div class="row">
		                    <div class="col-md-12">
		                    	<div class="row">
		                    		<div class="col-sm-8">
		                    			<div class="form-group" style="margin-top:0">
				                            <label class="control-label">Expiration Date</label>
				                            <div class="form-inline">
				                            	<input required name="exp_month" type="number" class="form-control" placeholder="mm" style="width:75px;">
				                            	<input required name="exp_year" type="number" class="form-control" placeholder="yy" style="width:120px;">
				                            </div>
				                        </div>
		                    		</div>

		                    		<div class="col-sm-4">
			                			<div class="form-group label-floating">
				                            <label class="control-label">CV</label>
				                            <input required name="cv" type="text" class="form-control">
				                        </div>
			                		</div>
		                    	</div>
		                    </div>
		            	</div>

						<div class="pull-left" style="margin-top:18px;">+2.5% Transaction Fees</div>
						<button type="submit" class="btn btn-info pull-right">Pay Now</button>
						<div class="clearfix"></div>
					</form>
		         </div>
		      </div>
		   </div>
		   <div id="paypal" class="tab-pane" style="">
		      <div class="card">
		         <div class="card-content">
	                <h3><i class="fa fa-paypal"></i> Paypal</h3>


	                <form action="/top-up/paypal" method="POST">
	                	{{ csrf_field() }}
			            <div class="row">
			               <div class="col-md-12">
		                        <div class="form-group label-floating">
		                            <label class="control-label">Amount</label>
		                            <select name="amount" class="form-control">
		                            	<option>50</option>
		                            	<option>100</option>
		                            	<option>200</option>
		                            	<option>500</option>
		                            </select>
		                        	<span class="material-input"></span>
		                        </div>
		                    </div>
			            </div>

			            <div class="pull-left" style="margin-top:18px;">+2.5% Transaction Fees</div>
						<button type="submit" class="btn btn-info pull-right">Pay Now</button>
						<div class="clearfix"></div>
					</form>
		         </div>
		      </div>
		   </div>
		</div>
	</div>

	<div class="col-md-8">
		<div class="card">
			<div class="card-header" data-background-color="blue">
				<h4 class="title">Top Up History</h4>
				<p class="category">List of top up history</p>
			</div>

			<div class="card-content table-responsive">
				<table id="top-up-table" class="table table-responsive">
					<thead>
						<tr>
							<th class="text-info"><strong>#</strong></th>
							<th class="text-info"><strong>Date</strong></th>
							<th class="text-info"><strong>Invoice #</strong></th>
							<th class="text-info"><strong>Amount</strong></th>
							<th class="text-info"><strong>Method</strong></th>
							<th class="text-info"><strong>Paid</strong></th>
							<th class="text-info"><strong>Action</strong></th>
						</tr>
					</thead>
					<tbody>
						@foreach($topUpList as $index => $item)
						<tr>
							<td>{{ $index + 1 }}</td>
							<td>{{ date('M d, Y h:i a', strtotime($item->date)) }}</td>
							<td>{{ $item->invoice_number }}</td>
							<td>${{ number_format((int)$item->amount + floatval($item->tax), 2) }}</td>
							<td>{{ $item->method == 'stripe' ? 'Credit Card' : ucfirst($item->method) }}</td>
							<td>
								@if($item->paid==1)
									<i class="fa fa-check text-success" aria-hidden="true"></i>
								@else
									<i class="fa fa-times text-danger" aria-hidden="true"></i>
								@endif
							</td>
							<td>
								<a class="text-info view" href="/top-up/print/{{ $item->id }}" style="margin-right:15px;">
									<i class="fa fa-file-text-o" aria-hidden="true"></i> View
								</a>
								<a class="text-info print" href="/top-up/print/{{ $item->id }}">
									<i class="fa fa-print" aria-hidden="true"></i> Print
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<iframe src="" frameborder="0" width="0" height="0" id="print-iframe"></iframe>
@endsection















@section('custom_scripts')
<script>
window.onafterprint = function(){
   console.log("Printing completed...");
}
$(function(){
	$('#top-up-table').on('click', 'a.view', function(e){
		e.preventDefault();
		window.open($(this).attr('href'), 'mywin','left=20,top=20,width=800,height=600,toolbar=1,resizable=0');
	})


	$('#top-up-table').on('click', 'a.print', function(e){
		e.preventDefault();
		$("#print-iframe")
        .attr("src", $(this).attr('href'));
        
        setTimeout(function(){
        	document.getElementById('print-iframe').contentWindow.print(); 
        }, 500);
	});
});




new Vue({
	el: '.main-panel',





	mounted(){
		$('#top-up-table').dataTable();
	}
});
</script>
@endsection