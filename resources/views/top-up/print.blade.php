
<html>
	<head>
		<title>Kallfly Invoice</title>
        <link href="/css/bootstrap.min.css" rel="stylesheet" />
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	</head>
	<body>
	<div class="modal-header">
		<h3 class="pull-right">
			INVOICE 
            @if($invoice->paid==1)
                <strong class="text-success">PAID</strong>
            @else
                <strong class="text-danger">UNPAID</strong>
            @endif
		</h3>
		<img src="/img/logo.png" class="image-responsive" alt="kallfy-logo">
	</div>
  	<div class="modal-body">
        <div class="row">
        	<div class="col-xs-3">
        		From<br>
        		<strong>Kallfly.com</strong><br>
        		https://kallfly.com<br>
        		finance@kallfly.com
        	</div>
        	<div class="col-xs-4">
        		To<br>
        		<strong>{{ $invoice->customer->fullname }}</strong><br>
        		Email: {{ $invoice->customer->email }}        	</div>
        	<div class="col-xs-5">
        		Details<br>
        		<strong>Invoice #<span class="modal-data invoice">{{ $invoice->invoice_number }}</span></strong><br>
        		Account: {{ $invoice->account_id }}</div>
        </div>

        <br><br>

        <table class="table">
        	<thead>
        		<tr>
        			<th>Qty</th>
        			<th>Product</th>
        			<th>Description</th>
        			<th>Method</th>
        			<th>Paid</th>
        			<th>Subtotal</th>
        		</tr>
        	</thead>

        	<tbody>
        		<tr class="active">
        			<td>1</td>
        			<td>VOIP</td>
        			<td>VOIP minutes Top up</td>
        			<td>{{ $method }}</td>
        			<td>{{ $invoice->paid == 1 ? 'Yes' : 'No' }}</td>
        			<td>{{ $amount }}</td>
        		</tr>
        	</tbody>
        </table>

        <br><br>

        <div class="row">
        	<div class="col-xs-6">
        		<h4>
        			Payment Method: 
        			<strong class="modal-data method">{{ $method }}</strong>
        		</h4>

        		<div class="alert alert-info">
        			This serves as the official invoice for VOIP Minutes top up from {{ $invoice->customer->fullname }} to Kallfly.com. Please print this page if a hard copy is needed.
        		</div>
        	</div>
        	<div class="col-xs-6">
        		<h5>Due Date: <strong class="modal-data format-date">{{ $date }}</strong></h5>

				<table class="table">
					<tbody>
						<tr>
							<th style="width:100px;">Subtotal:</th>
							<td><span class="modal-data amount">{{ $amount }}</span></td>
						</tr>
						<tr>
							<th>Tax:</th>
							<td>{{ $tax}}</td>
						</tr>
						<tr>
							<th><h4>Total</h4></th>
							<td><h4><span class="modal-data amount">{{ $total }}</span></h4></td>
						</tr>
					</tbody>
				</table>
        	</div>
        </div>
  	</div>
	</body>
</html>