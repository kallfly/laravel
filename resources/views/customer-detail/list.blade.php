<div class="col-md-8">

    @if(Session::has('flash_delete_message'))
    <div class="alert alert-danger">
        <div class="container-fluid">
            <div class="alert-icon">
                <i class="material-icons">delete</i>
            </div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="material-icons">clear</i></span>
            </button>
            {!! session('flash_delete_message') !!}
        </div>
    </div>
    @endif

    <div class="card">
        <div class="card-header" data-background-color="blue">
            <h4 class="title">Customer List</h4>
            <p class="category">Scheduled by Monthly Cycle</p>
        </div>

        <div class="card-content table-responsive">
            <table class="table">
                <thead class="text-danger">
                    <tr>
                        <th>Custommer ID</th>
                        <th>Custommer Name</th>
                        <th>Monthly Invoice Cycle</th>
                        <th>Allowed Users</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($customers)>0) @foreach($customers as $customer)
                    <tr>
                        <td>{{ $customer->customer_id }}</td>
                        <td>{{ $customer->vicidialUser->full_name }}</td>
                        <td>{{ App\Http\Helpers::ordinal($customer->monthly_invoice_cycle) }}</td>
                        <td>{{ $customer->allowed_users }}</td>
                        <td>
                            @if($customer->status)
                            <i class="fa fa-toggle-on text-success"></i>
                            @else
                            <i class="fa fa-toggle-off text-danger"></i>
                            @endif
                        </td>
                        <td>
                            <a href="/customer-detail/{{ $customer->id }}/edit"><i class="material-icons text-warning">edit</i></a>

                            <form style="display:inline;" action="/customer-detail/{{ $customer->id }}" method="post">
                                {!! method_field('delete') !!}
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-link delete" role="link"><i class="material-icons text-danger" style="font-size:24px;">delete</i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach @else
                    <tr>
                        <td colspan="6" class="text-center"><h5>No Schedules yet.</h5></td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>