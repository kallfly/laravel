@extends('layouts.master')


@section('content')
<div class="container-fluid">
    <div class="row">
    	@include('customer-detail.list')
    	@include('customer-detail.form')
    </div>
</div>
@endsection

@section('custom_scripts')
<script>
$(function(){
	$('#full-name').typeahead({
		ajax: {
			url: '/user/autocomplete',
			triggerLength: 1
		},
		onSelect: function(data){
			$('.customer-id-hidden').val(data.value);
			$('.customer-id').val(data.value).parent().removeClass('is-empty');
		}
	}).on('keyup', function(e){
		if(e.which==13) return;
		
		$('.customer-id-hidden').val('');
		$('.customer-id').val('').parent().addClass('is-empty')
	});

	$('.delete').click(function(e){
		e.preventDefault();
		var form = $(this).parent();
		swal({
	      title: "Are you sure?",
	      text: "Once deleted, you will not be able to recover this record",
	      icon: "warning",
	      buttons: true,
	      dangerMode: true
	    }).then((willDelete) => {
		  if (willDelete) {
		  	form.trigger('submit');
		  }
		});
	})
});
</script>
@endsection>