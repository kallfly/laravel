@extends('layouts.master')

@section('content')
<div class="row">
	<div class="col-md-4">
		<div class="card">
			<div class="card-header" data-background-color="blue">
				<h4 class="title">Calls Detailed Report</h4>
				<p class="category"><i class="fa fa-cloud-download"></i> Download CDR</p>
			</div>
			<div class="card-content">
				<div class="form-group label-floating">
					<label class="control-label">Date</label>
					<input type="text" class="form-control datepicker" v-model="date" />
				</div>
				
				<button class="btn btn-info pull-right" @click.prevent="updateReport">
					View Report
					<i class="fa fa-spinner fa-spin" v-show="retrievingReport"></i>
				</button>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="card">
			<div class="card-header" data-background-color="blue">
				<h4 class="title">Consumption Report</h4>
				<p class="category">Below displays the calls.</p>
			</div>

			<div class="card-content table-responsive">
				<table id="report" class="table table-responsive">
					<thead>
						<tr>
							<th class="text-info"><strong>#</strong></th>
							<th class="text-info"><strong>Call Date</strong></th>
							<th class="text-info"><strong>Caller ID</strong></th>
							<th class="text-info"><strong>Destination</strong></th>
							<th class="text-info"><strong>Duration</strong></th>
							<th class="text-info"><strong>Cost</strong></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection





@section('custom_scripts')
<script>
new Vue({
	el: '.main-panel',
	data: {
		date: '{{ date('Y-m-d') }}',
		table: {},
		url: '/cdr/ajax-report'
	},





	mounted: function(){
		var self = this;
		$('.datepicker').datepicker({
            format:'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function(ev) {
            self.date = $(this).val();
        });


        this.table = $('#report').dataTable({
			"processing": true,
			"language": {
				"processing": '<i class="fa fa-spinner fa-spin"></i> Retreiving Report'
			},
			"dom": 'B<fl>rtpi', // the "r" is for the "processing" message,
			buttons: [
				{ extend: 'csv', text: 'Export to CSV' }
			],
	        "ajax": {
	        	"url":this.url,
	        	"dataSrc":""
	        },
	        "columns": [
		        { "data": "index",
	                "render": function(data,type,row,meta) {
	                    return meta.row + 1;
	                }
	        	},
	            { "data": "formatted_date" },
	            { "data": "src" },
	            { "data": "dst" },
	            { "data": "duration" },
	            { "data": "cost" }
	        ]
		});
	},





	methods: {

		updateReport: function(){

			this.table.api().ajax.url(this.url + '?date='+this.date).load();

		}

	}

});
</script>
@endsection