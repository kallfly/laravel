@extends('layouts.master')

@section('content')
<div class="container-fluid">
    @include('lead.search.form')
    @include('lead.search.leads')
</div>
@endsection





@section('custom_scripts')
<script>
$(function(){
	$('.checkboxes input[type="checkbox"]').change(function(){
		checkbox = $(this);
		target = '.' + checkbox.attr('target');

		$(target).toggle();
		$(target).find('input').val('');
		$(target).find('select').val('none');
	});


	





	var table = $('.table').dataTable({
		"processing": true,
        "ajax":{"url":"/leads/search/ajax","dataSrc":""},
        "columns": [
            { "data": "phone_number",
                "render": function(data,type,row,meta) {
                    var a = '<a href="/leads/'+row.lead_id+'">' + row.phone_number +'</a>';
                    return a;
                }
        	},
            { "data": "lead_id" },
            { "data": "status" },
            { "data": "list_name" },
            { "data": "phone_code" },
            { "data": "first_name" },
            { "data": "last_name" },
            { "data": "user" }
        ]
	});





	$('#form-search').submit(function(e){
		e.preventDefault();

		var queryStr = '/leads/search/ajax?true';
		$('form input[type="text"], form select').each(function(){
			var obj = $(this);
			queryStr += '&' + obj.attr('name') + '=' + obj.val();
		});

		table.api().ajax.url( queryStr ).load();
	})

});
</script>
@endsection