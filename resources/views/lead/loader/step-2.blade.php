<div id="step-2" class="row load-steps" v-show="currentStep==2">
	<div class="col-md-7">
		<div class="card">
			<div class="card-header" data-background-color="blue">
		        <h4 class="title">Load a new list</h4>
		        <p class="category">Processing tab-delimited file...</p>
		    </div>


		    <div class="card-content">
		    	<table class="table table-striped">
		    		<thead>
		    			<tr>
		    				<th class="text-info"><strong>Column</strong></th>
		    				<th class="text-info"><strong>File Data</strong></th>
		    			</tr>
		    		</thead>
		    		<tbody>
			    		<tr
			    			v-for="field in columns">
			    			<td>@{{ field.label }}</td>
			    			<td>
			    				<select v-model="field.value">
									<option value="">(none)</option>
									<option v-for="clm, index in fileColumns" :value="index">@{{ clm }}</option>
								</select>
			    			</td>
			    		</tr>
			    	</tbody>
		    	</table>

		    	<button class="btn btn-info pull-right" @click.prevent="step3">OK TO PROCESS</button>
		    	<div class="clearfix"></div>

			</div><!-- .card-content -->
		</div><!-- .card -->
	</div>
</div><!-- .row -->