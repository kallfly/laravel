@extends('layouts.master')

@section('content')
    @include('lead.loader.step-1')
    @include('lead.loader.step-2')
    @include('lead.loader.step-3')
@endsection












@section('custom_scripts')
<script>


new Vue({
	el: '.main-panel',
	data: {
		// upload data
		uploadStarted: false,
		uploadCompleted: false,
		uploadStatus: 'Uploading',
		fileColumns: [],

		// post data
		filePath: '',
		listIdOverride: 'in_file',
		phoneCodeOverride: 1,
		leadDuplicateCheck: 'NONE',

		currentStep: 1,
		leadLogs: [],
		leadProcessing: false,
		retreivingFileClms: false,


		good: 0,
		bad: 0,


		// default columns
		columns: [
			{
				column_name: 'vendor_lead_code',
				label: 'VENDOR LEAD CODE',
				value: ''
			},
			{
				column_name: 'source_id',
				label: 'SOURCE ID',
				value: ''
			},
			{
				column_name: 'list_id',
				label: 'LIST ID',
				value: ''
			},
			{
				column_name: 'phone_code',
				label: 'PHONE CODE',
				value: ''
			},
			{
				column_name: 'phone_number',
				label: 'PHONE NUMBER',
				value: ''
			},
			{
				column_name: 'title',
				label: 'TITLE',
				value: ''
			},
			{
				column_name: 'first_name',
				label: 'FIRST NAME',
				value: ''
			},
			{
				column_name: 'middle_initial',
				label: 'MIDDLE INITIAL',
				value: ''
			},
			{
				column_name: 'last_name',
				label: 'LAST NAME',
				value: ''
			},
			{
				column_name: 'address1',
				label: 'ADDRESS1',
				value: ''
			},
			{
				column_name: 'address2',
				label: 'ADDRESS2',
				value: ''
			},
			{
				column_name: 'address3',
				label: 'ADDRESS3',
				value: ''
			},
			{
				column_name: 'city',
				label: 'CITY',
				value: ''
			},
			{
				column_name: 'state',
				label: 'STATE',
				value: ''
			},
			{
				column_name: 'province',
				label: 'PROVINCE',
				value: ''
			},
			{
				column_name: 'postal_code',
				label: 'POSTAL CODE',
				value: ''
			},
			{
				column_name: 'country_code',
				label: 'COUNTRY CODE',
				value: ''
			},
			{
				column_name: 'gender',
				label: 'GENDER',
				value: ''
			},
			{
				column_name: 'date_of_birth',
				label: 'DATE OF BIRTH',
				value: ''
			},
			{
				column_name: 'alt_phone',
				label: 'ALT PHONE',
				value: ''
			},
			{
				column_name: 'email',
				label: 'EMAIL',
				value: ''
			},
			{
				column_name: 'security_phrase',
				label: 'SECURITY PHRASE',
				value: ''
			},
			{
				column_name: 'comments',
				label: 'COMMENTS',
				value: ''
			},
			{
				column_name: 'rank',
				label: 'RANK',
				value: ''
			},
			{
				column_name: 'owner',
				label: 'OWNER',
				value: ''
			}
		]
	},
	methods: {

		uploadProgress: function(el, progress){


			if(progress < 0) progress = 0;
			if(progress > 100) progress = 100;
			

			var curr = (progress / 100) * 360;
			el.find(".per-circ-stat").html(Math.round(progress) + "%");

			if(curr <= 180)
			{
				el.css('background-image', 'linear-gradient(' + (90 + curr) + 'deg, transparent 50%, #ccc 50%),linear-gradient(90deg, #ccc 50%, transparent 50%)');
			}
			else
			{
				el.css('background-image', 'linear-gradient(' + (curr - 90) + 'deg, transparent 50%, #00cc00 50%),linear-gradient(90deg, #ccc 50%, transparent 50%)');
			}

		},





		step2: function() {

			var data = { file: this.filePath };

			this.retreivingFileClms = true;
			$.post('/leads-loader/get-file-columns', data, function(data){
				this.retreivingFileClms = false;
				this.fileColumns = data;
				this.currentStep = 2;
			}.bind(this));


		},





		step3: function() {
			
			this.currentStep = 3;

			var data = {
					file: this.filePath,
					columns: this.columns,
					listIdOverride: this.listIdOverride,
					phoneCodeOverride: this.phoneCodeOverride,
					leadDuplicateCheck: this.leadDuplicateCheck
				};

			this.leadProcessing = true;
			this.bad=0;
			this.good=0;
			this.leadLogs=[];
			$.post('/leads-loader/process-leads', data, function(data){
				this.leadProcessing = false;
				// this.leadLogs = data.lead_logs;
			}.bind(this));

		}

	},





	mounted(){

		var self = this;

		$('#lead-upload').fileupload({
			dataType: 'json',
			done: function (e, data) {
				self.uploadCompleted = true;
				self.uploadStatus = 'Completed';
				self.filePath = data.result.path;
			},
			progressall: function (e, data) {
				
				if(!this.uploadStarted)
				{
					this.uploadStarted = true;
					$('#sellPerCirc').show();
				}

		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        self.uploadProgress($('#sellPerCirc'), progress);

		    },
		    fail: function() { 

		    	progressBar = $('#sellPerCirc');
		    	progressBar.after('<div class="alert alert-danger"><div class="container-fluid"><div class="alert-icon"><i class="material-icons">error_outline</i></div><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="material-icons">clear</i></span></button><b>Error Alert:</b> There was an error uploading the file. please refresh the page and try again.</div></div>');
		    	progressBar.remove();

		    }
		});





		Echo.private('leadsprocess.{{ $user->user_id }}')
		.listen('LeadInserted', (e) => {
			this.leadLogs.push(e);
			if(e.status.type=='good') this.good++;
			if(e.status.type=='bad') this.bad++;
		});

	},





	components: {
		'lead-data': {
			template: '#lead-data-component',
			props: ['log', 'index'],
			computed: {
				errType: function(){
					var msg;

					switch(this.log.status.error)
					{
						case 'invalid_list':
							return 'INVALID LIST';
							break;

						case 'duplicate':
							return 'DUPLICATE';
							break;

						case 'invalid_phone':
							return 'INVALID PHONE NUMBER';
							break;

						default:
							return 'ERROR';
							break;
					}

				}
			}
		}
	}

});
</script>
@endsection










@section('vue_templates')
<template id="lead-data-component">
	<li  
		v-if="log.status.type=='bad'"
		class="list-group-item text-danger">
		Record @{{ index + 1 }} BAD- 
		<strong>@{{ errType }}</strong>
		PHONE: @{{ log.status.phone_number }}
		LIST ID: @{{ log.status.list_id }}
	</li>
</template>
@endsection