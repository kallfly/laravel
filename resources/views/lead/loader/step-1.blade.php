<div id="step-1" class="row load-steps" v-show="currentStep==1">
	<div class="col-md-7">
		<div class="card">
			<div class="card-header" data-background-color="blue">
		        <h4 class="title">Load a new list</h4>
		        <p class="category">Load leads</p>
		    </div>


		    <div class="card-content">
		    	<div class="form-group is-empty is-fileinput">
				    <input type="file" id="lead-upload" name="lead_file" data-url="/leads-loader/upload">
				    <div class="input-group">
				      <input type="text" readonly="" class="form-control" placeholder="Load leads from this file">
				        <span class="input-group-btn input-group-sm">
				          <button type="button" class="btn btn-fab btn-fab-mini">
				            <i class="material-icons">attach_file</i>
				          </button>
				        </span>
				    </div>
				</div>



				<div class="form-group label-floating">
		            <label class="control-label">List ID Override:</label>
		            <select name="list_id_override" v-model="listIdOverride" class="form-control">
						<option value="in_file">Load from Lead File</option>
						@foreach($lists as $listItem)
							<option value="{{ $listItem->list_id }}">{{ $listItem->list_id }} -- {{ $listItem->list_name }}</option>
						@endforeach
					</select>
		            <span class="material-input"></span>
		        </div>



		        <div class="form-group label-floating">
		            <label class="control-label">Phone Code Override:</label>
		            <select name="territory" v-model="phoneCodeOverride" class="form-control">
		            	@foreach($phoneCodes as $phoneCode)
		            		<option value="{{ $phoneCode->country_code }}">{{ $phoneCode->country_code }} - {{ $phoneCode->country }}</option>
		            	@endforeach
		            </select>
		            <span class="material-input"></span>
		        </div>



		        <div class="form-group label-floating">
		            <label class="control-label">Lead Duplicate Check:</label>
					<select name="dupcheck" class="form-control" v-model="leadDuplicateCheck">
						<option value="NONE">NO DUPLICATE CHECK</option>
						<option value="DUPLIST">CHECK FOR DUPLICATES BY PHONE IN LIST ID</option>
						<option value="DUPCAMP">CHECK FOR DUPLICATES BY PHONE IN ALL CAMPAIGN LISTS</option>
					</select>
		            <span class="material-input"></span>
		        </div>

			</div><!-- .card-content -->
		</div><!-- .card -->
	</div>
	<div class="col-md-5">
		<div id="sellPerCirc" class="per-circ">
			<div class="per-circ-inner">
				<div class="per-circ-stat">0%</div><div class="per-circ-status">@{{ uploadStatus }}</div>
			</div>
		</div>
		<div class="text-center">
			<button 
				type="submit" 
				class="btn btn-info" 
				v-show="uploadCompleted"
				@click.prevent="step2"
				style="display:none;">Next  <i v-show="retreivingFileClms" class="fa fa-spinner fa-spin"></i></button>
		</div>
	</div>
</div><!-- .row -->