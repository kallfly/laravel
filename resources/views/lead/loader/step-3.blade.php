<div id="step-3" class="row load-steps" v-show="currentStep==3">
	<div class="col-md-4">
		<div class="card">
			<div class="card-header" data-background-color="blue">
		        <h4 class="title">Load a new list</h4>
		        <p class="category">Processing Leads</p>
		    </div>


		    <div class="card-content">
		    	<ul class="list-group">
				  <li class="list-group-item ellipsis">
				  	<label>Lead file:</label>
				  	<strong>@{{ filePath }}</strong>
				  </li>
				  <li class="list-group-item">
				  	<label>List ID Override:</label>
				  	<strong>@{{ listIdOverride }}</strong>
				  </li>
				  <li class="list-group-item">
				  	<label>Phone Code Override:</label>
				  	<strong>@{{ phoneCodeOverride }}</strong>
				  </li>
				  <li class="list-group-item">
				  	<label>Lead Duplicate Check:</label>
				  	<strong>@{{ leadDuplicateCheck }}</strong>
				  </li>
				  <li class="list-group-item">
				  	<label>Lead Time Zone Lookup:</label>
				  	<strong>AREA</strong>
				  </li>
				</ul>

		    	<a class="btn btn-info pull-right" href="/leads-loader">Load another lead file</a>
		    	<div class="clearfix"></div>

			</div><!-- .card-content -->
		</div><!-- .card -->
	</div>





	<div class="col-md-7">

		<div class="card">


			<div class="card-content">

			  	<div class="row" style="margin-top:-35px;">
			  		
			  		<div class="col-md-3">
			  			<div class="alert alert-success text-center">
			  				<div>GOOD</div>
			  				<h3 style="margin:0"><strong>@{{ good }}</strong></h3>
			  			</div>
			  		</div>

			  		<div class="col-md-3">
			  			<div class="alert alert-danger text-center">
			  				<div>BAD</div>
			  				<h3 style="margin:0"><strong>@{{ bad }}</strong></h3>
			  			</div>
			  		</div>

			  		<div class="col-md-3">
			  			<div class="alert alert-info text-center">
			  				<div>TOTAL</div>
			  				<h3 style="margin:0"><strong>@{{ leadLogs.length }}</strong></h3>
			  			</div>
			  		</div>

			  		<div class="col-md-3">
			  			<div class="alert alert-warning text-center">
			  				<div>Status</div>
			  				<h3 style="margin:0" v-show="leadProcessing">Processing <i class="fa fa-spinner fa-spin"></i></h3>
			  				<h3 style="margin:0" v-show="!leadProcessing">Completed</h3>
			  			</div>
			  		</div>

			  	</div>



			  	<div class="live-leads">


				<div class="alert alert-success" v-show="!leadProcessing" style="margin-top:50px;">
					<div class="container-fluid">
						
						<div class="alert-icon">
							<i class="material-icons">check</i>
						</div>

						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true"><i class="material-icons">clear</i></span>
						</button>
						
						<b>Congratulations:</b> your lead file has been successfully processed!

					</div>
				</div>

			  		<ul class="list-group">
			  			<img class="shield" src="img/shield.svg" alt="loading" v-show="leadProcessing" />
			  			<lead-data v-for="log, index in leadLogs" :log="log" :index="index"></lead-data>
			  		</ul>
			  	</div>

			</div><!-- .card-content -->
		</div><!-- .card -->
	
	</div>
</div><!-- .row -->