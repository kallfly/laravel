<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
	<div id="app">
</div>
<script src="/js/app.js"></script>
<script>
	Echo.private('leadsprocess.{{ $user->user_id }}')
		.listen('LeadInserted', (e) => {
			console.log(e);
		});
</script>



</body>
</html>