@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
    	<div class="col-md-12">
    		<div class="card">
				<div class="card-header" data-background-color="blue">
			        <h4 class="title">Call information for <strong>{{ $lead->first_name }} {{ $lead->last_name }}</strong></h4>
			        <p class="category">Lead ID: {{ $lead->lead_id }} - List ID: {{ $lead->list_id }}</p>
			    </div>


			    <div class="card-content">

			    	<form action="/leads/{{ $lead->lead_id }}" method="post">

				    	<br><br>

				    	@include('layouts.form-errors');
						
						<div class="row">
							<div class="col-md-6">

						    	@include('lead.view.personal-information')

								<br><br>

								@include('lead.view.others')

							</div><!-- .col-md-6 -->


							<div class="col-md-6">

								@include('lead.view.location')

								<br><br>

								@include('lead.view.contact-details')

							</div><!-- .col-md-6 -->
						</div><!-- .row -->


						<br><br>


						<button class="btn btn-info pull-right" type="submit">Save</button>

						<div class="clearfix"></div>


						{{ method_field('patch') }}
						{{ csrf_field() }}

					</form>

				</div><!-- .card-content -->



				@include('lead.view.tab')
			</div><!-- .card -->

			<button class="btn btn-danger pull-right" type="button" @click="deleteLead">
				<i class="fa fa-trash"></i>
				Delete this Lead
			</button>
			<div class="clearfix"></div>
    	</div><!-- .col-md-12 -->
    </div><!-- .row -->
</div><!-- .container-fluid -->
@endsection











@section('custom_scripts')
<script>

new Vue({
	el: '.main-panel',
	data: {
		recordings: [],
		logs: []
	},

	methods: {

		getRecordings: function() {

			$.getJSON('/list/{{ $lead->lead_id }}/ajax/call-recordings', function(data){

				this.recordings = data;

			}.bind(this));

		},





		getLogs: function() {

			$.getJSON('/list/{{ $lead->lead_id }}/ajax/logs', function(data){

				this.logs = data;

			}.bind(this));

		},





		deleteLead: function() {
			swal({
				title: "Are you sure?",
				text: "Once deleted, you will not be able to recover this lead!",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					swal("Congratulations! Lead was deleted!", {
						icon: "success"
					});
				}
			});
		}

	},





	mounted: function() {

		this.getRecordings();
		this.getLogs();

	}

});

</script>
@endsection