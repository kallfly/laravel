<fieldset>
	<legend>Others</legend>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group label-floating">
				<label class="control-label">Show</label>
				<input type="text" class="form-control" value="{{ $lead->security_phrase }}" name="security_phrase" />
				<span class="material-input"></span>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group label-floating">
				<label class="control-label">Vendor ID</label>
				<input type="text" class="form-control" value="{{ $lead->vendor_lead_code }}" name="vendor_lead_code" />
				<span class="material-input"></span>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group label-floating">
				<label class="control-label">Rank</label>
				<input type="text" class="form-control" value="{{ $lead->rank }}" name="rank" />
				<span class="material-input"></span>
			</div>
		</div>
	</div><!-- .row -->


	<div class="row">
		<div class="col-md-6">
			<div class="form-group label-floating">
				<label class="control-label">Disposition</label>
				<select name="status" class="form-control">
					@foreach($statusOptions as $status => $label)
					<option
						value="{{ $status }}"
						{{ $status == $lead->status ? 'selected' : '' }} >
						{{ $status }} - {{ $label }}
					</option>
					@endforeach
				</select>
				<span class="material-input"></span>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group label-floating">
				<label class="control-label">Owner</label>
				<input type="text" class="form-control" value="{{ $lead->owner }}" name="owner" />
				<span class="material-input"></span>
			</div>
		</div>
	</div><!-- .row -->

	<div class="row">
		<div class="col-md-12">
			<div class="form-group label-floating">
				<label class="control-label">Comments</label>
				<textarea type="text" class="form-control" name="comments" rows="5">{{ $lead->comments }}</textarea>
				<span class="material-input"></span>
			</div>
		</div>
	</div><!-- .row -->
</fieldset>