<fieldset>
		<legend>Location</legend>
    	
    	<div class="row">
    		<div class="col-md-12">
    			<div class="form-group label-floating">
					<label class="control-label">Address 1</label>
					<input type="text" class="form-control" value="{{ $lead->address1 }}" name="address1" />
					<span class="material-input"></span>
				</div>
			</div>
		</div><!-- .row -->


		<div class="row">
    		<div class="col-md-6">
    			<div class="form-group label-floating">
					<label class="control-label">Address 2</label>
					<input type="text" class="form-control" value="{{ $lead->address2 }}" name="address2" />
					<span class="material-input"></span>
				</div>
			</div>

    		<div class="col-md-6">
    			<div class="form-group label-floating">
					<label class="control-label">Address 3</label>
					<input type="text" class="form-control" value="{{ $lead->address3 }}" name="address3" />
					<span class="material-input"></span>
				</div>
			</div>
		</div><!-- .row -->


		<div class="row">
			<div class="col-md-4">
				<div class="form-group label-floating">
					<label class="control-label">City</label>
					<input type="text" class="form-control" value="{{ $lead->city }}" name="city" />
					<span class="material-input"></span>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group label-floating">
					<label class="control-label">State</label>
					<input type="text" class="form-control" value="{{ $lead->state }}" name="state" />
					<span class="material-input"></span>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group label-floating">
					<label class="control-label">Postal Code</label>
					<input type="text" class="form-control" value="{{ $lead->postal_code }}" name="postal_code" />
					<span class="material-input"></span>
				</div>
			</div>
		</div><!-- .row -->


		<div class="row">
			<div class="col-md-6">
				<div class="form-group label-floating">
					<label class="control-label">Province</label>
					<input type="text" class="form-control" value="{{ $lead->province }}" name="province" />
					<span class="material-input"></span>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group label-floating">
					<label class="control-label">Country</label>
					<input type="text" class="form-control" value="{{ $lead->country_code }}" name="country_code" />
					<span class="material-input"></span>
				</div>
			</div>
		</div><!-- .row -->
	</fieldset>