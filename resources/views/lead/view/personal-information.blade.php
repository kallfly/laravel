<fieldset>
	<legend>Personal Information</legend>
	<div class="row">
		<div class="col-md-2">
			<div class="form-group label-floating">
				<label class="control-label">Title</label>
				<input type="text" class="form-control" value="{{ $lead->title }}" name="title" />
				<span class="material-input"></span>
			</div>
		</div>
	</div><!-- .row -->


	<div class="row">
		<div class="col-md-5">
			<div class="form-group label-floating">
				<label class="control-label">First Name</label>
				<input type="text" class="form-control" value="{{ $lead->first_name }}" name="first_name" />
				<span class="material-input"></span>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group label-floating">
				<label class="control-label">Last Name</label>
				<input type="text" class="form-control" value="{{ $lead->last_name }}" name="last_name" />
				<span class="material-input"></span>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group label-floating">
				<label class="control-label">MI</label>
				<input type="text" class="form-control" value="{{ $lead->middle_initial }}" name="middle_initial" />
				<span class="material-input"></span>
			</div>
		</div>
	</div><!-- .row -->
</fieldset>