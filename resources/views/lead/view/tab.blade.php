<div class="card card-nav-tabs" style="box-shadow:none;">
    <div class="card-header" data-background-color="blue">
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" data-tabs="tabs">
                    <li class="active">
                        <a href="#profile" data-toggle="tab" aria-expanded="false">
                            <i class="material-icons">phone</i> Calls
                            <div class="ripple-container"></div>
                        </a>
                    </li>
                    <li>
                        <a href="#messages" data-toggle="tab" aria-expanded="true">
                            <i class="material-icons">code</i> Recording
                            <div class="ripple-container"></div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card-content">
        <div class="tab-content" style="height:auto;">
            <div class="tab-pane active" id="profile">
                <table class="table">
                    <thead>
                        <tr>
                            <th><strong>DATE/TIME</strong></th>
                            <th><strong>LENGTH</strong></th>
                            <th><strong>STATUS</strong></th>
                            <th><strong>AGENT</strong></th>
                            <th><strong>CAMPAIGN</strong></th>
                            <th><strong>LIST</strong></th>
                            <th><strong>HANGUP</strong></th>
                            <th><strong>PHONE</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="log in logs">
                            <td>@{{ log.date }}</td>
                            <td>@{{ log.length_in_sec }}</td>
                            <td>@{{ log.status }}</td>
                            <td>@{{ log.user }}</td>
                            <td>@{{ log.campaign_name }}</td>
                            <td>@{{ log.list_id }}</td>
                            <td>@{{ log.term_reason }}</td>
                            <td>@{{ log.phone_number }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="messages">
                <table class="table">
                    <thead>
                        <th><strong>DATE/TIME</strong></th>
                        <th><strong>REC. ID</strong></th>
                        <th><strong>FILENAME</strong></th>
                        <th><strong>LOCATION</strong></th>
                        <th><strong>LISTEN</strong></th>
                        <th><strong>AGENT</strong></th>
                    </thead>
                    <tbody>
                        <tr v-for="log in recordings">
                            <td>@{{ log.date }}</td>
                            <td>@{{ log.recording_id }}</td>
                            <td>@{{ log.filename }}</td>
                            <td>
                                <a class="text-info" v-bind:href="log.location" target="_blank">
                                    <i class='fa fa-download'></i> Download
                                </a>
                            </td>
                            <td>
                                <audio controls>
                                    <source v-bind:src="log.location" type="audio/mpeg">
                                    Your browser does not support the audio element.
                                </audio>
                            </td>
                            <td>@{{ log.user }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>