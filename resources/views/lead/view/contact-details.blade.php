<fieldset>
	<legend>Contact Details</legend>
	<div class="row">
		<div class="col-md-2">
			<div class="form-group label-floating">
				<label class="control-label">Dial Code</label>
				<input type="text" class="form-control" value="{{ $lead->phone_code }}" name="phone_code" />
				<span class="material-input"></span>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group label-floating">
				<label class="control-label">Phone</label>
				<input type="text" class="form-control" value="{{ $lead->phone_number }}" name="phone_number" />
				<span class="material-input"></span>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group label-floating">
				<label class="control-label">Alt Phone</label>
				<input type="text" class="form-control" value="{{ $lead->alt_phone }}" name="alt_phone" />
				<span class="material-input"></span>
			</div>
		</div>
	</div><!-- .row -->


	<div class="row">
		<div class="col-md-12">
			<div class="form-group label-floating">
				<label class="control-label">Email</label>
				<input type="text" class="form-control" value="{{ $lead->email }}" name="email" />
				<span class="material-input"></span>
			</div>
		</div>
	</div><!-- .row -->
</fieldset>