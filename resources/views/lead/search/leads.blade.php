<div class="card">
    <div class="card-header" data-background-color="blue">
        <h4 class="title">Leads List</h4>
        <p class="category">List of Leads</p>
    </div>

    <div class="card-content table-responsive">
        <table class="table datatable">
            <thead class="text-danger">
                <tr>
                    <th>Phone</th>
                    <th>Lead ID</th>
                    <th>Status</th>
                    <th>List Name</th>
                    <th>Country Code</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Agent Name</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>