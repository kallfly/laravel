<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header" data-background-color="blue">
		        <h4 class="title">Lead Search</h4>
		        <p class="category">Search Form</p>
		    </div>
		    <div class="card-content">
		    	<form action="#" id="form-search">
					<div class="form-group label-floating">
                        <label class="control-label">Phone</label>
                        <input class="form-control" type="text" name="phone" value="">
                    	<span class="material-input"></span>
                    </div>



                	<div class="filters">
                    	<div class="col-md-4 input-status">
                    		<div class="form-group label-floating">
		                        <label class="control-label">Status</label>
		                        <select name="status" class="form-control">
		                        	<option value="none">Select a Status</option>
		                        	@foreach($statusOptions as $status => $label)
		                        		<option value="{{ $status }}">{{ $label }}</option>
		                        	@endforeach
		                        </select>
		                    	<span class="material-input"></span>
		                    </div>	
                    	</div>
                    	<div class="col-md-4 input-lead_id">
                    		<div class="form-group label-floating">
		                        <label class="control-label">Lead ID</label>
		                        <input class="form-control" type="text" name="lead_id" value="">
		                    	<span class="material-input"></span>
		                    </div>	
                    	</div>
                    	<div class="col-md-4 input-list_id">
                    		<div class="form-group label-floating">
		                        <label class="control-label">List ID</label>
		                        <input class="form-control" type="text" name="list_id" value="">
		                    	<span class="material-input"></span>
		                    </div>	
                    	</div>
                    	<div class="col-md-4 input-first_name">
                    		<div class="form-group label-floating">
		                        <label class="control-label">First Name</label>
		                        <input class="form-control" type="text" name="first_name" value="">
		                    	<span class="material-input"></span>
		                    </div>	
                    	</div>
                    	<div class="col-md-4 input-last_name">
                    		<div class="form-group label-floating">
		                        <label class="control-label">Last Name</label>
		                        <input class="form-control" type="text" name="last_name" value="">
		                    	<span class="material-input"></span>
		                    </div>	
                    	</div>
                    	<div class="col-md-4 input-user">
                    		<div class="form-group label-floating">
		                        <label class="control-label">Agent Name</label>
		                        <input class="form-control" type="text" name="user" value="">
		                    	<span class="material-input"></span>
		                    </div>	
                    	</div>
                    </div>


                	<div class="clearfix"></div>

                	<div class="checkboxes pull-left">
                		<div class="form-checkbox">
                			<input type="checkbox" id="check-status" name="check_status" target="input-status" />
                			<label for="check-status">Status</label>
                		</div>
                		<div class="form-checkbox">
                			<input type="checkbox" id="check-lead-id" name="check_lead_id" target="input-lead_id" />
                			<label for="check-lead-id">Lead ID</label>
                		</div>
                		<div class="form-checkbox">
                			<input type="checkbox" id="check-list-id" name="check_list_id" target="input-list_id" />
                			<label for="check-list-id">List ID</label>
                		</div>
                		<div class="form-checkbox">
                			<input type="checkbox" id="check-first-name" name="check_first_name" target="input-first_name" />
                			<label for="check-first-name">First Name</label>
                		</div>
                		<div class="form-checkbox">
                			<input type="checkbox" id="check-last-name" name="check_last_name" target="input-last_name" />
                			<label for="check-last-name">Last Name</label>
                		</div>
                		<div class="form-checkbox">
                			<input type="checkbox" id="check-user" name="check_user" target="input-user" />
                			<label for="check-user">Agent Name</label>
                		</div>
                	</div>


                	<button type="submit" class="btn btn-info pull-right">
                		<i class="fa fa-search"></i>
                		Search
                	</button>

                	<div class="clearfix"></div>

					{{ csrf_field() }}
				</form>
			</div>
		</div>
	</div>
</div>