<div class="col-md-8">

    @if(Session::has('flash_delete_message'))
    <div class="alert alert-danger">
        <div class="container-fluid">
            <div class="alert-icon">
                <i class="material-icons">delete</i>
            </div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="material-icons">clear</i></span>
            </button>
            {!! session('flash_delete_message') !!}
        </div>
    </div>
    @endif

    <div class="card">
        <div class="card-header" data-background-color="blue">
            <h4 class="title">Users</h4>
            <p class="category">List of Users</p>
        </div>

        <div class="card-content table-responsive">
            <table class="table">
                <thead class="text-danger">
                    <tr>
                        <th>Username</th>
                        <th>Full Name</th>
                        <th>Group</th>
                        <th>Active</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($users)>0) @foreach($users as $u)
                    <tr>
                        <td>{{ $u->user }}</td>
                        <td>{{ $u->full_name }}</td>
                        <td>{{ strtoupper($u->user_group) }}</td>
                        <td>
                            @if($u->active)
                            <i class="fa fa-toggle-on text-success"></i> Yes
                            @else
                            <i class="fa fa-toggle-off text-danger"></i> No
                            @endif
                        </td>
                        <td>
                            <a href="/user/{{ $form['user_type'] }}/{{ $u->user_id }}/edit"><i class="material-icons text-warning">edit</i></a>
                        </td>
                    </tr>
                    @endforeach @else
                    <tr>
                        <td colspan="6" class="text-center"><h5>No Users yet.</h5></td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>