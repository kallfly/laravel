<div class="col-md-5">

    <div class="card">
        <div class="card-header" data-background-color="blue">
            <h4 class="title">My Agent Login Details</h4>
            <p class="category">List of Users</p>
        </div>

        <div class="card-content agent-login-details">
            <br/>
            <h4>1. Download and install(windows) SIP Phone using this link</h4>
            <div class="access-info text-center">
                <a class="btn btn-info ellipsis" href="http://sg.kallfly.com/softphone/KFsphone.exe" title="http://sg.kallfly.com/softphone/KFsphone.exe">http://sg.kallfly.com/softphone/KFsphone.exe</a><br/>
                OR<br/>
                <a class="btn btn-info ellipsis" href="http://admin-kallpod.kallfly.com/softphone/KFsphone.exe" title="http://admin-kallpod.kallfly.com/softphone/KFsphone.exe">http://admin-kallpod.kallfly.com/softphone/KFsphone.exe</a><br/>
            </div>

            <h4>2. Configure your SIP Phone using this details:</h4>
            <div class="access-info">
                <strong>Username:</strong> {{ $user->phones->extension }}<br/>
                <strong>Password:</strong> {{ $user->phones->conf_secret }}<br/>
                <strong>Authorization:</strong> {{ $user->phones->extension }}<br/>
                <strong>Domain IP:</strong> {{ $user->phones->server_ip }}<br/>
            </div>

            <h4>3. Once SIP Phone status is READY, proceed to this URL to login:</h4>
            <div class="access-info text-center">
                <a class="btn btn-info ellipsis" href="http://agent-kallpod.kallfly.com/agent/login.php?relog=1&phone={{ $user->phones->extension }}&login={{ $user->user }}" title="http://agent-kallpod.kallfly.com/">http://agent-kallpod.kallfly.com/</a>
            </div>

            <h4>4 Login to KallPod Agent portal using this credentials:</h4>
            <div class="access-info">
                <strong>SIP Phone:</strong> {{ $user->user }}<br/>
                <strong>Username:</strong> {{ $user->phones->extension }}<br/>
                <strong>Password:</strong> {{ $user->phones->pass }}<br/>
            </div>
        </div>
    </div>
</div>