@extends('layouts.master')


@section('content')
<div class="container-fluid">
    <div class="row">
    	@include('user.agent.form')
    	@include('user.agent.login-details')
    </div>
</div>
@endsection

@section('custom_scripts')
<script>
$(function(){
	$('.table').dataTable({
		"columnDefs": [
			{
				"targets": 4,
				"orderable": false
			}
		]
	});
});
</script>
@endsection>