@extends('layouts.master')


@section('content')
<div class="container-fluid">
    <div class="row">
    	@include('user.list')
    	@include('user.form')
    </div>
</div>
@endsection

@section('custom_scripts')
<script>
$(function(){
	$('.table').dataTable({
		"columnDefs": [
			{
				"targets": 4,
				"orderable": false
			}
		]
	});
});
</script>
@endsection>