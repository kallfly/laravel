<div class="col-md-4">
    <div class="card">
        <div class="card-header" data-background-color="blue">
            <h4 class="title">User</h4>
            <p class="category">Complete the user details below</p>
        </div>
        <div class="card-content">
        	@include('layouts.form-errors')
            <form action="{{ $form['action'] }}" method="POST">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{ $form['user_type'] == 'agent' ? 'input-group' : '' }} label-floating">
                            @if($form['user_type'] == 'agent')
                                <div class="input-group-addon">{{ $agentPrefix }}</div>
                            @endif
                            <label class="control-label">Username</label>
                            <input 
                                type="text" 
                                class="form-control" 
                                value="{{ $user->user }}" 
                                name="user"
                                {{ $editMode ? 'disabled' : '' }}>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group label-floating">
                            <label class="control-label">Full Name</label>
                            <input type="text" class="form-control" value="{{ $user->full_name }}" name="full_name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group label-floating">
                            <label class="control-label">Password</label>
                            <input type="password" class="form-control" value="" name="pass">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group label-floating">
                            <label class="control-label">Confirm Password</label>
                            <input type="password" class="form-control" value="" name="pass_confirmation">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group label-floating">
                            <label class="control-label">Countries</label>
                            <select name="territory" class="form-control">
                            @foreach($countries as $code => $country)
                                <option
                                    value="{{ $code }}"
                                    {{ $code == $user->territory ? 'selected' : '' }}>
                                        {{ $country }}
                                </option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                @if($editMode)
                    <button type="submit" class="btn btn-info pull-right">Update {{ $form['user_type'] }}</button>
                    <a href="{{ $form['redirect'] }}" class="btn btn-default pull-right">Cancel</a>
                    {!! method_field('patch') !!}
                @else
                    <button type="submit" class="btn btn-info pull-right">Add {{ $form['user_type'] }}</button>
                @endif
                <div class="clearfix"></div>

                <input type="hidden" name="redirect" value="{{ $form['redirect'] }}">
                <input type="hidden" name="user_type" value="{{ $form['user_type'] }}">
                {{ csrf_field() }}
            </form>
        </div>
    </div>
</div>