
@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">

        <div class="col-md-8">
           @include('campaign.list')
        </div>
        <div class="col-md-4">
    	   @include('campaign.form')
        </div>
    </div>
</div>
@endsection

@section('custom_scripts')
<script>
$(function(){
    $('.datatable').dataTable({
        "columnDefs": [
            {
                "targets": 5,
                "orderable": false
            }
        ]
    });
});
</script>
@endsection>