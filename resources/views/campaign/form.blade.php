
<div class="card">
    <div class="card-header" data-background-color="blue">
        <h4 class="title">Campaign</h4>
        <p class="category">Complete the user details below</p>
    </div>
    <div class="card-content">
    	@include('layouts.form-errors')
        <form action="{{ $form['action'] }}" method="POST">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group label-floating">
                        <label class="control-label">Campaign Name</label>
                        <input type="text" class="form-control" value="{{ $campaign->campaign_name }}" name="campaign_name">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group label-floating">
                        <label class="control-label">Web Form Address</label>
                        <input type="text" class="form-control" value="{{ $campaign->web_form_address }}" name="web_form_address">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group label-floating">
                        <label class="control-label">Allow Inbound & Blended</label>
                        <select name="campaign_allow_inbound" class="form-control">
                            <option value="false">Please Select</option>
                            <option value="Y" {{ $campaign->campaign_allow_inbound == 'Y' ? 'selected' : '' }}>Yes</option>
                            <option value="N" {{ $campaign->campaign_allow_inbound == 'N' ? 'selected' : '' }}>No</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group label-floating">
                        <label class="control-label">Dial Method</label>
                        <select name="dial_method" class="form-control">
                            <option value="false">Please Select</option>
                            <option {{ $campaign->dial_method == 'MANUAL' ? 'selected' : '' }} value="MANUAL">Manual</option>
                            <option {{ $campaign->dial_method == 'RATIO' ? 'selected' : '' }} value="RATIO">Ratio</option>
                            <option {{ $campaign->dial_method == 'ADAPT_HARD_LIMIT' ? 'selected' : '' }} value="ADAPT_HARD_LIMIT">ADAPT Hard Limit</option>
                            <option {{ $campaign->dial_method == 'ADAPT_TAPERED' ? 'selected' : '' }} value="ADAPT_TAPERED">ADAPT Tapered</option>
                            <option {{ $campaign->dial_method == 'ADAPT_AVERAGE' ? 'selected' : '' }} value="ADAPT_AVERAGE">ADAPT Avarage</option>
                            <option {{ $campaign->dial_method == 'INBOUND_MAN' ? 'selected' : '' }} value="INBOUND_MAN">Inbound Man</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group label-floating">
                        <label class="control-label">Auto Dial Level</label>
                        <select name="auto_dial_level" class="form-control">
                            <option value="false">Please Select</option>
                            <option {{ $campaign->auto_dial_level == '0' ? 'selected' : '' }} value="0">0</option>
                            <option {{ $campaign->auto_dial_level == '1' ? 'selected' : '' }} value="1">1</option>
                            <option {{ $campaign->auto_dial_level == '2' ? 'selected' : '' }} value="2">2</option>
                            <option {{ $campaign->auto_dial_level == '3' ? 'selected' : '' }} value="3">3</option>
                            <option {{ $campaign->auto_dial_level == '4' ? 'selected' : '' }} value="4">4</option>
                            <option {{ $campaign->auto_dial_level == '5' ? 'selected' : '' }} value="5">5</option>
                            <option {{ $campaign->auto_dial_level == '6' ? 'selected' : '' }} value="6">6</option>
                            <option {{ $campaign->auto_dial_level == '7' ? 'selected' : '' }} value="7">7</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group label-floating">
                        <label class="control-label">Next Agent Call</label>
                        <select name="next_agent_call" class="form-control">
                            <option {{ $campaign->next_agent_call == 'false' ? 'selected' : '' }} value="false">Please Select</option>
                            <option {{ $campaign->next_agent_call == 'random' ? 'selected' : '' }} value="random">Random</option>
                            <option {{ $campaign->next_agent_call == 'oldest_call_start' ? 'selected' : '' }} value="oldest_call_start">Oldest Call Start</option>
                            <option {{ $campaign->next_agent_call == 'oldest_call_finish' ? 'selected' : '' }} value="oldest_call_finish">Oldest Call Finish</option>
                            <option {{ $campaign->next_agent_call == 'overall_user_level' ? 'selected' : '' }} value="overall_user_level">Overall User Level</option>
                            <option {{ $campaign->next_agent_call == 'campaign_rank' ? 'selected' : '' }} value="campaign_rank">Campaign Rank</option>
                            <option {{ $campaign->next_agent_call == 'fewest_calls' ? 'selected' : '' }} value="fewest_calls">Fewest Call</option>
                            <option {{ $campaign->next_agent_call == 'longest_wait_time' ? 'selected' : '' }} value="longest_wait_time">Longest wait time</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group label-floating">
                        <label class="control-label">Campaign Caller ID</label>
                        <input type="text" class="form-control" name="campaign_cid" value="{{ $campaign->campaign_name }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group label-floating">
                        <label class="control-label">Campaign Recording</label>
                        <select name="campaign_recording" class="form-control">
                            <option {{ $campaign->campaign_recording == 'false' ? 'selected' : '' }} value="false">Please Select</option>
                            <option {{ $campaign->campaign_recording == 'NEVER' ? 'selected' : '' }} value="NEVER">NEVER</option>
                            <option {{ $campaign->campaign_recording == 'ONDEMAND' ? 'selected' : '' }} value="ONDEMAND">ONDEMAND</option>
                            <option {{ $campaign->campaign_recording == 'ALLCALLS' ? 'selected' : '' }} value="ALLCALLS">ALLCALLS</option>
                            <option {{ $campaign->campaign_recording == 'ALLFORCE' ? 'selected' : '' }} value="ALLFORCE">ALLFORCE</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group label-floating">
                        <label class="control-label">Status</label>
                        <select name="active" class="form-control">
                            <option {{ $campaign->active == 'Y' ? 'selected' : '' }} value="Y">Active</option>
                            <option {{ $campaign->active == 'N' ? 'selected' : '' }} value="N">Inactive</option>
                        </select>
                    </div>
                </div>
            </div>

            @if($editMode)
                <button type="submit" class="btn btn-info pull-right">Update</button>
                <a href="/campaigns" class="btn btn-default pull-right">Cancel</a>
                {!! method_field('patch') !!}
            @else
                <button type="submit" class="btn btn-info pull-right">Add</button>
            @endif
            <div class="clearfix"></div>

            {{ csrf_field() }}
        </form>
    </div>
</div>