


<form 
    action="/campaigns/{{ $campaign->campaign_id }}/add-status"
    method="POST"
    id="add-call-status-form"
    class="form-inline">

    <select class="form-control" name="status">
        @foreach($statusOptions as $status => $statusName)
            <option 
                value="{{ $status }}"
                {{ $status == 'SALE' ? 'selected' : '' }}>
                {{ $status . ' - ' .$statusName }}
            </option>
        @endforeach
    </select>
    <button class="btn btn-success">
        <i class="fa fa-save"></i> Save
    </button>
    <a class="back btn btn-default" href="#">
        <i class="fa fa-arrow-circle-left"></i>
        Cancel
    </a>

    {{ csrf_field() }}
</form>




<div class="card card-stats">
    <div class="card-header" data-background-color="green">
        <i class="fa fa-check-square-o"></i>
    </div>
    <div class="card-content">
        <p class="category">
            STATUSES TO DIAL
            <a 
                href="#" 
                class="btn btn-success btn-xs" 
                id="add-call-status-btn"
                data-toggle="tooltip" 
                data-placement="top"
                title="Add Status">
                <i class="fa fa-plus"></i>
            </a>
        </p>

        <h3 class="title tags">
        @foreach($dialables as $item)
            <form action="/campaigns/{{ $campaign->campaign_id }}/delete-status" method="post">
                {{ csrf_field() }}
                {{ method_field('delete') }}
                <input type="hidden" name="status" value="{{ $item }}" />
                <a
                    href="#"
                    class="tag" 
                    data-toggle="tooltip" 
                    data-placement="top" 
                    title="Delete {{ $item }} - {{ \App\Status::getStatusName($item) }}"
                    data-description="{{ $item }} - {{ \App\Status::getStatusName($item) }}">
                    {{ $item }}
                    <i class="fa fa-times"></i>
                </a>
            </form>
        @endforeach
        </h3>
    </div>
    <div class="card-footer">
        <div class="stats">
            Click any of the statuses to delete
        </div>
    </div>
</div>





<div class="card card-stats">
    <div class="card-header" data-background-color="orange">
        <i class="fa fa-phone"></i>
    </div>
    <div class="card-content">
        <p class="category">TOTAL DIALABLE LEADS</p>
        <h3 class="title" v-html="totalDialables">0</h3>
    </div>
    <div class="card-content table-responsive">
        <table class="table">
            <thead class="text-danger">
                <tr>
                    <th>List Name</th>
                    <th>Dialable</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <tr 
                    is="dialable-row"
                    v-for="listItem in dialableList" 
                    :list_item="listItem"></tr>
                <tr 
                    is="dialable-row-no-list"
                    v-show="!retrievingDialables && dialableList.length==0"></tr>
            </tbody>
        </table>
    </div>
</div>





<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>