
@extends('layouts.master')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-7">
            @include('campaign.form')
        </div>
        <div class="col-md-5">
            @include('campaign.campaign-details')
        </div>
    </div>
</div>
@endsection










@section('vue_templates')
<template id="dialable-row">
    <tr>
        <td>@{{ list_item.list_name }}</td>
        <td v-html="list_item.dialables"></td>
        <td v-html="list_item.total"></td>
    </tr>
</template>

<template id="dialable-row-no-list">
    <tr>
        <td colspan="3" class="text-center"><h5>No List yet.</h5></td>
    </tr>
</template>
@endsection










@section('custom_scripts')
<script>
$(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('#add-call-status-btn').click(function(e){
        e.preventDefault();  
        $(this).hide();
        $('#add-call-status-form').show();
    });

    $('#add-call-status-form .back').click(function(e){
        e.preventDefault();
        $('#add-call-status-form').hide();
        $('#add-call-status-btn').show();
    });

    $('#add-call-status-btn').click(function(e){
        e.preventDefault();   
    });


    $('.tag').click(function(e){
        e.preventDefault();

        var button = $(this);
        var form = button.parent();


        swal({
          title: "DELETE " + button.attr('data-description'),
          text: "Are you sure you want to delete " + button.attr('data-description') + '?',
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then(function(willDelete){

            if(!willDelete) return

            form.trigger('submit');

        });
    });
});



new Vue({
    el: '.main-panel',
    data: {
        dialableList: {!! $dialableList !!},
        retrievingDialables: true
    },
    computed: {
        totalDialables: function(){
            Array.prototype.sum = function (prop) {
                var dialables = 0
                for ( var i = 0, _len = this.length; i < _len; i++ ) {
                    dialables += parseInt(this[i][prop]) || 0;
                }
                return dialables
            }

            return this.dialableList.sum('dialables');
        }
    },
    mounted(){

        var self = this;
        this.dialableList.forEach(function(element, index){
            spinner = '<i class="fa fa-spin fa-spinner"></i>';
            self.dialableList[index].dialables = spinner;
            self.dialableList[index].total = spinner;
            $.getJSON('/list/' + element.list_id + '/ajax/total-dialables', function(listItem){
                self.dialableList[index].dialables = listItem.dialables;
                self.dialableList[index].total = listItem.total;
            });
        });
    },
    components: {
        'dialable-row': {
            template: '#dialable-row',
            props: ['list_item'],
            created: function(){
                // console.log(this.list_item);
            }
        },
        'dialable-row-no-list': {
            template: '#dialable-row-no-list'
        }
    }
})
</script>
@endsection>