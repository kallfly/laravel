@if(Session::has('flash_delete_message'))
<div class="alert alert-danger">
    <div class="container-fluid">
        <div class="alert-icon">
            <i class="material-icons">delete</i>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"><i class="material-icons">clear</i></span>
        </button>
        {!! session('flash_delete_message') !!}
    </div>
</div>
@endif

<div class="card">
    <div class="card-header" data-background-color="blue">
        <h4 class="title">Campaigns</h4>
        <p class="category">List of Campaigns</p>
    </div>

    <div class="card-content table-responsive">
        <table class="table datatable">
            <thead class="text-danger">
                <tr>
                    <th>Campaign ID</th>
                    <th>Name</th>
                    <th>Active</th>
                    <th>Dial Method</th>
                    <th>Level</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if(count($campaigns)>0) @foreach($campaigns as $c)
                <tr>
                    <td>{{ $c->campaign_id }}</td>
                    <td>{{ $c->campaign_name }}</td>
                    <td>
                        @if($c->active=='Y')
                        <i class="fa fa-toggle-on text-success"></i> Yes
                        @else
                        <i class="fa fa-toggle-off text-danger"></i> No
                        @endif
                    </td>
                    <td>{{ $c->dial_method }}</td>
                    <td>{{ $c->auto_dial_level }}</td>
                    <td>
                        <a href="/campaigns/{{ $c->campaign_id }}/edit"><i class="material-icons text-warning">edit</i></a>
                    </td>
                </tr>
                @endforeach @else
                <tr>
                    <td colspan="6" class="text-center"><h5>No Campaigns yet.</h5></td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>