<div class="col-md-12">

    @if(Session::has('flash_delete_message'))
    <div class="alert alert-danger">
        <div class="container-fluid">
            <div class="alert-icon">
                <i class="material-icons">delete</i>
            </div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="material-icons">clear</i></span>
            </button>
            {!! session('flash_delete_message') !!}
        </div>
    </div>
    @endif

    <div class="card">
        <div class="card-header" data-background-color="blue">
            <h4 class="title">Invoice List</h4>
            <p class="category">List of Customer Invoice</p>
        </div>

        <div class="card-content table-responsive">
            <table class="table">
                <thead class="text-danger">
                    <tr>
                        <th>Invoice #</th>
                        <th>Custommer Name</th>
                        <th>Number of Users</th>
                        <th>Total Price</th>
                        <th>Invoice Date</th>
                        <th>Paid</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($invoices)>0) @foreach($invoices as $invoice)
                    <tr>
                        <td>{{ $invoice->invoice_number }}</td>
                        <td>{{ $invoice->vicidialUser->full_name }}</td>
                        <td>{{ $invoice->num_users }}</td>
                        <td>{{ '$' . number_format($invoice->total_price, 2) }}</td>
                        <td>{{ $invoice->invoice_date }}</td>
                        <td>{{ $invoice->paid ? 'Paid' : 'Unpaid' }}</td>
                        <td>
                            <a href="#" data-toggle="modal" data-target="#modal-invoice"><i class="material-icons text-primary">search</i></a>
                            <a href="#"><i class="material-icons text-success">print</i></a>
                        </td>
                    </tr>
                    @endforeach @else
                    <tr>
                        <td colspan="6" class="text-center"><h5>No Schedules yet.</h5></td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal-invoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" style="width:100%;max-width:800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="pull-right">
                        INVOICE 
                        <span class="modal-data paid-heading"><strong class="text-success">PAID</strong></span>
                    </h3>
                    <a href="index.php"><img src="img/logo.png" class="image-responsive" alt="kallfy-logo"></a>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            From<br>
                            <strong>Kallfly.com</strong><br>
                            https://kallfly.com<br>
                            finance@kallfly.com
                        </div>
                        <div class="col-md-4">
                            To<br>
                            <strong>Mark Anthony Lapuz</strong><br>
                            Email: mlapuz@kallfly.com                       </div>
                        <div class="col-md-5">
                            Details<br>
                            <strong>Invoice #<span class="modal-data invoice">20180123-542530</span></strong><br>
                            Account: 000914                     </div>
                    </div>

                    <br><br>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Qty</th>
                                <th>Product</th>
                                <th>Description</th>
                                <th>Method</th>
                                <th>Paid</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr class="active">
                                <td>1</td>
                                <td>VOIP</td>
                                <td>VOIP minutes Top up</td>
                                <td><span class="modal-data method">Paypal</span></td>
                                <td><span class="modal-data paid-bool">Yes</span></td>
                                <td><span class="modal-data amount">$50.00</span></td>
                            </tr>
                        </tbody>
                    </table>

                    <br><br>

                    <div class="row">
                        <div class="col-md-6">
                            <h4>
                                Payment Method: 
                                <strong class="modal-data method">Paypal</strong>
                            </h4>

                            <div class="alert alert-default">
                                This serves as the official invoice for VOIP Minutes top up from Mark Anthony Lapuz to Kallfly.com. Please print this page if a hard copy is needed.
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h5>Due Date: <strong class="modal-data format-date">Jan 23, 2018 07:16 am</strong></h5>

                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th style="width:100px;">Subtotal:</th>
                                        <td><span class="modal-data amount">$50.00</span></td>
                                    </tr>
                                    <tr>
                                        <th>Tax:</th>
                                        <td class="modal-data format-tax">$1.25</td>
                                    </tr>
                                    <tr>
                                        <th><h4>Total</h4></th>
                                        <td><h4 class="modal-data format-total">$51.25</h4></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success pull-left print-this" href="top-up-invoice.php?id=268"><i class="fa fa-file-print print-this" aria-hidden="true" href="top-up-invoice.php?id=268"></i> Print</a>
                    <button type="button" class="btn btn-default btn-close" data-dismiss="modal">Close</button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
</div>