<div class="col-md-4">
    <div class="card">
        <div class="card-header" data-background-color="blue">
            <h4 class="title">Edit Profile</h4>
            <p class="category">Complete your profile</p>
        </div>
        <div class="card-content">
        	@include('layouts.form-errors')
            <form action="{{ $form['action'] }}" method="POST">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group label-floating">
                            <label class="control-label">Customer ID</label>
                            <input type="text" class="customer-id form-control" value="{{ $customer->customer_id }}" disabled>
                            <input type="hidden" class="customer-id-hidden form-control" name="customer_id" value="{{ $customer->customer_id }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                	<div class="col-md-12">
                        <div class="form-group label-floating">
                            <label class="control-label">Full Name</label>
                            <input 
                                id="full-name" 
                                class="form-control" 
                                type="text" 
                                name="full_name" 
                                autocomplete="off"
                                value="{{ $customer->vicidialUser->full_name or '' }}"  
                                {{ $editMode ? 'disabled' : '' }}>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group label-floating">
                            <label class="control-label">Monthly Invoice</label>
                            <input type="number" class="form-control" name="monthly_invoice_cycle" value="{{ $customer->monthly_invoice_cycle }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group label-floating">
                            <label class="control-label">Allowed Users</label>
							<input type="number" class="form-control" name="allowed_users" value="{{ $customer->allowed_users }}" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group label-floating">
                            <label class="control-label">Status</label>
                            <select name="status" class="form-control">
                            	<option value="1" {{ $customer->status ? 'selected' : '' }}>Active</option>
                            	<option value="0" {{ !$customer->status ? 'selected' : '' }}>Inactive</option>
                            </select>
                        </div>
                    </div>
                </div>

                @if($editMode)
                    <button type="submit" class="btn btn-info pull-right">Update</button>
                    <a href="/customer-detail" class="btn btn-default pull-right">Cancel</a>
                    {!! method_field('patch') !!}
                @else
                    <button type="submit" class="btn btn-info pull-right">Add</button>
                @endif
                <div class="clearfix"></div>

                {{ csrf_field() }}
            </form>
        </div>
    </div>
</div>