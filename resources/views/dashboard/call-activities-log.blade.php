<div class="card card-nav-tabs">
    <div class="card-header" data-background-color="blue">
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <span class="nav-tabs-title">Call Activities:</span>
                <ul class="nav nav-tabs" data-tabs="tabs">
                    <li class="active">
                        <a href="#profile" data-toggle="tab" @click="callActivities('all')">
                            <i class="material-icons">phone</i> All
                            <div class="ripple-container"></div>
                        </a>
                    </li>
                    <li class="">
                        <a href="#messages" data-toggle="tab" @click="callActivities('yes')">
                            <i class="material-icons">phone_in_talk</i> Answered
                            <div class="ripple-container"></div>
                        </a>
                    </li>
                    <li class="">
                        <a href="#settings" data-toggle="tab" @click="callActivities('no')">
                            <i class="material-icons">phone_missed</i> No Answer
                            <div class="ripple-container"></div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card-content row">
        <div class="tab-content col-md-9">
            <div class="tab-pane active" id="profile">
                <h3 class="text-warning text-center" v-show="this.callLogs.length==0 && !retrievingCallActitivies" style="display: none;">
                    No Call Activities
                </h3>

                <call-lead v-for="log in callLogs" :log="log" v-show="!retrievingCallActitivies"></call-lead>

                <div id="call-activities-loader" v-show="retrievingCallActitivies" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>

        <div id="knob-container" class="tab-panel col-md-3">
            <div class="knob-item">
                <div class="knob-label text-success">Answered %</div>
                <div id="knob-answered" class="knob-graph"></div>
            </div>
            <div class="knob-item">
                <div class="knob-label text-danger">No Answer %</div>
                <div id="knob-no-answer" class="knob-graph"></div>
            </div>
        </div>
    </div>
</div>