<div id="agent-realtime-view" class="card">
    <div class="card-header" data-background-color="blue">
        <a 
            href="#" 
            class="pull-right" 
            @click.prevent="realtimeAgents"
            data-toggle="tooltip" 
            data-placement="top" 
            data-original-title="Refresh">
            <i class="fa fa-refresh" v-bind:class="{ 'fa-spin': retrievingRealtimeAgentList }"></i>
        </a>
        <h4 class="title">Agent Realtime View</h4>
        <p class="category">List of logged in agents</p>
    </div>
    <div class="card-content table-responsive">
        <table class="table">
            <thead class="text-danger">
                <tr>
                    <th>#</th>
                    <th>Agent</th>
                    <th>Status</th>
                    <th>Customer Phone #</th>
                    <th>Duration</th>
                    <th>Campaign</th>
                </tr>
            </thead>
            <tbody>
                <tr
                    is="agent-realtime-list-item"
                    v-for="(agent, index) in realtimeAgentsList"
                    :agent="agent"
                    :index="index+1">
                </tr>
                <tr v-show="realtimeAgentsList.length==0" style="display: none;">
                    <td colspan="6" class="text-warning text-center">
                        <h1 class="big-fa"><i class="fa fa-podcast"></i></h1>
                        There are no agents logged in right now.
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>