@extends('layouts.master')


@section('content')

@include('dashboard.header')

<div class="row">
    <div class="col-lg-6 col-md-12">
        @include('dashboard.call-activities-log')
    </div>
    <div class="col-lg-6 col-md-12">
        @include('dashboard.call-statistics-graph')
    </div>
</div>



<div class="row">
    <div class="col-lg-12">
        @include('dashboard.agent-realtime-view')
    </div>
</div>



<div class="row">
    <div class="col-lg-12">
        @include('dashboard.agent-calls-breakdown')
    </div>
</div>
@endsection





















@section('vue_templates')
<template id="call-lead">
    <a class="tab-pane-group" href="javascript:void(0)" v-bind:class="{ 'answered': parseInt(log.talk_sec), 'no-answer': !parseInt(log.talk_sec) }">
      <i class="menu-icon fa" v-bind:class="{ 'fa-check-square': parseInt(log.talk_sec) > 0, 'fa-user-times': parseInt(log.talk_sec) == 0 }"></i>
      <div class="menu-info">
        <h4 class="control-sidebar-subheading">
            @{{ log.full_name }}
            <i class="fa fa-angle-double-right"></i> 
            @{{ log.campaign_name }}
            <i class="fa fa-angle-double-right"></i> 
            @{{ log.phone_number }}
        </h4>
        <p class="menu-meta">
            <i class="fa fa-clock-o"></i> 
            @{{ log.event_time }}, Talk Time: @{{ toTime }}, @{{ log.status }}
        </p>
      </div>
    </a>
</template>





<template id="agent-realtime-list-item">
    <tr>
        <td>@{{ index }}</td>
        <td>@{{ agent.full_name.toUpperCase() }}</td>
        <td v-html="status"></td>
        <td>@{{ customerPhone }}</td>
        <td>@{{ agent.calltime }}</td>
        <td>@{{ agent.campaign_name.toUpperCase() }}</td>
    </tr>
</template>





<template id="agent-calls-breakdown-table-template">
    <table id="agent-calls-breakdown-table" class="table table-striped">
        <thead>
            <tr>
                <th><strong>AGENTS</th>
                <th><strong>TOTAL</strong></th>
                <th v-for="(item, index) in table_info.data">
                    <strong>@{{ table_info.dispos_desc[index] }}</strong>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="(item, index, counter) in table_info.data_orig">
                <th>@{{ item.full_name }}</th>
                <td class="info">@{{ item.total_calls }}</td>
                <td v-for="(dispo, dispo_title) in table_info.data">
                    @{{ dispo[counter] }}
                </td>
            </tr>
            <tr>
                <th>TOTAL CALLS</th>
                <td class="info">@{{ dispo.grandTotal }}</td>
                <td v-for="total in dispo.dispoTotals" class="info">@{{ total }}</td>
            </tr>
        </tbody>
    </table>
</template>
@endsection














@section('custom_scripts')
<script src="/js/demo.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    // Javascript method's body can be found in assets/js/demos.js
    // demo.initDashboardPageCharts();
    $('[data-toggle="tooltip"]').tooltip();

});








var gaugeOptions = {
        chart: {
            type: 'solidgauge'
        },

        title: null,

        pane: {
            center: ['50%', '50%'],
            size: '100%',
            background: {
                innerRadius: '60%',
                outerRadius: '100%'
            }
        },

        yAxis: {
            stops: [
                [0, '#55BF3B'] // green
            ],
            labels: false,
            min: 0,
            max:100,
            showFirstLabel:false,
            showLastLabel:false,
            lineWidth: 0,
            tickWidth:0
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    borderWidth: 0
                }
            }
        },
        
        tooltip: {
            enabled: false
        },

        series: [{
            name: '',
            data: [0],
            dataLabels: {
                format: '<div style="font-size:16px;">{y}</span></div>',
                y:-10
            }
        }],

        credits: {
          enabled: false
        },

        exporting: false
    };





var callStatisticsOptions = {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: []
    },
    yAxis: {
        title: {
            text: ''
        }
    },
    tooltip: {
        headerFormat: '<h3 style="font-size:14px;font-weight:bold;">{point.key}</h3><br/><br/><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>'
    },
    plotOptions: {
        column: {
            pointPadding: 0.2
        }
    },
    series: [{
        name: 'Call Dispositions',
        data: []

    }],
    credits: {
      enabled: false
    },

    exporting: false
}




var callsBreakdownOptions = {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: []
    },
    yAxis: {
        title: {
            text: ''
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2
        }
    },
    series: [],
    credits: {
        enabled: false
    },
    exporting: false
}













    // Vue Start
    var vm = new Vue({
    el: '.main-panel',
    data: {
        retrievingData: false,

        // status totals
        modalStatus: 'false',
        retrievingStatusTotal: false,
        status: '{{ $selectedStatus }}',
        statusName: '{{ $statuses[$selectedStatus] }}',
        statusTotal: 0,
        
        // total values
        loggedInAgents: 0,
        totalCalls: 0,
        answeredCalls: 0,
        notAnsweredCalls: 0,
        talkTime: '00:00:00',

        postData: {
            startDate: '{{ $startDate }}',
            endDate: '{{ $endDate }}'
        },

        // gauge values
        gaugeOptions: gaugeOptions,
        knobAnswered: {},
        knobNoAnswer: {},

        // call activities
        retrievingCallActitivies: false,
        callLogs: [],

        // call statistics
        callStatisticsOptions: callStatisticsOptions,
        callstatisticsGraph: {},

        // realtime agents
        retrievingCallGraphData: false,
        callGraphData: {
            callStats: {
                data: [],
                label: []
            },
            callsBreakdown: {
                data: {},
                data_orig: {},
                label: []
            }
        },

        // live agents
        retrievingRealtimeAgentList: false,
        realtimeAgentsList: [],

        // agent calls breakdown
        callsBreakdownOptions: callsBreakdownOptions,
        callsBreakdownGraph: {}
    },





    methods: {
        refreshDashboard: function(){
            
            this.retrievingData = true;

            $.post('/dashboard/search', this.postData, function(data){
                this.retrievingData = false;
                this.loggedInAgents = data.loggedInAgents;
                this.totalCalls = parseInt(data.totalCalls);
                this.answeredCalls = parseInt(data.answeredCalls);
                this.notAnsweredCalls = parseInt(data.notAnsweredCalls);
                this.talkTime = data.talkTime;

                this.refreshKnob();
            }.bind(this));


            this.callActivities('all');
            this.callGraphs();
        },





        refreshKnob: function(){
            max = this.totalCalls == 0 ? 100 : this.totalCalls;
            gaugeOptions = {
              min: 0,
              max: max
            };

            this.knobAnswered.yAxis[0].update(gaugeOptions);
            this.knobNoAnswer.yAxis[0].update(gaugeOptions);

            var answerPoint = this.knobAnswered.series[0];
            // answerPoint.update(this.answeredCalls);
            answeredPoint = parseInt(this.answeredCalls);
            var answeredCallPercentage = ( answeredPoint / max ) * 100;
            answerPoint.update({
                                dataLabels: {
                                    format: '<div style="font-size:16px;">'+ answeredCallPercentage.toFixed(0) +'%</span></div>',
                                    y:-10
                                }
                            });
            answerPoint.points[0].update(this.answeredCalls);

            var noAnswerPoint = this.knobNoAnswer.series[0];
            // noAnswerPoint.update(this.notAnsweredCalls);
            notAnsweredPoint = parseInt(this.notAnsweredCalls);
            var notAnsweredCallPercentage = ( notAnsweredPoint / max ) * 100;
            noAnswerPoint.update({
                                dataLabels: {
                                    format: '<div style="font-size:16px;">'+ notAnsweredCallPercentage.toFixed(0) +'%</span></div>',
                                    y:-10
                                }
                            });
            noAnswerPoint.points[0].update(this.notAnsweredCalls);

        },





        callActivities: function(callType){
            this.retrievingCallActitivies = true;
            var data = {
                startDate: this.postData.startDate,
                endDate: this.postData.endDate,
                callType: callType
            }

            $.post('/dashboard/call-activities', data, function(data){
                this.retrievingCallActitivies = false;
                this.callLogs = data;
            }.bind(this));
        },





        callGraphs: function(){

            this.retrievingCallGraphData = true;
            $.post('/dashboard/call-graph-data', this.postData, function(data){
                this.retrievingCallGraphData = false;

                this.callGraphData = data;

                // this.callstatisticsGraph.update({
                //     xAxis: {
                //         categories: data.callStats.label
                //     },
                //     series: [{
                //         name: 'Call Dispositions',
                //         data: data.callStats.data
                //     }]
                // });


                // >>> Call Statistics Graph start
                while( this.callstatisticsGraph.series.length > 0 ) {
                    this.callstatisticsGraph.series[0].remove( false );
                }

                this.callstatisticsGraph.addSeries({
                    name: 'Call Dispositions',
                    data: data.callStats.data
                });

                this.callstatisticsGraph.update({
                    xAxis: {
                        categories: data.callStats.label
                    }
                }, false);
                this.callstatisticsGraph.redraw();

                // >>> Call Statistics Graph end



                // >>> Agent Calls Breakdown start
                while( this.callsBreakdownGraph.series.length > 0 ) {
                    this.callsBreakdownGraph.series[0].remove( false );
                }
                
                Object.keys(data.callsBreakdown.data).forEach(function(key){
                    var name = '['+ key +'] ' + data.callsBreakdown.dispos_desc[key];
                    this.callsBreakdownGraph.addSeries({
                        name: name.toUpperCase(),
                        data: data.callsBreakdown.data[key]
                    });
                }.bind(this));


                this.callsBreakdownGraph.update({
                    xAxis: {
                        categories: data.callsBreakdown.label
                    }
                }, false);
                this.callsBreakdownGraph.redraw();
                // <<< Agent Calls Breakdown end

            }.bind(this));
        },





        realtimeAgents: function(){
          this.retrievingRealtimeAgentList = true;
          $.post('/dashboard/realtime-agents', function(data){
            this.retrievingRealtimeAgentList = false;
            this.realtimeAgentsList = data;
            }.bind(this));  
        },





        changeStatus: function(){
            if(this.modalStatus=='false') return;

            this.status = this.modalStatus;
            this.statusName = '---';
            this.getStatusTotal();
            $('#myModal').modal('hide');
        },





        getStatusTotal: function(){
            this.retrievingStatusTotal = true;
            this.statusTotal = '<i class="fa fa-spinner fa-spin"></i>';
            $.getJSON('/dashboard/status-count/'+this.status, function(data){
                this.retrievingStatusTotal = false;
                this.statusName            = data.statusName;
                this.statusTotal           = data.total;
            }.bind(this));
        },





        tableToExcel: function(table, name, filename){
            let uri = 'data:application/vnd.ms-excel;base64,', 
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><title></title><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>', 
            base64 = function(s) { return window.btoa(decodeURIComponent(encodeURIComponent(s))) },         format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; })}

            if (!table.nodeType) table = document.getElementById(table)
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}

            var link = document.createElement('a');
            link.download = filename;
            link.href = uri + base64(format(template, ctx));
            link.click();
        }
    },





    mounted: function(){

        var self = this;



        $('#startDate').datepicker({
          format:'yyyy-mm-dd',
          autoclose:true,
        }).on('changeDate', function(ev) {
            self.postData.startDate = $('#startDate').val();
          $('#endDate').focus();
        });



        $('#endDate').datepicker({
          format:'yyyy-mm-dd',
          autoclose:true,
        }).on('changeDate', function(ev) {
            self.postData.endDate = $('#endDate').val();
        });



        // initialize knob
        this.knobAnswered = Highcharts.chart('knob-answered', this.gaugeOptions);

        var noAnswer = $.extend(true, {}, this.gaugeOptions);
        noAnswer.yAxis.stops[0] = [0, '#DF5353']
        this.knobNoAnswer = Highcharts.chart('knob-no-answer', noAnswer);
        // end knob



        // initialize call statistics graph
        this.callstatisticsGraph = Highcharts.chart('call-statistics', this.callStatisticsOptions);
        // end statistics graph



        // initialize agent calls breakdown
        this.callsBreakdownGraph = new Highcharts.chart('agent-calls-breakdown', this.callsBreakdownOptions);
        // end calls breakdown graph


        this.refreshDashboard();

        this.getStatusTotal();

        this.realtimeAgents();
        window.setInterval(function(){
            this.realtimeAgents();
        }.bind(this), 5000);
    },





    components: {
        'call-lead': {
            template: '#call-lead',
            props: ['log'],
            computed: {
                toTime: function () {
                    var sec_num = parseInt(this.log.talk_sec, 10); // don't forget the second param
                    var hours   = Math.floor(sec_num / 3600);
                    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
                    var seconds = sec_num - (hours * 3600) - (minutes * 60);

                    if (hours   < 10) {hours   = "0"+hours;}
                    if (minutes < 10) {minutes = "0"+minutes;}
                    if (seconds < 10) {seconds = "0"+seconds;}

                    return hours+':'+minutes+':'+seconds;
                },

                icon: function(){
                    // return (this.log.talk_sec) ? 'menu-icon fa fa-check-square' : 'menu-icon fa fa-user-times';
                    return ['menu-icon', 'fa', 'fa-check-square'];
                }
            }
        },





        'agent-realtime-list-item': {
            template: '#agent-realtime-list-item',
            props: ['agent', 'index'],
            computed: {
                status: function(){

                    status = this.agent.status.toUpperCase();
                    color = 'label-default';
                    statusColors = {
                        'INCALL' : 'label-success',
                        'HANGUP' : 'label-warning',
                        'PAUSED' : 'label-warning',
                        'WAITING' : 'label-success',
                    }


                    if(statusColors[status]) color = statusColors[status];
                    status = '<label class="label '+color+'">'+status+'</label>';

                    return status;
                },
                customerPhone: function(){
                    return this.agent.phone_number ? 
                                '+' + la.phone_code + '-' + la.phone_number : 
                                '+0-0000000000';
                }
            }
        },





        'agent-calls-breakdown-table-template': {
            template: '#agent-calls-breakdown-table-template',
            props: ['table_info'],
            computed: {
                dispo: function(){
                    var dispo = {
                        grandTotal: 0,
                        dispoTotals: []
                    };

                    Object.keys(this.table_info.data).forEach(function(key){
                        var sum = this.table_info.data[key].reduce(function(a,b) {
                            return a + b;
                        });

                        dispo.grandTotal += sum;
                        dispo.dispoTotals.push(sum);
                    }.bind(this));


                    return dispo;
                }
            }
        }
    }
});
</script>
@endsection