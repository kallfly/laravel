<div id="dashboard-dates" class="form-inline label-floating">
    <input 
        type="text" 
        id="startDate" 
        class="btn btn-info btn-sm datepicker" 
        name="start_date" 
        v-model="postData.startDate"
        data-toggle="tooltip" 
        data-placement="top" 
        data-original-title="Start Date" />
    <input 
        type="text" 
        id="endDate" 
        class="btn btn-info btn-sm datepicker"
        name="end_date"
        v-model="postData.endDate"
        data-toggle="tooltip" 
        data-placement="top" 
        data-original-title="End Date" />

    <button 
        class="btn btn-info btn-sm" 
        type="submit" 
        @click.prevent="refreshDashboard"
        data-toggle="tooltip" 
        data-placement="top" 
        data-original-title="Refresh dashboard base on the daterange">
        <i class="fa" v-bind:class="{ 'fa-refresh': !retrievingData, 'fa-spinner fa-spin': retrievingData }"></i>
    </button>
</div>





<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="orange">
                <i class="fa fa-user-times"></i>
            </div>
            <div class="card-content">
                <p class="category">Agents</p>
                <h3 class="title" v-html="loggedInAgents">0</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="fa fa-user-circle"></i>
                    Agents Logged In
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="green">
                <i class="fa fa-bar-chart"></i>
            </div>
            <div class="card-content">
                <p class="category">Total | Answered | No Answer</p>
                <h3 class="title">
                    <span v-html="totalCalls">0</span> | 
                    <span v-html="answeredCalls">0</span> | 
                    <span v-html="notAnsweredCalls">0</span>
                </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="fa fa-bar-chart"></i> Total Call Statistics
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="">
                <i class="fa fa-comments-o"></i>
            </div>
            <div class="card-content">
                <p class="category">Talk Time</p>
                <h3 class="title" v-html="talkTime">00:00:00</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="fa fa-line-chart"></i> Total Talk Time
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="red">
                <i class="fa fa-flag"></i>
            </div>
            <div class="card-content">
                <p class="category" v-html="statusName"></p>
                <h3 class="title">
                    <span data-toggle="tooltip"  data-placement="top"  data-original-title="Change Status">
                        <a 
                            href="#" 
                            data-toggle="modal" 
                            data-target="#myModal"
                            v-html="status"
                            style="color:rgba(0, 0, 0, 0.87);text-decoration:underline;"></a>
                    </span>:

                    <a 
                        href="#" 
                        v-html="statusTotal"
                        @click.prevent="getStatusTotal"
                        data-toggle="tooltip" 
                        data-placement="top" 
                        data-original-title="Click to Refresh"
                        style="color:rgba(0, 0, 0, 0.87);text-decoration:underline;">0</a>
                </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="fa fa-info"></i> Total <span v-html="statusName">
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Statuses</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group label-floating">
                    <label class="control-label">Select a main status to be displayed on the dashboard:</label>
                    <select name="campaign_allow_inbound" class="form-control" v-model="modalStatus">
                        <option value="false">Please Select</option>
                        @foreach($statuses as $status => $statusName)
                        <option value="{{ $status }}">{{ $status }} - {{ $statusName }}</option>
                        @endforeach
                    </select>
                    <span class="material-input"></span>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger" @click="changeStatus" v-bind:disabled="modalStatus=='false'">Save changes</button>
      </div>
    </div>
  </div>
</div>