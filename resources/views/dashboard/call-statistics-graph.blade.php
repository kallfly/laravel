<div class="card">
    <div class="card-header" data-background-color="blue">
    	<a 
    		href="#" 
    		class="pull-right" 
    		@click.prevent="callGraphs"
    		data-toggle="tooltip" 
    		data-placement="top" 
    		data-original-title="Refresh">
    		<i class="fa fa-refresh" v-bind:class="{ 'fa-spin': retrievingCallGraphData }"></i>
    	</a>
        <h4 class="title">Call Statistics</h4>
        <p class="category">Graph for Call Statistics</p>
    </div>
    <div class="card-content table-responsive" style="min-height:312px;">
    	<h3 class="text-warning text-center" v-show="!retrievingCallGraphData && callGraphData.callStats.data.length==0" style="display: none;">
            No Call Statistics
        </h3>
        <div id="call-statistics" style="width:100%;height:280px;"  v-show="callGraphData.callStats.data.length>0"></div>
    </div>
</div>