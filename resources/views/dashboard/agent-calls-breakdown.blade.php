<div class="card">
    <div class="card-header" data-background-color="blue">
    	<a 
    		href="#" 
    		class="pull-right" 
    		@click.prevent="callGraphs"
    		data-toggle="tooltip" 
    		data-placement="top" 
    		data-original-title="Refresh">
    		<i class="fa fa-refresh" v-bind:class="{ 'fa-spin': retrievingCallGraphData }"></i>
    	</a>
        <h4 class="title">Agent Calls Breakdown</h4>
        <p class="category">Graph for Agent Calls Breakdown</p>
    </div>
    <div class="card-content table-responsive" style="min-height:120px;">

    	<h3 class="text-warning text-center" v-show="!retrievingCallGraphData && callGraphData.callsBreakdown.data.length==0" style="display: none;">
            No Agent Calls Breakdown
        </h3>
        <div id="agent-calls-breakdown" v-show="callGraphData.callsBreakdown.label.length>0"></div>

		<div v-show="callGraphData.callsBreakdown.label.length>0">
	        <a href="#" class="pull-right text-info" @click.prevent="tableToExcel('agent-calls-breakdown-table', 'name', 'agent-breakdown.xls')">
	        	<i class="fa fa-file-excel-o"></i>
	        	Download CSV
	        </a>
	        <agent-calls-breakdown-table-template :table_info="callGraphData.callsBreakdown"></agent-calls-breakdown-table-template>
	    </div>
    </div>
</div>