@extends('layouts.master')





@section('content')
<div class="nav-center">
    <ul class="nav nav-pills nav-pills-info nav-pills-icons" role="tablist">
        <li class="active">
            <a href="#agent-hours" role="tab" data-toggle="tab">
                <i class="fa fa-clock-o"></i> Agent Hours
            </a>
        </li>
        <li>
            <a href="#agent-call-report" role="tab" data-toggle="tab">
                <i class="fa fa-phone"></i> Agent Call Report
            </a>
        </li>
        <li>
            <a href="#call-recordings" role="tab" data-toggle="tab">
               <i class="fa fa-file-audio-o"></i> Call Recordings
            </a>
        </li>
        <li>
            <a href="#performance" role="tab" data-toggle="tab">
                <i class="fa fa-dashboard"></i> Performance
            </a>
        </li>
        <li>
            <a href="#disposition" role="tab" data-toggle="tab">
                <i class="fa fa-exchange"></i> Dispo
            </a>
        </li>
        <li>
            <a href="#monitoring-agents" role="tab" data-toggle="tab">
                <i class="fa fa-eye"></i> Monitoring Agents
            </a>
        </li>
    </ul>
</div>





<div id="report-content" class="tab-content">
    <div class="tab-pane active" id="agent-hours">
        <div class="card">
        	<div class="card-content">
        		@include('reports.agent-hours')
        	</div>
        </div>
    </div>
    <div class="tab-pane" id="agent-call-report">
        <div class="card">
            <div class="card-content">
                @include('reports.agent-call-report')
            </div>
        </div>
    </div>
    <div class="tab-pane" id="call-recordings">
        <div class="card">
            <div class="card-content">
                @include('reports.call-recordings')
            </div>
        </div>
    </div>
    <div class="tab-pane" id="performance">
        <div class="card">
            <div class="card-content">
                @include('reports.performance')
            </div>
        </div>
    </div>
    <div class="tab-pane" id="disposition">
        <div class="card">
            <div class="card-content">
                @include('reports.disposition')
            </div>
        </div>
    </div>
    <div class="tab-pane" id="monitoring-agents">
        <div class="card">
            <div id="agent-realtime-view" class="card-content">
                @include('reports.live-monitoring')
            </div>
        </div>
    </div>
</div>
@endsection










@section('custom_scripts')
<script>


var barChartOptions = {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: []
    },
    yAxis: {
        title: {
            text: ''
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2
        }
    },
    series: [],
    credits: {
        enabled: false
    },
    exporting: false
}











new Vue({
    el: '.main-panel',
    data: {
        startDate: '{{ $startDate }}',
        endDate: '{{ $endDate }}',
        agents: [],
        campaigns: [],
        status: 'SALE',
        phoneMonitor: '',

        // agent work hours
        retrievingAgentWorkHours: false,
        agentWorkHours: [],

        // agent call report
        retrievingAgentCallReport: false,
        agentCallReports: [],
        agentCallReportsGraph: {},
        agentCallReportsOptions: barChartOptions,

        // call recordings
        retrievingCallRecordings: false,
        callRecordings: [],

        // performance
        retrievingPerformance: false,
        performanceAvg: [],
        performance: [],

        // dipositions
        retrievingDisposition: false,
        dispositionHasValues: false,
        dispositionGraph: {},
        dipositionOptions: barChartOptions,

        // live monitoring
        retrievingRealtimeAgentList: false,
        realtimeAgentsList: [],
    },





    methods: {

        getAgentHours: function() {

            var data = {
                startDate: this.startDate,
                endDate: this.endDate,
                agents: this.agents
            };


            this.retrievingAgentWorkHours = true;
            $.post('/agent-hours/ajax', data, function(data){
                this.retrievingAgentWorkHours = false;
                this.agentWorkHours = data;
            }.bind(this));

        },




        getAgentCallReport : function() {

            var self = this;

            var data = {
                    startDate: self.startDate,
                    endDate: self.endDate,
                    agents: self.agents,
                    campaigns: self.campaigns
                };

            self.retrievingAgentCallReport = true;
            $.post('/agent-call-report/ajax', data, function(data){
                self.retrievingAgentCallReport = false;
                self.agentCallReports = data;
            });



            while( self.agentCallReportsGraph.series.length > 0 ) {
                self.agentCallReportsGraph.series[0].remove( false );
            }

            
            $.post('/agent-call-report/ajax-graph', data, function(data){
                self.retrievingAgentCallReport = false;

                Object.keys(data.series).forEach(function(key){
                    self.agentCallReportsGraph.addSeries({
                        name: key,
                        data: data.series[key]
                    });
                });


                self.agentCallReportsGraph.update({
                    xAxis: {
                        categories: data.categories
                    }
                }, false);
                self.agentCallReportsGraph.redraw();

            });

        },





        getCallRecordings: function() {
            var data = {
                    startDate: this.startDate,
                    endDate: this.endDate,
                    agents: this.agents
                };

            this.retrievingCallRecordings = true;
            $.post('/call-recordings/ajax', data, function(data){
                this.retrievingCallRecordings = false;
                this.callRecordings = data;
            }.bind(this));
        },





        getPerformance: function(){
            var data = {
                    startDate: this.startDate,
                    endDate: this.endDate,
                    campaigns: this.campaigns
                };

            this.retrievingPerformance = true;
            $.post('/performance/ajax-average', data, function(data){
                this.retrievingPerformance = false;
                this.performanceAvg = data;
            }.bind(this));



            $.post('/performance/ajax', data, function(data){
                this.retrievingPerformance = false;
                this.performance = data;
            }.bind(this));
        },





        getDisposition: function() {
            var data = {
                    startDate: this.startDate,
                    endDate: this.endDate,
                    status: this.status
                };

            

            
            this.retrievingDisposition = true;
            $.post('/disposition/ajax', data, function(data){
                this.retrievingDisposition = false;
                this.dispositionHasValues = data.categories.length > 0 ? true : false;


                while( this.dispositionGraph.series.length > 0 ) {
                    this.dispositionGraph.series[0].remove( false );
                }


                Object.keys(data.series).forEach(function(key){
                    this.dispositionGraph.addSeries({
                        name: key,
                        data: data.series[key]
                    });
                }.bind(this));


                this.dispositionGraph.update({
                    xAxis: {
                        categories: data.categories
                    }
                }, false);
                this.dispositionGraph.redraw();

            }.bind(this));
        },





        getRealtimeAgents: function(){
          this.retrievingRealtimeAgentList = true;
          $.post('/dashboard/realtime-agents', function(data){
            this.retrievingRealtimeAgentList = false;
            this.realtimeAgentsList = data;
            }.bind(this));  
        },





        tableToExcel: function(table, name, filename){
            let uri = 'data:application/vnd.ms-excel;base64,', 
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><title></title><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>', 
            base64 = function(s) { return window.btoa(decodeURIComponent(encodeURIComponent(s))) },         format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; })}

            if (!table.nodeType) table = document.getElementById(table)
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}

            var link = document.createElement('a');
            link.download = filename;
            link.href = uri + base64(format(template, ctx));
            link.click();
        }

    },





    mounted: function(){
        var self = this;

        $('.startDate').datepicker({
            format:'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function(ev) {
            self.startDate = $(this).val();
        });


        $('.endDate').datepicker({
            format:'yyyy-mm-dd',
            autoclose: true
        })
        .on('changeDate', function(ev) {
            self.endDate = $(this).val();
        });


        $('.agentpicker').selectpicker().
        on('changed.bs.select', function (e) {
          
          self.agents = $(this).val();
          
          $('.agentpicker').val(self.agents);
          $('.agentpicker').selectpicker('refresh');

        });


        $('.campaignpicker').selectpicker().
        on('changed.bs.select', function (e) {
          
          self.campaigns = $(this).val();
          
          $('.campaignpicker').val(self.campaigns);
          $('.campaignpicker').selectpicker('refresh');
           
        });


        $('.statuspicker').selectpicker().
        on('changed.bs.select', function (e) {
          
          self.status = $(this).val();
          
          $('.statuspicker').val(self.status);
          $('.statuspicker').selectpicker('refresh');
           
        });


        this.agentCallReportsGraph = new Highcharts.chart('agent-call-report-graph', this.agentCallReportsOptions);
        this.dispositionGraph      = new Highcharts.chart('disposition-graph', this.agentCallReportsOptions);



        this.getAgentHours();


        this.getRealtimeAgents();
        window.setInterval(function(){
            this.getRealtimeAgents();
        }.bind(this), 5000);

    },





    components: {
        'agent-work-hours': {
            template: '#agent-work-hours',
            props: ['work_hours', 'user', 'start_date', 'end_date'],
            data: function(){
                return {
                    'startDate': this.start_date,
                    'endDate': this.end_date
                }
            }
        },





        'agent-call-report-table': {
            template: '#agent-call-report-table',
            props: ['logs']
        },





        'call-recordings': {
            template: '#call-recordings-table',
            props: ['logs', 'start_date', 'end_date'],
            data: function(){
                return {
                    'startDate': this.start_date,
                    'endDate': this.end_date
                }
            }
        },





        'performance': {
            template: '#performance-table',
            props: ['performance', 'performance_avg'],
            methods: {
                tableToExcel: function(table, name, filename){
                    let uri = 'data:application/vnd.ms-excel;base64,', 
                    template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><title></title><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>', 
                    base64 = function(s) { return window.btoa(decodeURIComponent(encodeURIComponent(s))) },         format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; })}

                    if (!table.nodeType) table = document.getElementById(table)
                    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}

                    var link = document.createElement('a');
                    link.download = filename;
                    link.href = uri + base64(format(template, ctx));
                    link.click();
                }
            }
        },





        'agent-realtime-list-item': {
            template: '#agent-realtime-list-item',
            props: ['agent', 'index'],
            computed: {
                status: function(){

                    status = this.agent.status.toUpperCase();
                    color = 'label-default';
                    statusColors = {
                        '3-WAY'   : 'label-default',
                        'DEAD'    : 'label-danger',
                        'DISPO'   : 'label-info',
                        'INCALL'  : 'label-success',
                        'HANGUP'  : 'label-warning',
                        'PAUSED'  : 'label-warning',
                        'WAITING' : 'label-success',
                    }


                    if(statusColors[status]) color = statusColors[status];
                    status = '<label class="label '+color+'">'+status+'</label>';

                    return status;
                },
                customerPhone: function(){
                    return this.agent.phone_number ? 
                                '+' + la.phone_code + '-' + la.phone_number : 
                                '+0-0000000000';
                },
                phone: function() {
                    var phone = this.agent.extension;
                    var arr = phone.split('/');

                    return arr[1];
                }
            },
            methods: {
                listen: function() {
                    alert('calling...');
                    // javascript:send_monitor(this.agent.conf_exten, this.agent.server_ip, 'MONITOR');
                }
            }
        }
    }
});
</script>
@endsection














@section('vue_templates')
<template id="agent-work-hours">
    <div class="user">
        <strong><i class="fa fa-user-circle"></i> <strong>@{{ user }}</strong></strong><br/>
        <strong><i class="fa fa-calendar"></i> <strong>@{{ startDate }} - @{{ endDate }}</strong></strong>
        <table class="table table-hover table-responsive">
            <thead>
                <tr>
                    <th class="text-info"><strong>User</strong></th>
                    <th class="text-info"><strong>Date</strong></th>
                    <th class="text-info"><strong>Login Time</strong></th>
                    <th class="text-info"><strong>Last Activity</strong></th>
                    <th class="text-info"><strong>Total Time</strong></th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="log in work_hours">
                    <td>@{{ user }}</td>
                    <td>@{{ log.date }}</td>
                    <td>@{{ log.firstLog }}</td>
                    <td>@{{ log.lastLog }}</td>
                    <td>@{{ log.total }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</template>





<template id="agent-call-report-table">
    <table id="agent-call-report-excel-table" class="table table-hover table-responsive" v-show="logs.length>0">
        <thead>
            <tr>
                <th class="text-info">#</th>
                <th class="text-info">Date Time</th>
                <th class="text-info">Phone #</th>
                <th class="text-info">Call Duration</th>
                <th class="text-info">Agent Code</th>
                <th class="text-info">Call Status</th>
                <th class="text-info">Campaign ID</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="(log, index) in logs">
                <td>@{{ index + 1 }}</td>   
                <td>@{{ log.event_time }}</td>
                <td>
                    <a v-bind:href="'/leads/'+log.lead_id">
                        @{{ log.phone_number }}
                    </a>
                </td>
                <td>@{{ log.talk_sec }}</td>
                <td>@{{ log.user }}</td>
                <td>@{{ log.status }}</td>
                <td>@{{ log.campaign_id }}</td>
            </tr>
        </tbody>
    </table>
</template>





<template id="call-recordings-table">
    <div class="user">
        <table 
            class="table table-hover table-responsive"
            v-show="logs.length>0">
            <thead>
                <tr>
                    <th class="text-info"><strong>#</strong></th>
                    <th class="text-info"><strong>Agent</strong></th>
                    <th class="text-info"><strong>Phone</strong></th>
                    <th class="text-info"><strong>Date</strong></th>
                    <th class="text-info"><strong>Campaign</strong></th>
                    <th class="text-info"><strong>Download</strong></th>
                    <th class="text-info"><strong>Audio</strong></th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(log, index) in logs">
                    <td>@{{ index+1 }}</td>
                    <td>@{{ log.full_name }}</td>
                    <td>@{{ log.phone }}</td>
                    <td>@{{ log.date }}</td>
                    <td>@{{ log.campaign_id }}</td>
                    <td class="text-center" v-html="log.download"></td>
                    <td>
                        <audio controls style="width:230px;">
                            <source v-bind:src="log.location" type='audio/mpeg'>
                            Your browser does not support the audio element.
                        </audio>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</template>





<template id="performance-table">
    <div class="performance">
        <strong><i class="fa fa-dashboard"></i> Ave Handling Time</strong>
        <table id="avg-handling-time-excel-table" class="table table-hover table-responsive">
            <thead>
                <tr>
                    <th class="text-info"><strong>Agent</strong></th>
                    <th class="text-info"><strong>Ave. Talk Time</strong></th>
                    <th class="text-info"><strong>Ave. Pause Time</strong></th>
                    <th class="text-info"><strong>Ave. Wait Time</strong></th>
                    <th class="text-info"><strong>Ave. Dispo Time</strong></th>
                    <th class="text-info"><strong>Ave. Dead Time</strong></th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="avg in performance_avg">
                    <td>@{{ avg.user }}</td>
                    <td>@{{ avg.avg_talk_time }}</td>
                    <td>@{{ avg.avg_pause_time }}</td>
                    <td>@{{ avg.avg_wait_time }}</td>
                    <td>@{{ avg.avg_dispo_time }}</td>
                    <td>@{{ avg.avg_dead_time }}</td>
                </tr>
            </tbody>
        </table>
        <a 
            @click.prevent="tableToExcel('avg-handling-time-excel-table', 'Average_Handling_Time', 'avg-handing-time.xls')" 
            href="#" 
            class="btn btn-success pull-right">
            Export to CSV
        </a>


        <div class="clearfix" style="height:50px;clear:both;"></div>



        <strong><i class="fa fa-dashboard"></i> Agent Calls</strong>
        <table id="agent-calls-excel-table" class="table table-hover table-responsive">
            <thead>
                <tr>
                    <th class="text-info"><strong>Agent</strong></th>
                    <th 
                        class="text-info"
                        v-for="(value, status) in performance.statuses">
                        <strong>@{{ status }}</strong>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(log, user) in performance.logs">
                    <td>@{{ user }}</td>
                    <td v-for="(count, status) in log">@{{ count }}</td>
                </tr>
            </tbody>
        </table>
        <a
            @click.prevent="tableToExcel('agent-calls-excel-table', 'Agent_Calls', 'agent-calls.xls')" 
            href="#" 
            class="btn btn-success pull-right">
            Export to CSV
        </a>

    </div>
</template>





<template id="agent-realtime-list-item">
    <tr>
        <td v-html="status"></td>
        <td>@{{ agent.full_name.toUpperCase() }}</td>
        <td>
            <a class="text-info" @click.prevent="listen" href="#">
                <strong>
                    <i class="fa fa-assistive-listening-systems" aria-hidden="true"></i>
                    LISTEN
                </strong>
            </a>
        </td>
        <td>@{{ phone }}</td>
        <td>@{{ customerPhone }}</td>
        <td>@{{ agent.calltime }}</td>
        <td>@{{ agent.campaign_name.toUpperCase() }}</td>
    </tr>
</template>

@endsection