<div class="row">
	<div class="col-md-12">
		<h3><i class="fa fa-dashboard"></i> Performance</h3>
	</div>
	<div class="col-md-3">
		<form id="agent-hours-form" class="form" action="#" @submit.prevent="getPerformance">
			<div class="form-group label-floating">
				<label class="control-label">Date From</label>
				<input type="text" id="startDate" class="form-control startDate" v-model="startDate" name="campaign_name">
				<span class="material-input"></span>
				<span class="material-input"></span>
			</div>
			<div class="form-group label-floating">
				<label class="control-label">Date To</label>
				<input type="text" id="startDate" class="form-control endDate" v-model="endDate" name="campaign_name">
				<span class="material-input"></span>
				<span class="material-input"></span>
			</div>
			<div class="form-group label-floating">
				<label class="control-label">Campaigns</label>
			</div>
			<select class="form-control campaignpicker selectpicker" multiple data-live-search="true">
				<option value="all" selected="selected">All Campaigns</option>
				@foreach($campaigns as $campaign)
				<option value="{{ $campaign->campaign_id }}">{{ $campaign->campaign_name }}</option>
				@endforeach
			</select>

			<br/><br/>
			<button type="submit" class="btn btn-info pull-right">View Report<div class="ripple-container"></div></button>

			<div class="clearfix"></div>
		</form>	
	</div>



	<div class="col-md-9">

		<div 
			class="alert alert-warning"
			style="display:none;"
			v-show="!retrievingPerformance && performanceAvg.length==0">
			<div class="container-fluid">
				<div class="alert-icon">
					<i class="material-icons">warning</i>
				</div>
				<strong>No Results Found:</strong> Hey, it looks like there were no performance report during this period.
			</div>
		</div>



		<h1
			class="text-center"
			v-show="retrievingPerformance">
			<i class="fa fa-spinner fa-spin"></i>
		</h1>



		<performance 
			:performance_avg="performanceAvg"
			:performance="performance"
			v-show="performanceAvg.length>0"></performance>

	</div>
</div>