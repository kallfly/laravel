<div class="row">
	<div class="col-md-12">
		<h3><i class="fa fa-exchange"></i> Disposition</h3>
	</div>
	<div class="col-md-3">
		<form id="agent-hours-form" class="form" action="#" @submit.prevent="getDisposition">
			<div class="form-group label-floating">
				<label class="control-label">Date From</label>
				<input type="text" id="startDate" class="form-control startDate" v-model="startDate" name="campaign_name">
				<span class="material-input"></span>
				<span class="material-input"></span>
			</div>
			<div class="form-group label-floating">
				<label class="control-label">Date To</label>
				<input type="text" id="startDate" class="form-control endDate" v-model="endDate" name="campaign_name">
				<span class="material-input"></span>
				<span class="material-input"></span>
			</div>
			<div class="form-group label-floating">
				<label class="control-label">Campaigns</label>
			</div>
			<select class="form-control statuspicker selectpicker" data-live-search="true" required>
				@foreach($statuses as $status => $statusName)
				<option 
					value="{{ $status }}"
					{{ $status == 'SALE' ? 'selected' : '' }}>{{ $statusName }}</option>
				@endforeach
			</select>

			<br/><br/>
			<button type="submit" class="btn btn-info pull-right">View Report<div class="ripple-container"></div></button>

			<div class="clearfix"></div>
		</form>	
	</div>



	<div class="col-md-9">

		<div 
			class="alert alert-warning"
			style="display:none;"
			v-show="!retrievingDisposition && !dispositionHasValues">
			<div class="container-fluid">
				<div class="alert-icon">
					<i class="material-icons">warning</i>
				</div>
				<strong>No Results Found:</strong> Hey, it looks like there were no disposition report during this period.
			</div>
		</div>



		<h1
			class="text-center"
			v-show="retrievingDisposition">
			<i class="fa fa-spinner fa-spin"></i>
		</h1>


		<div id="disposition-graph"></div>

	</div>
</div>