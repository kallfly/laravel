<div class="row">
	<div class="col-md-12">
		<h3><i class="fa fa-clock-o"></i> Agent Work Hours</h3>
	</div>
	<div class="col-md-3">
		<form id="agent-hours-form" class="form" action="#" @submit.prevent="getAgentHours">
			<div class="form-group label-floating">
				<label class="control-label">Date From</label>
				<input type="text" id="startDate" class="form-control startDate" v-model="startDate" name="campaign_name">
				<span class="material-input"></span>
				<span class="material-input"></span>
			</div>
			<div class="form-group label-floating">
				<label class="control-label">Date To</label>
				<input type="text" id="startDate" class="form-control endDate" v-model="endDate" name="campaign_name">
				<span class="material-input"></span>
				<span class="material-input"></span>
			</div>
			<div class="form-group label-floating">
				<label class="control-label">Select Agents</label>
			</div>
			<select class="form-control agentpicker selectpicker" multiple data-live-search="true">
				<option value="all" selected="selected">All Agents</option>
				@foreach($agents as $agent)
				<option value="{{ $agent->user_id }}">{{ $agent->full_name }}</option>
				@endforeach
			</select>

			<br/><br/>
			<button type="submit" class="btn btn-info pull-right">View Report<div class="ripple-container"></div></button>

			<div class="clearfix"></div>
		</form>	
	</div>



	<div class="col-md-9">
		<div 
			class="alert alert-warning"
			style="display:none;"
			v-show="!retrievingAgentWorkHours && agentWorkHours.length==0">
			<div class="container-fluid">
				<div class="alert-icon">
					<i class="material-icons">warning</i>
				</div>
				<strong>No Results Found:</strong> Hey, it looks like there were no work done during this period.
			</div>
		</div>



		<h1
			class="text-center"
			v-show="retrievingAgentWorkHours">
			<i class="fa fa-spinner fa-spin"></i>
		</h1>



		<agent-work-hours 
			v-for="(workHours, user) in agentWorkHours" 
			:user="user" 
			:work_hours="workHours"
			:start_date="startDate"
			:end_date="endDate"></agent-work-hours>


		<a 
			@click.prevent="tableToExcel('agent-work-hours-excel-table', 'Agent_Work_Hours', 'agent-work-hours.xls')"
			href="#" 
			class="btn btn-success pull-right"
			style="display:none;"
			v-show="!retrievingAgentWorkHours && agentWorkHours.length!=0">
			Export to CSV
		</a>


		<table id="agent-work-hours-excel-table" class="table table-hover table-responsive excel-table">
			<template v-for="(workHours, user) in agentWorkHours">
				<tr>
					<td><strong>User</strong></td>
					<td>@{{ user }}</td>
	    		</tr>
	    		<tr>
	    			<td><strong>Date</strong></td>
	    			<td>@{{ startDate }} - @{{ endDate }}</td>
				</tr>
				<tr>
					<th class="text-info"><strong>User</strong></th>
	                <th class="text-info"><strong>Date</strong></th>
	                <th class="text-info"><strong>Login Time</strong></th>
	                <th class="text-info"><strong>Last Activity</strong></th>
	                <th class="text-info"><strong>Total Time</strong></th>
				</tr>
				<tr v-for="log in workHours">
                    <td>@{{ user }}</td>
                    <td>@{{ log.date }}</td>
                    <td>@{{ log.firstLog }}</td>
                    <td>@{{ log.lastLog }}</td>
                    <td>@{{ log.total }}</td>
                </tr>
                <tr>
                	<td></td>
                </tr>
                <tr>
                	<td></td>
                </tr>
			</template>
		</table>

	</div>
</div>