<div class="row">
	<div class="col-md-12">
		<h3><i class="fa fa-phone"></i> Agents Call Report</h3>
	</div>
	<div class="col-md-3">
		<form id="agent-hours-form" class="form" action="#" @submit.prevent="getAgentCallReport">
			<div class="form-group label-floating">
				<label class="control-label">Date From</label>
				<input type="text" id="startDate" class="form-control startDate" v-model="startDate" name="campaign_name">
				<span class="material-input"></span>
				<span class="material-input"></span>
			</div>
			<div class="form-group label-floating">
				<label class="control-label">Date To</label>
				<input type="text" id="startDate" class="form-control endDate" v-model="endDate" name="campaign_name">
				<span class="material-input"></span>
				<span class="material-input"></span>
			</div>
			<div class="form-group label-floating">
				<label class="control-label">Select Agents</label>
			</div>
			<select class="form-control agentpicker selectpicker" multiple data-live-search="true">
				<option value="all" selected="selected">All Agents</option>
				@foreach($agents as $agent)
				<option value="{{ $agent->user_id }}">{{ $agent->full_name }}</option>
				@endforeach
			</select>


			<div class="clearfix" style="height:10px;"></div>


			<div class="form-group label-floating">
				<label class="control-label">Campaigns</label>
			</div>
			<select class="form-control campaignpicker selectpicker" multiple data-live-search="true">
				<option value="all" selected="selected">All Campaigns</option>
				@foreach($campaigns as $campaign)
				<option value="{{ $campaign->campaign_id }}">{{ $campaign->campaign_name }}</option>
				@endforeach
			</select>

			<br/><br/>
			<button type="submit" class="btn btn-info pull-right">View Report<div class="ripple-container"></div></button>

			<div class="clearfix"></div>
		</form>	
	</div>



	<div class="col-md-9">
		<div 
			class="alert alert-warning"
			style="display:none;"
			v-show="!retrievingAgentCallReport && agentCallReports.length==0">
			<div class="container-fluid">
				<div class="alert-icon">
					<i class="material-icons">warning</i>
				</div>
				<strong>No Results Found:</strong> Hey, it looks like there were no work done during this period.
			</div>
		</div>



		<h1
			class="text-center"
			v-show="retrievingAgentCallReport">
			<i class="fa fa-spinner fa-spin"></i>
		</h1>


        <div id="agent-call-report-graph"></div>



		<agent-call-report-table :logs="agentCallReports"></agent-call-report-table>

		<a 
			@click.prevent="tableToExcel('agent-call-report-excel-table', 'Agent_Call_Report', 'agent-call-report.xls')"
			href="#" 
			class="btn btn-success pull-right"
			style="display:none;"
			v-show="!retrievingAgentCallReport && agentCallReports.length!=0">
			Export to CSV
		</a>
	</div>
</div>