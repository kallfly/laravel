<h3><i class="fa fa-eye"></i> Monitoring Agents</h3>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <strong>LEGEND:</strong> 
            <span class="label label-default">3-WAY</span>
            <span class="label label-danger">DEAD</span>
            <span class="label label-info">DISPO</span>
            <span class="label label-warning">PAUSED</span>
            <span class="label label-success">IN-CALL</span>
        </div>
    </div>

    <div class="col-md-offset-5 col-md-3">
        <div class="form-group label-floating">
            <label class="control-label">Input phone number to listen to live a call:</label>

            <input 
                type="text" 
                class="form-control" 
                name="campaign_name"
                v-model="phoneMonitor" />

            <span class="material-input"></span>
            <span class="material-input"></span>
        </div>
    </div>
</div>

<table class="table table-hover table-responsive">
    <thead class="text-danger">
        <tr>
            <th class="text-info"><strong>Status</strong></th>
            <th class="text-info"><strong>User</strong></th>
            <th class="text-info"><strong>Action</strong></th>
            <th class="text-info"><strong>Phone</strong></th>
            <th class="text-info"><strong>Customer Phone</strong></th>
            <th class="text-info"><strong>Time</strong></th>
            <th class="text-info"><strong>Campaign</strong></th>
        </tr>
    </thead>
    <tbody>
        <tr
            is="agent-realtime-list-item"
            v-for="(agent, index) in realtimeAgentsList"
            :agent="agent"
            :phone_monitor="phoneMonitor">
        </tr>
        <tr v-show="realtimeAgentsList.length==0" style="display: none;">
            <td colspan="6" class="text-warning text-center">
                <h1 class="big-fa"><i class="fa fa-podcast"></i></h1>
                There are no agents logged in right now.
            </td>
        </tr>
    </tbody>
</table>

<br/><br/>

<div class="text-right text-info" v-show="realtimeAgentsList.length>0"><strong>There are @{{ realtimeAgentsList.length }} Agent(s) logged in the Campaign</strong></div>