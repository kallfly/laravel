<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Campaign;
use App\Lists;


class KallflyList extends Model
{
    protected $table 	  = 'vicidial_list';
    protected $primaryKey = 'lead_id';
    protected $fillable = [
                            'title',
                            'first_name',
                            'last_name',
                            'middle_initial',
                            
                            'address1',
                            'address2',
                            'address3',
                            'city',
                            'state',
                            'postal_code',
                            'province',
                            'country_code',

                            'phone_code',
                            'phone_number',
                            'alt_phone',
                            'email',

                            'security_phrase',
                            'vendor_lead_code',
                            'rank',
                            'status',
                            'owner',
                            'comments'
                        ];
    
    public $timestamps = false;




    public static function getDialableLeads(Campaign $campaign)
    {
    	$lists = $campaign->getActiveLists();
		$listsArray = array_column($lists->toArray(), 'list_id');
		$dialablesStatusesArray = $campaign->getDialableStatuses();


    	return self::whereIn('list_id', $listsArray)
	    			->whereIn('status', $dialablesStatusesArray)
	    			->where('called_since_last_reset', 'N')
	    			->get();
    }





    public function list()
    {
        return $this->belongsTo(Lists::class, 'list_id', 'list_id');
    }





    public function callRecordings()
    {
        return $this->hasMany(\App\CallRecording::class, 'lead_id', 'lead_id');
    }





    public function logs()
    {
        return $this->hasMany(\App\Log::class, 'lead_id', 'lead_id');
    }

}
