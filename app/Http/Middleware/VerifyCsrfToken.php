<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'dashboard/*',
        'agent-hours/*',
        'agent-call-report/*',
        'call-recordings/*',
        'performance/*',
        'disposition/*',
        'leads-loader/*',
        'cdr/*'
    ];
}
