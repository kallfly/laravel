<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Lists;
use \App\UserGroup;
use \App\Campaign;
use \App\KallflyList;
use \App\Status;
use \App\PhoneCode;

class LeadsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }





    public function search()
    {
        $data['title'] = 'Lead Search';

        $data['statusOptions'] = Status::getStatusOptions();


        return view('lead.search', $data);
    }





    public function searchAjax()
    {
        $filters['phone_number'] = request()->query('phone');
        $filters['lead_id']      = request()->query('lead_id');
        $filters['list_id']      = request()->query('list_id');
        $filters['first_name']   = request()->query('first_name');
        $filters['last_name']    = request()->query('last_name');
        $filters['user']         = request()->query('user');
        
        $filters['status'] = request()->query('status') == 'none' ? '' : request()->query('status');



        $campaignIds = UserGroup::getAllowedCampaignIds();
        $listIds = Lists::select('list_id')
                        ->whereIn('campaign_id', $campaignIds)
                        ->get()
                        ->toArray();

        $query = \DB::table('vicidial_list as lead')
                ->selectRaw('lead.phone_number, lead.lead_id, lead.status, lists.list_name, lead.phone_code, lead.first_name, lead.last_name, lead.user')
                ->join('vicidial_lists as lists', 'lists.list_id', 'lead.list_id')
                ->whereIn('lists.list_id', $listIds)
                ->whereNotNull('lead_id')
                ->limit(1000);



        $filters = array_filter($filters);
        if(count($filters)==0) return array();
        foreach($filters as $column => $value)
        {
            $query->where('lead.'.$column, $value);
        }



        $leads = $query->get();

        return $leads;
    }





    public function view(KallflyList $lead)
    {
        $data['title'] = 'Lead Record Modification';

        $data['lead'] = $lead;

        $data['statusOptions'] = Status::getStatusOptions();

        return view('lead.view', $data);
    }





    public function update(KallflyList $lead)
    {
        $request = request()->all();
        $lead->update(request()->all());

        \Session::flash('flash_message', 'Lead ' . $lead->lead_id . ' successfully updated.');

        return redirect(url()->previous());
    }

}