<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\CallRecording;
use \App\User;

class CallRecordingsController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }





    public function ajax()
    {
    	$startDate 	 = request('startDate');
    	$endDate   	 = request('endDate');
		$agentIds 	 = request('agents', ['all']);


		$agentGroup = User::getUserGroup('agent');
		$agentsList = User::getAgents();



		$callRecordings = \DB::table('recording_log as recording')
							->join('vicidial_users as user', 'user.user', 'recording.user')
							->select(
								'user.full_name',
								'recording.recording_id', 
								'recording.user', 
								'recording.start_time', 
								'recording.length_in_sec', 
								'recording.filename', 
								'recording.location')

							->where('start_time', '>=', $startDate)
							->where('start_time', '<=', $endDate)

							->where('user_group', $agentGroup)
                            ->where('active', 'Y');



		if(!in_array('all', $agentIds)) $callRecordings->whereIn('user.user_id', $agentIds);

		$callRecordings = $callRecordings->get();


		$response = array();
		foreach($callRecordings as $record)
		{
			$file = explode('_', $record->filename);
			$phone = $file[1];
			$campaign = $file[3];


			$temp = array();
			$temp['full_name'] 	 = $record->full_name;
			$temp['phone'] 		 = $phone;
			$temp['date'] 		 = $record->start_time;
			$temp['campaign_id'] = $campaign;
			$temp['download'] 	 = "<a class='text-info' href='".$record->location."' target = '_blank'><i class='fa fa-download'></i></a>";
			$temp['location'] 	 = $record->location;

			$response[] = $temp;
		}


		return $response;
    }
}
