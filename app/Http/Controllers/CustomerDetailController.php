<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\CustomerDetail;

class CustomerDetailController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $data['title'] = "Schedule Invoice";
        $data['editMode'] = false;

        $data['form']['action'] = '/customer-detail';

        $customer = new CustomerDetail();
        $customer->status = 1;
        $data['customer'] = $customer;
        $data['customers'] = CustomerDetail::latest()->get();

    	return view('customer-detail.master', $data);
    }


    public function edit(CustomerDetail $customer){
        $data['title'] = "Schedule Invoice";
        $data['editMode'] = true;

        $data['form']['action'] = '/customer-detail/'.$customer->id;

        $data['customer'] = $customer;
        $data['customers'] = CustomerDetail::latest()->get();

        return view('customer-detail.master', $data);
    }


    public function update(CustomerDetail $customer){
        $this->validate(request(),[
            'monthly_invoice_cycle' => 'required',
            'allowed_users' => 'required'
        ]);


        $customer->monthly_invoice_cycle = request('monthly_invoice_cycle');
        $customer->allowed_users = request('allowed_users');
        $customer->status = request('status');
        $customer->save();

        \Session::flash('flash_message','Schedule Successfully Updated.');

        return redirect('/customer-detail');
    }


    public function store(){
    	
    	$this->validate(request(),[
    		'customer_id' => 'required',
    		'monthly_invoice_cycle' => 'required',
    		'allowed_users' => 'required',
            'customer_id' => 'required'
    	]);

    	CustomerDetail::create(request()->all());

        \Session::flash('flash_message','Customer Successfully Scheduled.');

    	return redirect('/customer-detail');
    }


    public function delete(CustomerDetail $customer){

        \Session::flash('flash_delete_message', 'Schedule for Customer '. $customer->customer_id . ' Deleted!');
        
        $customer->delete();

        return redirect('/customer-detail');
    }
}
