<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campaign;
use App\UserGroup;
use App\Lists;
use App\KallflyList;
use App\Status;
use App\CampaignStatus;

class CampaignController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}
    
	public function index()
	{
		$data['title'] = 'Campaign List';
		$data['editMode'] = false;
		$data['form'] = [
            'action' => '/campaigns'
        ];


		$query = Campaign::select('campaign_id','campaign_name','active','dial_method','auto_dial_level','lead_order','dial_statuses');


        $campaignIds = UserGroup::getAllowedCampaignIds();
        if( $campaignIds!==false ) $query->whereIn('campaign_id', $campaignIds);


		$data['campaign'] = new Campaign;
		$data['campaigns'] = $query->get();



		return view('campaign.master', $data);
	}




	public function store()
	{

		$this->validate(request(),[
			'campaign_name' => 'required',
			'campaign_allow_inbound' => 'not_in:false',
			'dial_method' => 'not_in:false',
			'auto_dial_level' => 'not_in:false',
			'next_agent_call' => 'not_in:false',
			'campaign_cid' => 'required',
			'campaign_recording' => 'not_in:false'
		]);



		$campaign = new Campaign;
		$campaign->allow_closers 		 	 = 'Y';
		$campaign->lead_order 			 	 = 'UP COUNT';
		$campaign->hopper_level 		 	 = 100;
		$campaign->local_call_time 		 	 = '24hours';
		$campaign->dial_prefix 			 	 = 7243;
		$campaign->manual_dial_prefix 	 	 = 7243;
		$campaign->use_custom_cid 		 	 = 'Y';
		$campaign->campaign_rec_filename 	 = 'FULLDATE_CUSTPHONE_AGENT_CAMPAIGN';
		$campaign->agent_lead_search 	 	 = 'ENABLED';
		$campaign->timer_action_seconds  	 = 1;
		$campaign->scheduled_callbacks   	 = 'Y';
		$campaign->scheduled_callbacks_alert = 'NONE';
		$campaign->scheduled_callbacks_count = 'ALL_ACTIVE';
		$campaign->my_callback_option 		 = 'CHECKED';
		$campaign->drop_call_seconds 		 = '5';
		$campaign->drop_action 				 = 'MESSAGE';
		$campaign->use_internal_dnc 		 = 'Y';
		$campaign->use_campaign_dnc 		 = 'Y';
		$campaign->manual_dial_list_id 		 = '10018';


		$campaignId = Campaign::incrementNewCampaignId();
		$campaign->campaign_id 			  = $campaignId;
		$campaign->campaign_name 		  = request('campaign_name');
		$campaign->web_form_address 	  = request('web_form_address');
		$campaign->campaign_allow_inbound = request('campaign_allow_inbound');
		$campaign->dial_method 			  = request('dial_method');
		$campaign->auto_dial_level 		  = request('auto_dial_level');
		$campaign->next_agent_call 		  = request('next_agent_call');
		$campaign->campaign_cid 		  = request('campaign_cid');
		$campaign->campaign_recording 	  = request('campaign_recording');
		$campaign->active 	  			  = request('active');


		$campaign->save();





		$allowedCampaignIds = UserGroup::addToAllowedCampaignIds($campaignId);

        $userGroup = UserGroup::where('user_group', auth()->user()->user_group)->first();
        $userGroup->allowed_campaigns = $allowedCampaignIds;
        $userGroup->save();





		\Session::flash('flash_message', request('campaign_name') . 'successfully added.');

    	return redirect(url()->previous());
	}




	public function edit(Campaign $campaign)
	{
		$data['title'] = 'Campaign List';
		$data['editMode'] = true;
		$data['form'] = [
            'action' => '/campaigns/'.$campaign->campaign_id
        ];


		$query = Campaign::select('campaign_id','campaign_name','active','dial_method','auto_dial_level','lead_order','dial_statuses');


        $campaignIds = UserGroup::getAllowedCampaignIds();
        if( $campaignIds!==false ) $query->whereIn('campaign_id', $campaignIds);


		$data['campaign'] = $campaign;
		$data['campaigns'] = $query->get();

		$dialables = $campaign->getDialableStatuses();
		$data['dialables'] = $dialables;
		// $dialableLeads = KallflyList::getDialableLeads($campaign);
		$data['dialableLeadsCount'] = 0; //count($dialableLeads);
		$data['dialablesListCount'] = array();
		$data['dialableList'] = $campaign->getDialablesList();



		$statusList = Status::select('status', 'status_name')->get()->toArray();
		$campaignStatusList = CampaignStatus::select('status', 'status_name')->whereIn('campaign_id', $campaignIds)->get()->toArray();
		

		$statusOptions = array_merge($statusList, $campaignStatusList);
		$data['statusOptions'] = array();
		foreach($statusOptions as $s)
		{
			$status = strtoupper($s['status']);
			$name   = ucwords($s['status_name']);

			// $statusFilter = array_column($dialableLeads->toArray(), 'status');
			$statusFilter = array();
			if( ! in_array($status, $dialables)) $data['statusOptions'][$status] = $name;
		}

		return view('campaign.edit', $data);
	}





	public function update(Campaign $campaign)
	{
		$this->validate(request(), [
			'campaign_name' => 'required',
			'campaign_allow_inbound' => 'not_in:false',
			'dial_method' => 'not_in:false',
			'auto_dial_level' => 'not_in:false',
			'next_agent_call' => 'not_in:false',
			'campaign_cid' => 'required',
			'campaign_recording' => 'not_in:false'
		]);


		$campaign->campaign_name 		  = request('campaign_name');
		$campaign->web_form_address 	  = request('web_form_address');
		$campaign->campaign_allow_inbound = request('campaign_allow_inbound');
		$campaign->dial_method 			  = request('dial_method');
		$campaign->auto_dial_level 		  = request('auto_dial_level');
		$campaign->next_agent_call 		  = request('next_agent_call');
		$campaign->campaign_cid 		  = request('campaign_cid');
		$campaign->campaign_recording 	  = request('campaign_recording');
		$campaign->active 	  			  = request('active');

		$campaign->save();

		\Session::flash('flash_message', request('campaign_name') . 'successfully added.');
    	
    	return redirect('/campaigns');
	}





	public function addStatus(Campaign $campaign)
	{
		$newStatus = request('status');
		$dialables = $campaign->getDialableStatuses();


		$dialables[] = $newStatus;

		$strStatus = implode(' ', $dialables);
		$strStatus .= ' -';

		$campaign->dial_statuses = $strStatus;
		$campaign->save();

		return redirect(url()->previous());
	}






	public function deleteStatus(Campaign $campaign)
	{
		$toDelete = request('status');
		$dialables = $campaign->getDialableStatuses();


		$dialables = array_diff($dialables, array($toDelete));
		$strStatus = implode(' ', $dialables);
		$strStatus .= ' -';

		$campaign->dial_statuses = $strStatus;
		$campaign->save();

		return redirect(url()->previous());
	}
}
