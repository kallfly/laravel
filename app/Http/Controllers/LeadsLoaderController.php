<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

use App\Events\LeadInserted;

use \App\Lists;
use \App\KallflyList;
use \App\PhoneCode;

class LeadsLoaderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }





    public function index()
    {
        $data['title'] = 'Loader';
        $data['user'] = auth()->user();

        $data['lists'] = Lists::getUserList();
        $data['phoneCodes'] = PhoneCode::select('country_code', 'country')->distinct()->get();
        return view('lead.loader.master', $data);
    }





    public function upload(Request $request)
    {
        $path = $request->file('lead_file')->store('public/lead-files');
        return $data = array('success' => true, 'path' => $path);
    }





    public function getFileColumns()
    {
    	// $file = 'public/lead-files/5Tf3i5AMiYJ5kVbWlqgRVVMiTVd1JDzxjf2Muw5U.txt';
        
        $file = request('file');

    	$file = Storage::get($file);
    	$content = strtok($file, PHP_EOL);
    	$columns = preg_split('/[\t]/', $content);

    	return $columns;
    }





    public function processLeads()
    {
        $file = request('file');
        $configColumns = request('columns');

        $listIdOverride = request('listIdOverride', 'in_file');
        $phoneCodeOverride = request('phoneCodeOverride', 'in_file');
        $leadDuplicateCheck = request('leadDuplicateCheck', 'NONE');

        

        $file = Storage::get($file);
        $content = explode(PHP_EOL, $file);
        array_shift($content);

        

        foreach($content as $row)
        {
            // get the index value of the configColumns data
            $setColumn = array();
            foreach($configColumns as $clmObj)
            {
                if($clmObj['value'] != null) $setColumn[ $clmObj['column_name'] ] = $clmObj['value'];
            }

            $rowData = preg_split('/[\t]/', $row);






            // set important variables for validations
            $phoneNumber = $this->getPostColumn('phone_number', $setColumn, $rowData);
            $phoneCode   = $this->getPostColumn('phoneCode', $setColumn, $rowData);
            $listId      = $this->getPostColumn('list_id', $setColumn, $rowData);





            // overrides
            if($listIdOverride!='in_file') $listId = $listIdOverride;


            // set isBad and isDuplicate to false before checking
            $isBad = false;
            $isDuplicate = false;
            $status = array();




            if($leadDuplicateCheck=='DUPLIST')
            {
                $leadCount = KallflyList::selectRaw('count(*) as count')
                                ->where('phone_number', $phoneNumber)
                                ->where('list_id', $listId)
                                ->count();

                if($leadCount>0)
                {
                    $isBad = true;
                    $isDuplicate = true;
                }
            }



            if($leadDuplicateCheck=='DUPCAMP')
            {
                $list = Lists::find($listId);
                $lists = Lists::select('list_id')
                            ->where('campaign_id', $list->campaign_id)
                            ->get()
                            ->toArray();

                $listIds = array_column($lists, 'list_id');


                $leadCount = KallflyList::select('list_id')
                            ->where('phone_number', $phoneNumber)
                            ->whereIn('list_id', $listIds)
                            ->count();

                if($leadCount>0)
                {
                    $isBad = true;
                    $isDuplicate = true;
                }
            }



            if(strlen($phoneNumber <= 6))
            {
                $isBad = true;
                $status['error'] = 'invalid_phone';
            }
            elseif($isDuplicate)
            {
                $isBad = true;
                $status['error'] = 'duplicate';   
            }
            elseif($listId < 100)
            {
                $isBad = true;
                $status['error'] = 'invalid_list';      
            }
            
            



            if(!$isBad)
            {
                $lead = new KallflyList();
                

                // adding the posted value to the new lead object
                foreach($setColumn as $clmName => $index)
                {
                    if(isset($rowData[$index])) $lead->$clmName = $rowData[$index];
                }

                if($phoneCodeOverride!='in_file') $lead->phone_code = $phoneCodeOverride;



                // setting up default values to the new lead object
                $lead->entry_date               = date('Y-m-d H:i:s');
                $lead->modify_date              = '';
                $lead->status                   = 'NEW';
                $lead->user                     = '';
                $lead->gmt_offset_now           = 0;
                $lead->called_since_last_reset  = 'N';
                $lead->called_count             = 0;
                $lead->last_local_call_time     = '2008-01-01 00:00:00';
                $lead->list_id                  = $listId;

                if( strlen($phoneCode) < 1 ) $lead->phone_code = '1';


                $lead->save();
                $status['type'] = 'good';
            }






            if($isBad)
            {
                $isBad = true;
                $status['phone_number'] = $phoneNumber;
                $status['list_id'] = $listId;    
                $status['type'] = 'bad';
            }


            $user = auth()->user();
            LeadInserted::dispatch($user, $status);
        }

        return ['success' => 'OK'];
    }





    public function getPostColumn($key, $columns, $postData)
    {

        $index = isset($columns[$key]) ? $columns[$key] : false;

        return (isset($postData[$index])) ? $postData[$index] : '';

    }

}
