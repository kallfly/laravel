<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;


use App\TopUpHistory;
use App\User;
use App\Customer;

class PaypalController extends Controller
{
    
    private $_api_context;
	public function __construct()
	{
		$paypal_conf = \Config::get('paypal');
		$this->_api_context = new ApiContext(new OAuthTokenCredential(
			$paypal_conf['client_id'],
			$paypal_conf['secret']
		));

		$this->_api_context->setConfig($paypal_conf['settings']);
	}





	public function topUp()
	{
		$inputAmount = request('amount', 0);
		$taxRate = TopUpHistory::taxRate();
		$tax = $inputAmount * $taxRate;
		$total = $inputAmount + $tax;

		$payer = new Payer();
		$payer->setPaymentMethod("paypal");


		$item1 = new Item();
		$item1->setName('Top Up')
		    ->setCurrency('USD')
		    ->setQuantity(1)
		    ->setPrice($inputAmount);

		$itemList = new ItemList();
		$itemList->setItems(array($item1));

		$details = new Details();
		$details->setTax($tax)
				->setSubtotal($inputAmount);

		$amount = new Amount();
		$amount->setCurrency("USD")
		    ->setTotal($total)
		    ->setDetails($details);

		$transaction = new Transaction();
		$transaction->setAmount($amount)
		    ->setItemList($itemList)
		    ->setDescription("VOIP minutes Top up - Paypal");
		    // ->setInvoiceNumber(uniqid());

		$redirectUrls = new RedirectUrls();
		$paymentStatusUrl = request()->root() . '/top-up/paypal/payment-status';
		$redirectUrls->setReturnUrl($paymentStatusUrl)
		    ->setCancelUrl($paymentStatusUrl);

		$payment = new Payment();
		$payment->setIntent("sale")
		    ->setPayer($payer)
		    ->setRedirectUrls($redirectUrls)
		    ->setTransactions(array($transaction));

		$request = clone $payment;

		try {

		    $payment->create($this->_api_context);

		    
		    $log = new TopUpHistory;

		    $accountId = User::getAccountId();
		    $log->account_id 	 = $accountId;
		    $log->invoice_number = date('Ymd-is') . rand(10,99);
		    $log->amount 		 = $inputAmount;
		    $log->tax 			 = $tax;
		    $log->paid 			 = 0;
		    $log->date 			 = date('Y-m-d h:i:s');
		    $log->method 		 = 'paypal';
		    $log->transaction_id = $payment->getId();

		    $log->save();

		} catch (\PayPal\Exception\PPConnectionException $ex) {
			if (\Config::get('app.debug')) {
 
                \Session::flash('flash_error', 'Connection timeout');
                return redirect('top-up/paypal/payment-status');
 
            } else {
 
                \Session::flash('flash_error', 'Some error occur, sorry for inconvenient');
                return redirect('top-up/paypal/payment-status');
 
            }
		}


		foreach ($payment->getLinks() as $link) {
 
            if ($link->getRel() == 'approval_url') {
 
                $redirect_url = $link->getHref();
                break;
 
            }
 
        }
 
        /** add payment ID to session **/
        \Session::put('paypal_payment_id', $payment->getId());
 
        if (isset($redirect_url)) {
 
            /** redirect to paypal **/
            return redirect()->away($redirect_url);
 
        }
 
        \Session::flash('flash_error', 'Unknown error occurred');

        return redirect('top-up');
	}





	public function paymentStatus()
	{
		/** Get the payment ID before session clear **/
        $paymentId = \Session::get('paypal_payment_id', 0);
        $payment = Payment::get($paymentId, $this->_api_context);
 
        if (empty(request('PayerID')) || empty(request('token'))) {

            \Session::flash('flash_error', 'Payment failed');
            return redirect('/');
 
        }
 
        $payment = Payment::get($paymentId, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(request('PayerID'));
 
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        // return $result->getState();

        if ($result->getState() == 'approved') {

        	$log = TopUpHistory::where('transaction_id', $paymentId)->first();
	    	$log->paid = 1;
	    	$log->save();

        	Customer::addBalance($log->amount);

 
            \Session::flash('flash_message', 'Transaction was successful');
            return redirect('top-up');
 
        }
 
        \Session::flash('flash_error', 'Payment failed');
        return redirect('top-up');
	}

}
