<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campaign;
use App\Lists;
use App\KallflyList;

class ListAjaxController extends Controller
{
    public function totalLeads(Lists $listsData)
    {
    	$totalLeads = KallflyList::selectRaw('count(*) as total')
    				->where('list_id', $listsData->list_id)
    				->first();

    	return $totalLeads->total;
    }





    public function totalDialables(Lists $listsData)
    {
    	$campaign = $listsData->campaign;
    	$status = $campaign->getDialableStatuses();
        $dialableStatuses = '"' . implode('","', $status) . '"';


    	$totalLeads = KallflyList::selectRaw('
                        COALESCE(SUM(CASE 
                            WHEN 
                                status IN ('.$dialableStatuses.') 
                                AND called_since_last_reset = "N"
                            THEN 1 ELSE 0
                        END), 0) AS dialables,
                        count(*) as total')
    				->where('list_id', $listsData->list_id)
    				->first();

    	return $totalLeads;
    }





    public function totalCount(Lists $listsData)
    {
        $campaign = $listsData->campaign;
        $status = $campaign->getDialableStatuses();
        $dialableStatuses = '"' . implode('","', $status) . '"';


        $totalLeads = KallflyList::selectRaw('count(*) as total')
                    ->where('list_id', $listsData->list_id)
                    ->first();

        return $totalLeads;
    }





    public function dialables(Campaign $campaign)
    {
    	$dialableStatus = $campaign->getDialableStatuses();

    	return $campaign->getDialablesListCount();
    }





    public function leadList($listId)
    {
        return KallflyList::selectRaw('status, count(*) as count')
                    ->where('list_id', $listId)
                    ->groupBy('status')
                    ->get();
    }





    public function getLeadRecordings(KallflyList $lead)
    {
        $recordings = $lead->callRecordings;

        $response = array();
        foreach($recordings as $log)
        {
            $log->date = date('F d, Y h:i:s A', strtotime($log->start_time));
            $response[] = $log;
        }

        return $response;
    }





    public function getLogs(KallflyList $lead)
    {
        $recordings = $lead->logs;

        $response = array();
        foreach($recordings as $log)
        {
            $log->date = date('F d, Y h:i:s A', strtotime($log->call_date));
            $log->campaign_name = $log->campaign->campaign_name;
            $response[] = $log;
        }

        return $response;
    }

}
