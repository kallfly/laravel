<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\UserGroup;
use \App\Campaign;

class PerformanceController extends Controller
{

    public function __construct()
    {
    	$this->middleware('auth');
    }





    public function ajax()
    {
    	$startDate 	 = request('startDate');
    	$endDate   	 = request('endDate');
		$campaignIds = request('campaigns', ['all']);


		$allowedCampaignIds = UserGroup::getAllowedCampaignIds();



		$query = \DB::table('vicidial_agent_log as log')
    				->join('vicidial_users as user', 'log.user', 'user.user')
    				->selectRaw("
    				    user.user_id, 
    				    user.full_name, 
						log.status,
						COUNT(*) as status_count")

    				->where('event_time', '>=', $startDate)
					->where('event_time', '<=', $endDate)
					
					->whereNotNull('lead_id')
					->whereNotNull('status')

					->groupBy('log.user', 'log.status');



		if(!in_array('all', $campaignIds))
		{
			$query->whereIn('log.campaign_id', $campaignIds);
		}
		else
		{
			$query->whereIn('log.campaign_id', $allowedCampaignIds);
		}

		

		$logs = $query->get()->toArray();
		$statuses = array_column($logs, 'status');
		$statuses = array_unique($statuses);
		$statuses = array_fill_keys($statuses, 0 );




		$response = array();
		foreach($logs as $log)
		{
			$response[$log->full_name][$log->status] = $log->status_count;
		}



		foreach($response as $key => $stats)
		{
			$response[$key] = array_merge($statuses, $stats);
		}


		return $data = array('statuses' => $statuses, 'logs' => $response);

    }





    public function ajaxAverage()
    {

    	$startDate 	 = request('startDate');
    	$endDate   	 = request('endDate');
		$campaignIds = request('campaigns', ['all']);


		$allowedCampaignIds = UserGroup::getAllowedCampaignIds();



    	$query = \DB::table('vicidial_agent_log as log')
    				->join('vicidial_users as user', 'log.user', 'user.user')
    				->selectRaw("
    				    user.full_name, 
						log.user,
						SUM(log.talk_sec) / COUNT(log.user) AS avg_talk_time,
						SUM(log.pause_sec) / COUNT(log.user) AS avg_pause_time,
						SUM(log.wait_sec) / COUNT(log.user) AS avg_wait_time,
						SUM(log.dispo_sec) / COUNT(log.user) AS avg_dispo_time,
						SUM(log.dead_sec) / COUNT(log.user) AS 'avg_dead_time'")

    				->where('event_time', '>=', $startDate)
					->where('event_time', '<=', $endDate)
					
					->whereNotNull('lead_id')
					->whereNotNull('status')

					->groupBy('log.user');



		if(!in_array('all', $campaignIds))
		{
			$query->whereIn('log.campaign_id', $campaignIds);
		}
		else
		{
			$query->whereIn('log.campaign_id', $allowedCampaignIds);
		}




		$response = array();
		$avgTimes = $query->get();
		foreach($avgTimes as $times)
		{
			$temp = array();
			$temp['user'] = $times->full_name;
			$temp['avg_talk_time']  = $this->secondToHours($times->avg_talk_time);
			$temp['avg_pause_time'] = $this->secondToHours($times->avg_pause_time);
			$temp['avg_wait_time']  = $this->secondToHours($times->avg_wait_time);
			$temp['avg_dispo_time'] = $this->secondToHours($times->avg_dispo_time);
			$temp['avg_dead_time']  = $this->secondToHours($times->avg_dead_time);

			$response[] = $temp;
		}


		return $response;
    }





    public function secondToHours($time)
    {
		$mins = $time % 3600;
		$secs = $mins % 60;
		$hours = (int)($time - $mins) / 3600;
		$mins = ($mins - $secs) / 60;
		return $hours . ':' . (($mins < 10 ) ? '0' : '') . $mins . ':'  . (($secs < 10 ) ? '0' : '') . $secs;
    }

}
