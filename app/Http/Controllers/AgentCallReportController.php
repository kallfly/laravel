<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;

class AgentCallReportController extends Controller
{
    public function ajax()
    {
    	$startDate 	 = request('startDate');
    	$endDate   	 = request('endDate');
		$agentIds 	 = request('agents', ['all']);
		$campaignIds = request('campaigns', ['all']);

		$agentGroup = User::getUserGroup('agent');


		$logs = \DB::table('vicidial_users as user')
							->select(
									'log.agent_log_id', 
									'lead.phone_number',
									'log.user', 
									'log.event_time', 
									'log.lead_id', 
									'log.campaign_id', 
									'log.pause_sec', 
									'log.wait_sec',
									'log.talk_sec',
									'log.dispo_sec',
									'log.status',
									'log.uniqueid',
									'log.sub_status',
									'log.comments',
									'log.dead_epoch')
				    		
				    		->leftJoin('vicidial_agent_log as log', 'user.user', 'log.user')
							->leftJoin('vicidial_list as lead', 'lead.lead_id', 'log.lead_id')
				    		
				    		->where('event_time', '>=', $startDate)
							->where('event_time', '<=', $endDate)
		    				
		    				->where('user.user_group', $agentGroup)
		                    ->where('user.active', 'Y')
		                    ->whereNotNull('log.status')
							
							->limit(5000);


		if(!in_array('all', $agentIds))
    		$logs->whereIn('user.user_id', $agentIds);

    	if(!in_array('all', $campaignIds))
    		$logs->whereIn('log.campaign_id', $campaignIds);

							 


		return $logs->get()->toArray();;
    	
    }





    public function ajaxGraph()
    {

    	$startDate 	 = request('startDate');
    	$endDate   	 = request('endDate');
		$agentIds 	 = request('agents', ['all']);
		$campaignIds = request('campaigns', ['all']);

		$agentGroup = User::getUserGroup('agent');


    	$query = \DB::table('vicidial_users as user')
    					->join('vicidial_agent_log as log', 'user.user', 'log.user')

    					->selectRaw('
    							count(*) as calls,
    							user.full_name,
    							user.user,
    							log.status')

    					->where('event_time', '>=', $startDate)
						->where('event_time', '<=', $endDate)

						->where('user.user_group', $agentGroup)
		                ->where('user.active', 'Y')
		                ->whereNotNull('log.status')
						
						->groupBy('user.user', 'status')
						->orderBy('full_name', 'user.user', 'status desc')
						->limit(5000);


		if(!in_array('all', $agentIds))
    		$query->whereIn('user.user_id', $agentIds);

    	if(!in_array('all', $campaignIds))
    		$query->whereIn('log.campaign_id', $campaignIds);



    	$logs = $query->get();
    	$agentLogs = array();
    	$chartSeries = array();
    	$agentCalls = array();
    	foreach($logs as $log)
    	{
			$agentLogs[$log->status][$log->user]['user'] 		= $log->user;
			$agentLogs[$log->status][$log->user]['full_name'] = $log->full_name;
			$agentLogs[$log->status][$log->user]['calls'] 	= $log->calls;

			$chartSeries[$log->status][] = (int)$log->calls;
			$agentCalls[$log->full_name][] = $log->calls;
		}



		$chartCategories = array();
		foreach($agentCalls as $name => $calls)
		{
			$chartCategories[] = $name . ' | Sum: ' . array_sum($calls) . ' Calls';
		}



		return $data = array( 'categories' => $chartCategories, 'series' => $chartSeries );

		return $agentLogs;
    }
}
