<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\KallpodInvoice;

class KallpodInvoiceController extends Controller
{
	public function index(){
        $data['title'] = 'Kallpod Invoice';
        $data['invoices'] = KallpodInvoice::latest()->get();

		return view('invoice.master', $data);
	}

	
    public function generate(){
    	$price_per_user = 24;
    	$customers = KallpodInvoice::onDueCustomers();

    	foreach($customers as $customer)
    	{
    		$invoice = new KallpodInvoice;
    		$invoice->invoice_number = KallpodInvoice::generateInvoiceNumber();
    		$invoice->customer_id = $customer->customer_id;
    		$invoice->num_users = $customer->allowed_users;
    		$invoice->total_price = $price_per_user * $customer->allowed_users;
    		$invoice->invoice_date = now();
    		$invoice->paid = false;
    		$invoice->save();
    	}

    	return('Total Invoices Created:' . count($customers));
    }
}
