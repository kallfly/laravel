<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use Illuminate\Support\Facades\Hash;
use Auth;

class SessionHandlerController extends Controller
{
	public function __construct(){

		$this->middleware('guest', ['except' => 'logout']);

	}


    public function login(){

    	return view('session-handler.login');
    	
    }


    public function attempt(){

        $user = request('user');
        $pass = request('pass');



        // update user pass if password is not hash
        // this will be removed eventually when all the users get their password hashed
        $oldUser = User::where('user', $user)->where('pass', $pass)->first();
        if($oldUser)
        {
            $oldUser->pass = Hash::make($pass);
            $oldUser->save();
        }



    	$credentials = [
    		'user' => $user, 
    		'password' => $pass,
    		'user_level' => 9
    	];

    	if(Auth::attempt($credentials))
        {
            $territory = auth()->user()->territory;
            $timezone = in_array($territory, timezone_identifiers_list()) ? $territory : "America/Chicago";
            
            session(['timezone' => $timezone]);


            return redirect()->intended('/');
        }


		$message = array('Invalid Credentials.');
		return redirect(url()->previous())->withErrors($message);

    }

    public function logout(){

    	Auth::logout();
    	return redirect('/login');

    }
}
