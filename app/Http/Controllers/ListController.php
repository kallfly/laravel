<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Lists;
use \App\UserGroup;
use \App\Campaign;
use \App\KallflyList;
use \App\Status;

class ListController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }





    public function index()
    {
    	$data['title'] = 'List';
    	$data['editMode'] = false;
    	$data['form'] = array(
			'action' => '/list'
		);

    	$data['listsData'] = new Lists;
    	$data['lists'] = Lists::getUserList();


    	$allowedCampaignIds = UserGroup::getAllowedCampaignIds();
    	$data['campaignLists'] = Campaign::select('campaign_id', 'campaign_name')->whereIn('campaign_id', $allowedCampaignIds)->get();
    	$data['activeStatuses'] = array('Y' => 'active', 'N' => 'inactive');

    	return view('list.master', $data);
    }





    public function store()
    {
    	$this->validate(request(), [
    		'list_name' => 'required',
    		'list_description' => 'required'
    	]);

    	Lists::create(request()->all());

		\Session::flash('flash_message', request('list_name') . ' successfully added.');

    	return redirect(url()->previous());
    }





    public function edit(Lists $listsData)
    {
    	$data['title'] = 'List';
    	$data['editMode'] = true;
    	$data['form'] = array(
			'action' => '/list/' . $listsData->list_id
		);

    	$data['listsData'] = $listsData;
    	$data['lists'] = Lists::getUserList();


    	$allowedCampaignIds = UserGroup::getAllowedCampaignIds();
    	$data['campaignLists'] = Campaign::select('campaign_id', 'campaign_name')->whereIn('campaign_id', $allowedCampaignIds)->get();
    	$data['activeStatuses'] = array('Y' => 'active', 'N' => 'inactive');


        $leadCount = [];//$listsData->kallflyList()->selectRaw('status, count(*) as count')->groupBy('status')->get();
        $data['leadCount'] = $leadCount;

        $pieChartData = array();
        foreach($leadCount as $lead)
        {
            $pieChartData[] = array(
                                'name' => $lead->status, 
                                'y' => $lead->count);
        }
        $data['pieChartData'] = json_encode($pieChartData);



    	return view('list.edit', $data);
    }





    public function update(Lists $listsData)
    {

    	$this->validate(request(), [
    		'list_name' => 'required',
    		'list_description' => 'required'
    	]);


    	$listsData->list_name = request('list_name');
    	$listsData->list_description = request('list_description');
    	$listsData->campaign_id = request('campaign_id');
    	$listsData->active = request('active');
    	$listsData->save();

    	\Session::flash('flash_message', request('list_name') . ' successfully updated.');

    	return redirect('/list');
    }





    public function reset(Lists $listsData)
    {
        $listsData->kallflyList()->update( ['called_since_last_reset' => 'N'] );

        \Session::flash('flash_message_list', $listsData->list_name . ' leads has been reset.');
     
        return redirect(url()->previous());
    }





    public function downloadLeadSummary(Lists $listsData)
    {
        $leads = $listsData->kallflyList;
        $txt_csv = "First Name\tLast Name\tPhone Number\tCountry Code\tStatus\tEmail\tAddress 1\tAddress 2\tAddress 3\tCity\tPostal Code\tState\tComments\tCalled Count\tAgent\tLast Call Time";
        $txt_csv .= "\r\n";
        $filename = "LeadSummaryLeadID";


        \Excel::create('Lead Summary', function($excel) use ($leads){

            $excel->sheet('New sheet', function($sheet) use ($leads) {
                $sheet->loadView('list.lead-summary', compact('leads'));
            });

        })->download('csv');
    }

}
