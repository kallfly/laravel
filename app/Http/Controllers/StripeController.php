<?php

namespace App\Http\Controllers;

use Stripe\Error\Card;

use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

use App\User;
use App\TopUpHistory;
use App\Customer as KfCustomer;


class StripeController extends Controller
{
	public function topUp()
	{
		try{
			$this->validate(request(),[
	    		'card' => 'required',
	    		'exp_month' => 'required',
	    		'exp_year' => 'required',
	    		'cv' => 'required',
	    		'amount' => 'required'
	    	]);

			Stripe::setApiKey(env('STRIPE_SECRET'));

			$card 	  = request('card');
			$expMonth = request('exp_month');
			$expYear  = request('exp_year');
			$cv 	  = request('cv');
			$amount	  = request('amount');

			$tax = $amount * TopUpHistory::taxRate();
			$total = $amount + $tax;
			$total = number_format($amount, 2);

	        $card = array(
				"number" => $card,
				"exp_month" => $expMonth,
				"exp_year" => $expYear,
				"cvc" => $cv);
			$token = \Stripe\Token::create(array("card" => $card));

			$chargeArr = array(
							'source' => $token, 
							'amount' => str_replace('.', '', $total),
							'currency' => 'usd', 
							"description" => 'Top-up Payment amount '.$amount.' for credit card ending from KallFly');
			$charge = \Stripe\Charge::create($chargeArr);

			if($charge)
			{
				// $log = htmlspecialchars(json_encode($charge), ENT_QUOTES, 'UTF-8');

				$log = new TopUpHistory;

			    $accountId = User::getAccountId();
			    $log->account_id 	 = $accountId;
			    $log->invoice_number = date('Ymd-is') . rand(10,99);
			    $log->amount 		 = $amount;
			    $log->tax 			 = $tax;
			    $log->paid 			 = 1;
			    $log->date 			 = date('Y-m-d h:i:s');
			    $log->method 		 = 'stripe';
			    $log->transaction_id = $charge->id;

			    $log->save();


	        	KfCustomer::addBalance($amount);


				// $cdr = new CDR();
				// $cdr->send_top_up_email($userId, $amount);

				\Session::flash('flash_message', 'Transaction was successful');
	            return redirect('top-up');
			}
			else
			{
				\Session::flash('flash_error', 'Payment failed');
	        	return redirect('top-up');
			}


		} catch (Card  $e ) {

			\Session::flash('flash_error', $e->getMessage());
			return redirect('top-up');

        }

		return 'running';
	}
}
