<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Carbon\Carbon;
use App\User;
use App\UserGroup;
use App\AgentLog;
use App\AgentCampaign;
use App\Status;
use App\CampaignStatus;
use App\LiveAgent;
use \Session;

class DashboardController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

    public function index()
    {
    	$data['title'] = 'Kallfly Dashboard';

    	$dt = Carbon::now();
    	$data['endDate'] = $dt->format('Y-m-d');
    	$data['startDate'] = $dt->modify('-1 months')->format('Y-m-d');

        $statuses = Status::getStatusOptions();
        ksort($statuses);
        $data['statuses'] = $statuses;

        $selectedStatus = Session::has('selectedStatus') ? Session::get('selectedStatus') : 'SALE';

        $data['selectedStatus'] = $selectedStatus;

    	return view('dashboard.master', $data);
    }





    public function search()
    {
    	$startDate = request('startDate');
    	$endDate   = request('endDate');

    	$countOnly = true;
    	return AgentLog::getAgentCallsTotal($startDate, $endDate);
    }





    public function callActivities()
    {
    	$startDate = request('startDate');
    	$endDate   = request('endDate');
    	$callType  = request('callType');
        $limit = 20;

    	$calls = AgentLog::getAgentCalls($startDate, $endDate, $callType, $limit);

		return $calls;
    }





    public function callGraphData()
    {
    	$startDate = request('startDate');
    	$endDate   = request('endDate');
	
		



        $agentGroup = User::getUserGroup('agent');
        $users = User::select('user_id')
            ->where("user_group", $agentGroup)
            ->get()
            ->toArray();
        $userIds = array_column($users, 'user_id');



        $allCalls = array();
        $campaigns = AgentCampaign::select('campaign_ids')
            ->whereIn('user_id', $userIds)
            ->get()
            ->toArray();
        $campaignIds = array_column($campaigns, 'campaign_ids');
        $campaignIds = implode(',', $campaignIds);
        $campaignIds = explode(',', $campaignIds);



        $allDispositions = array();

        $campaignDispositions = CampaignStatus::whereIn('campaign_id', $campaignIds)
                        ->get();
        foreach($campaignDispositions as $dispo)
        {
            $status = strtoupper($dispo->status);
            $allDispositions[$status] = $dispo->status_name;
        }


        $systemDispositions = Status::all();
        foreach($systemDispositions as $dispo)
        {
            $status = strtoupper($dispo->status);
            $allDispositions[$status] = $dispo->status_name;
        }
        ksort($allDispositions);














        $calls = AgentLog::getAgentCalls($startDate, $endDate);

        $agentCalls = array();
        foreach($calls as $c){
            $stat = strtoupper($c->status);
            $agentCalls[$stat] = isset($agentCalls[$stat]) ? $agentCalls[$stat] + 1 : 1;
        }
        ksort($agentCalls);

        $label = array();
        $value = array();
        $callStatsLabel = array();
        foreach($agentCalls as $status => $count){
            $label[] = $status;
            $value[] = $count;

            $callStatsLabel[] = '['.$status.'] ' . $allDispositions[$status];
        }

        $data['callStats'] = array(
                                    'label' => $callStatsLabel,
                                    'data' => $value
                                );


















        foreach($calls as $c)
        {
            $status   = strtoupper($c->status);
            $user     = strtoupper($c->user);
            $fullName = strtoupper($c->full_name);
            $allCalls[$user][$status]       = isset($allCalls[$user][$status]) ? $allCalls[$user][$status] + 1 : 1;
            $allCalls[$user]['full_name']   = $fullName;
            $allCalls[$user]['total_calls'] = isset($allCalls[$user]['total_calls']) ? $allCalls[$user]['total_calls'] + 1 : 1;
        }
        ksort($allCalls);



        $callsBreakdown = array();
        $cbLabel = array();
        foreach($allCalls as $agent => $arr)
        {
            $fn = strtoupper($arr['full_name']);
            $cbLabel[] = $fn . '|Total:' . $arr['total_calls'];

            foreach($label as $dispo)
            {
                $d = strtoupper($dispo);
                $callsBreakdown[$d][] = isset($allCalls[$agent][$d]) ? $allCalls[$agent][$d] : 0;
            }
        }


        $data['callsBreakdown'] = array(
                                        "label" => $cbLabel,
                                        "data" => $callsBreakdown,
                                        "data_orig" => $allCalls,
                                        "dispos_desc" => $allDispositions
                                    );



		return $data;
    }





    public function realtimeAgents()
    {
        $agentGroup = User::getUserGroup('agent');
        return LiveAgent::getRealtimeAgents($agentGroup);
    }





    public function statusCount($status)
    {
        session(['selectedStatus' => $status]);
        $statuses = Status::getStatusOptions();
        
        $campaignIds = UserGroup::getAllowedCampaignIds();

        $lists = \DB::table('vicidial_lists as a')
                        ->selectRaw('count(*) as count')
                        ->join('vicidial_list as b', 'a.list_id', 'b.list_id')
                        ->whereIn('a.campaign_id', $campaignIds)
                        ->where('b.status', 'like', $status)
                        ->get()
                        ->toArray();

        $data = array(
                    'status' => $status,
                    'statusName' => $statuses[$status],
                    'total' => $lists[0]->count
                );
        return $data;
    }
}
