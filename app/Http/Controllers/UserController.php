<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use \App\User;
use \App\Phone;
use \App\Server;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function admin()
    {
        $data['editMode'] = false;
        $data['title'] = 'Admin List';
        $data['countries'] = \App\Http\Helpers::getCountries();

        $data['form'] = [
            'action' => '/user',
            'user_type' => 'admin',
            'redirect' => '/user/admin'
        ];

        $groupPrefix = User::getAuthUser();
        $userGroup = $groupPrefix . '_ADMIN';


        $data['agentPrefix'] = User::getAgentPrefix();


        $data['user'] = new User;
        $data['users'] = User::where('user_group', $userGroup)
                                ->where('active', 'Y')
                                ->get();

    	return view('user.master', $data);
    }


    public function adminEdit(User $user)
    {
        $data['editMode'] = true;
        $data['title'] = 'Admin List';
        $data['countries'] = \App\Http\Helpers::getCountries();

        $data['form'] = [
            'action' => '/user/'.$user->user_id.'/edit',
            'user_type' => 'admin',
            'redirect' => '/user/admin'
        ];

        $groupPrefix = User::getAuthUser();
        $userGroup = $groupPrefix . '_ADMIN';

        $data['user'] = $user;
        $data['users'] = User::where('user_group', $userGroup)
                                ->where('active', 'Y')
                                ->get();

        return view('user.admin.master', $data);
    }

    public function agent()
    {
        $data['editMode'] = false;
        $data['title'] = 'Agent List';
        $data['countries'] = \App\Http\Helpers::getCountries();

        $data['form'] = [
            'action' => '/user',
            'user_type' => 'agent',
            'redirect' => '/user/agent'
        ];

        $groupPrefix = User::getAuthUser();
        $userGroup = $groupPrefix . '_AGENT';

        $data['agentPrefix'] = User::getAgentPrefix();

        $data['user'] = new User;
        $data['users'] = User::where('user_group', $userGroup)
                                ->where('active', 'Y')
                                ->get();

        return view('user.master', $data);
    }


    public function agentEdit(User $user)
    {
        $data['editMode'] = true;
        $data['title'] = 'Admin List';
        $data['countries'] = \App\Http\Helpers::getCountries();

        $data['form'] = [
            'action' => '/user/'.$user->user_id.'/edit',
            'user_type' => 'agent',
            'redirect' => '/user/agent'
        ];

        $groupPrefix = User::getAuthUser();
        $userGroup = $groupPrefix . '_AGENT';

        $data['user'] = $user;

        return view('user.agent.master', $data);
    }


    public function store()
    {
    	
    	$this->validate(request(),[
    		'user' => 'required|unique:vicidial_users',
    		'pass' => 'required|confirmed|min:6',
    		'full_name' => 'required'
    	]);

        $authUserPrefix = User::getAuthUser();
        $userType = strtoupper(request('user_type'));
        $userGroup = $authUserPrefix . '_' . $userType;

        $user = new User;
        $user->pass = Hash::make(request('pass'));
        $user->full_name = request('full_name');
        $user->user_group = $userGroup;
        $user->territory = request('territory');
        $user->agent_choose_ingroups = '1';
        $user->agentonly_callbacks = '1';
        $user->scheduled_callbacks = '1';
        $user->agentcall_manual = '1';
        $user->vicidial_recording = '1';
        $user->vicidial_transfers = '1';
        $user->alter_custphone_override = 'ALLOW_ALTER';


        if($userType == 'ADMIN')
        {
            $user->user = request('user');
            $user->user_level = 9;
            $user->agent_choose_blended = '1';
            $user->hotkeys_active = '1';
        }
        else
        {
            $query = Phone::selectRaw('max(CONVERT(extension, signed)) as max')->first();
            $extension = (int) $query->max;
            $extension++;



            $agentPrefix = User::getAgentPrefix();
            $user->user = $agentPrefix . request('user');
            $user->user_level = 1;
            $user->agent_choose_blended = '0';
            $user->hotkeys_active = '0';
            $user->phone_login = $extension;



            $query = Phone::selectRaw('count(*) as phoneCount, server_ip')
                        ->groupBy('server_ip')
                        ->orderByRaw('phoneCount')
                        ->first();
            $serverIp = $query->server_ip;


            $phone = new Phone;
            $phone->extension       = $extension;
            $phone->dialplan_number = $extension;
            $phone->voicemail_id    = $extension;
            $phone->outbound_cid    = $extension;
            $phone->login           = $extension;
            $phone->server_ip       = $serverIp;
            $phone->pass            = str_random(5);
            $phone->conf_secret     = str_random(8);
            $phone->fullname        = request('full_name');
            $phone->active          = 'Y';
            $phone->status          = 'ACTIVE';

            $phone->save();





            $serverTable = (new Server())->getTable();
            \DB::table($serverTable)
            ->where('generate_vicidial_conf', 'Y')
            ->where('active_asterisk_server', 'Y')
            ->where('server_ip', $serverIp)
            ->update(array('rebuild_conf_files' => 'Y'));


        }

        $user->save();




        
    	\Session::flash('flash_message', request('user_type') . ' successfully added.');

    	return redirect(request('redirect'));
    }


    public function update(User $user)
    {   
        $this->validate(request(),[
            'pass' => 'confirmed',
            'full_name' => 'required'
        ]);

        $pass = request('pass');
        if(strlen($pass)<6 && strlen($pass)>0)
        {
            $message = array('The pass must be at least 6 characters.');
            return redirect(url()->previous())->withErrors($message);
        }

        if(strlen($pass)>0) $user->pass = Hash::make(request('pass'));

        $user->full_name = request('full_name');
        $user->territory = request('territory');
        $user->save();
        
        \Session::flash('flash_message', request('user_type') . 'successfully updated.');

        return redirect(request('redirect'));
    }


    function autocomplete(Request $request){
    	$query = $request->query('query');

    	$users = User::select(['user_id AS id', 'full_name AS name'])
    					->where('full_name', 'like', "%$query%")->get();

    	return $users;
    }
}
