<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TimezoneController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }


    public function update()
    {
    	$timezone = request('select-timezone');
		$timezone = in_array($timezone, timezone_identifiers_list()) ? $timezone : "America/Chicago";
        
        session(['timezone' => $timezone]);
		

		return redirect(url()->previous());
    }
}
