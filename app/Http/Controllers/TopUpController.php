<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TopUpHistory;
use App\User;
use App\Customer;

class TopUpController extends Controller
{

    public function __construct()
    {
    	$this->middleware('auth');
    }





    public function index()
    {
    	$data['title'] = 'Top Up';
    	$data['topUpList'] = TopUpHistory::userHistory();

        // return Customer::getBalance();



    	return view('top-up.master', $data);
    }





    public function print(TopUpHistory $topUpHistory)
    {
    	$invoice = $topUpHistory;
		$tax = floatval($invoice->tax);


		$data['invoice'] = $invoice;
		$data['total']   = '$'.number_format($invoice->amount + $tax, 2);
		$data['tax']     = '$'.number_format($tax, 2);
    	$data['amount']  = '$' . number_format($invoice->amount, 2);
    	$data['method']  = $invoice->method == 'stripe' ? 'Credit Card' : ucfirst($invoice->method);
    	$data['date'] 	 = date('M d, Y h:i a', strtotime($invoice->date));




    	return view('top-up.print', $data);
    }

}
