<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Status;
use \App\User;
use \App\UserGroup;

class DispositionController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }





    public function ajax()
    {
    	$startDate 	 = request('startDate');
    	$endDate   	 = request('endDate');
		$status 	 = request('status');

		$agentGroup  = User::getUserGroup('agent');
		$campaignIds = UserGroup::getAllowedCampaignIds();


    	$query = \DB::table('vicidial_users as user')
    					->join('vicidial_agent_log as log', 'user.user', 'log.user')

    					->selectRaw('
    							count(*) as calls,
    							user.full_name,
    							user.user,
    							log.status')

    					->where('event_time', '>=', $startDate)
						->where('event_time', '<=', $endDate)

						->where('user.user_group', $agentGroup)
		                // ->where('user.active', 'Y')  removed filter since its not the old query doesn't check user's active status

		                ->whereIn('log.campaign_id', $campaignIds)
		                ->where('log.status', $status)
						
						->groupBy('user.user', 'status')
						->orderBy('full_name', 'user.user', 'status desc')
						->limit(5000);



    	$logs = $query->get();
    	$agentLogs = array();
    	$chartSeries = array();
    	$agentCalls = array();
    	foreach($logs as $log)
    	{
			$agentLogs[$log->status][$log->user]['user'] 		= $log->user;
			$agentLogs[$log->status][$log->user]['full_name'] = $log->full_name;
			$agentLogs[$log->status][$log->user]['calls'] 	= $log->calls;

			$chartSeries[$log->status][] = (int)$log->calls;
			$agentCalls[$log->full_name][] = $log->calls;
		}



		$chartCategories = array();
		foreach($agentCalls as $name => $calls)
		{
			$chartCategories[] = $name . ' | Sum: ' . array_sum($calls) . ' Calls';
		}



		return $data = array( 'categories' => $chartCategories, 'series' => $chartSeries );

		return $agentLogs;
    }
}
