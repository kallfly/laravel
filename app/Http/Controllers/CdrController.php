<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cdr;
use App\User;

class CdrController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }





    public function index()
    {
    	$data['title'] = 'CDR';

    	return view('cdr.master', $data);
    }





    public function ajaxReport()
    {
        $date = request('date');
        $limit = request('limit');

    	$accountId = User::getAccountId();
    	$query = Cdr::where('accountcode', $accountId)
                    ->orderByRaw('call_date desc');

        if($limit) $query->limit($limit);
        
        if($date) $query->where('call_date', 'like', $date . '%');

        $cdr = $query->get();

    	return $cdr;
    }

}
