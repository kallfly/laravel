<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\UserLog;
use \App\User;
use \Carbon\Carbon;

class AgentHoursController extends Controller
{
	protected $dateFormat = 'Y-m-d';
    public function ajax()
    {
		$startDate = request('startDate');    	
		$endDate = request('endDate');    	
		$agentIds = request('agents', ['all']);


		$agentGroup = User::getUserGroup('agent');



    	$agentListsQuery = User::select('user', 'full_name')
    						->where('user_group', $agentGroup)
                            ->where('active', 'Y');

    	if(!in_array('all', $agentIds))
    		$agentListsQuery->whereIn('user_id', $agentIds);

    	$agentLists = $agentListsQuery->get()->toArray();

    	$agents = array_column($agentLists, 'full_name', 'user');
    	$agentUsernames = array_column($agentLists, 'user');


        $agentHours = UserLog::select('user', 'event_date')
						->where('event_date', '>=', $startDate)
						->where('event_date', '<=', $endDate)
						->whereIn('user', $agentUsernames)
						->where('user_group', $agentGroup)
						->orderBy('user', 'event_date')
						->get();


		$logLists = array();
		foreach($agentHours as $data)
		{
			$timezone = session('timezone');
			$eventTime = new Carbon($data->event_date, $timezone);

			$logLists[$data->user][$eventTime->toDateString()][] = $eventTime->format('Y-m-d h:i:s');
		}



		$response = array();
		foreach($logLists as $agent => $workHours)
		{
			foreach($workHours as $date => $dateTime)
			{
				$firstLog = Carbon::parse($dateTime[0], $timezone);
				$lastLog  = Carbon::parse($dateTime[count($dateTime) - 1], $timezone);
				$logDuration = Carbon::parse($dateTime[count($dateTime) - 1], $timezone);
				$totalDuration = $logDuration->diff($firstLog)->format('%H:%I:%S');




				$temp = array(
							'date' => $firstLog->format('M j Y [D]'),
							'firstLog' => $firstLog->format('h:i:s A'),
							'lastLog' => $lastLog->format('h:i:s A'),
							'total' => $totalDuration
							);

				$response[$agents[$agent]][] = $temp;
			}
		}



		return $response;
    }
}
