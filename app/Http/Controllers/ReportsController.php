<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Carbon\Carbon;
use \App\User;
use \App\UserGroup;
use \App\Campaign;
use \App\Status;

class ReportsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }





    public function index()
    {
    	$data['title'] = 'Reports';

    	$dt = Carbon::now();
    	$data['endDate'] = $dt->format('Y-m-d');
    	$data['startDate'] = $dt->modify('-1 months')->format('Y-m-d');

    	$data['agents'] = User::getAgents();

        $campaignIds = UserGroup::getAllowedCampaignIds();
        $data['campaigns'] = Campaign::whereIn('campaign_id', $campaignIds)->get();

        $data['statuses'] = Status::getStatusOptions();


    	
    	return view('reports.master', $data);
    }
}
