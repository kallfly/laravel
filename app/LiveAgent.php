<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveAgent extends Model
{
    protected $table = 'vicidial_live_agents';
    protected $primaryKey = 'live_agent_id';

    public $timestamps = false;


    public static function getRealtimeAgents($userGroup)
    {
    	return \DB::select("SELECT
    			extension, 
				vicidial_live_agents.user,
				conf_exten,
				vicidial_live_agents.status,
				vicidial_live_agents.server_ip,
				UNIX_TIMESTAMP(last_call_time),
				UNIX_TIMESTAMP(last_call_finish),
				call_server_ip,
				vicidial_live_agents.campaign_id,
				vicidial_users.user_group,
				vicidial_users.full_name,
				vicidial_live_agents.comments,
				vicidial_live_agents.calls_today,
				vicidial_live_agents.callerid,
				lead_id,
				UNIX_TIMESTAMP(last_state_change) as last_state_change,
				TIMEDIFF(NOW(), last_state_change) as calltime,
				on_hook_agent,ring_callerid,
				agent_log_id,
				(SELECT phone_number FROM vicidial_list WHERE vicidial_list.lead_id = vicidial_live_agents.lead_id LIMIT 1) as phone_number,
				(SELECT phone_code FROM vicidial_list WHERE vicidial_list.lead_id = vicidial_live_agents.lead_id LIMIT 1) as phone_code,
				(SELECT full_name FROM vicidial_users WHERE vicidial_users.user = vicidial_live_agents.user LIMIT 1) as agent_full_name,
				(SELECT campaign_name FROM vicidial_campaigns WHERE vicidial_campaigns.campaign_id = vicidial_live_agents.campaign_id LIMIT 1) as campaign_name
				FROM 
					vicidial_live_agents,vicidial_users 
				WHERE 
					vicidial_live_agents.user=vicidial_users.user
				AND
					vicidial_users.user_group LIKE '$userGroup%'
				ORDER BY vicidial_live_agents.campaign_id, vicidial_live_agents.status, calltime DESC");
    }
}
