<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\UserGroup;
use \App\CampaignStatus;

class Status extends Model
{
    protected $table 	  = 'vicidial_statuses';
    protected $primaryKey = 'status';
    
    public $incrementing = false;
    public $timestamps = false;





    public static function getStatusOptions()
    {
        $campaignIds = UserGroup::getAllowedCampaignIds();
    	$statusList = self::select('status', 'status_name')->get()->toArray();
		$campaignStatusList = CampaignStatus::select('status', 'status_name')->whereIn('campaign_id', $campaignIds)->get()->toArray();
		

		$statusOptions = array_merge($statusList, $campaignStatusList);
		$formattedStatusOptions = array();
		foreach($statusOptions as $s)
		{
			$status = strtoupper($s['status']);
			$name   = ucwords($s['status_name']);

			$formattedStatusOptions[$status] = $name;
		}

		return $formattedStatusOptions;
    }





    public static function getStatusName($statusId)
    {
    	$status = self::where('status', $statusId)->first();
    	return $status->status_name;
    }
}
