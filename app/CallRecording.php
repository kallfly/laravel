<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallRecording extends Model
{
    protected $table = 'recording_log';
    protected $primaryKey = 'recording_id';

    public $timestamps = false;

}
