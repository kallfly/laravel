<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Lists;

class Campaign extends Model
{
    protected $table = 'vicidial_campaigns';
    protected $primaryKey = 'campaign_id';
    
    public $incrementing = false;
    public $timestamps = false;


    public static function incrementNewCampaignId()
    {
    	$userId = auth()->user()->user_id;

		$campaigns = Campaign::select('campaign_id')
							->where('campaign_id', 'like', $userId.'%')
							->get();


		// set a 0 value to make sure max() doesn't throw error when having 0 campaigns
		$campaignIds = array(0);
		foreach($campaigns as $c)
		{
			$id = str_replace($userId, '', $c->campaign_id);
			$campaignIds[] = intval($id);
		}


		$id = max($campaignIds);
		$id = $id + 1;
		$campaignId = $userId . str_pad($id, 4, "0", STR_PAD_LEFT);

		return $campaignId;
    }




    public function getDialablesListCount()
    {
        $status = $this->getDialableStatuses();
        $dialableStatuses = "'" . implode("','", $status) . "'";

    	$query = \DB::table('vicidial_lists as a')
			    	->selectRaw("a.list_name, 
                        SUM(CASE 
                            WHEN 
                                b.status IN (".$dialableStatuses.") 
                                AND b.called_since_last_reset = 'N'
                            THEN 1 ELSE 0
                        END) AS 'dialables', 
                        count(*) as total")
			    	->join('vicidial_list as b', 'a.list_id', 'b.list_id')
			    	->where('campaign_id', $this->campaign_id)
			    	->groupBy('a.list_name', 'b.list_id')
                    ->get();

		return $query;
	}





    public function getDialablesList()
    {
        return Lists::select(
                            'campaign_id', 
                            'list_name', 
                            'list_id', 
                            \DB::raw('0 as dialables'), 
                            \DB::raw('0 as total'))
                        ->where('campaign_id', $this->campaign_id)
                        ->get();
    }





    public function getDialableStatuses()
    {
    	$dialables = str_replace('-', '', trim($this->dial_statuses));
		$dialablesArray = explode(' ', $dialables);
		return array_filter($dialablesArray);
    }






    public function getActiveLists()
    {
    	return Lists::where('campaign_id', $this->campaign_id)
    						->where('active', 'Y')
    						->get();
    }





    public function list(){
    	return $this->hasMany(\App\Lists::class, 'campaign_id', 'campaign_id');
    }
}
