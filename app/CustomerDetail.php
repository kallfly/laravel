<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerDetail extends Model
{
    protected $fillable = ['customer_id', 'monthly_invoice_cycle', 'allowed_users', 'status'];

    public function vicidialUser(){
    	return $this->belongsTo(VicidialUser::class, 'customer_id', 'user_id');
    }
}
