<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Customer;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);


        view()->composer('layouts.nav', function($view){
            $balance = number_format(Customer::getBalance(), 2);
            $view->with('balance', $balance);

            $timezones = array(
                'America/New York' => 'America/New_York',
                'America/Chicago' => 'America/Chicago',
                'America/Denver' => 'America/Denver',
                'America/Los Angeles' => 'America/Los_Angeles',
                'America/Anchorage' => 'America/Anchorage',
                'America/Adak' => 'America/Adak',
                'Asia/Manila' => 'Asia/Manila',
                'Asia/Singapore' => 'Asia/Singapore',
                'Australia/Darwin' => 'Australia/Darwin',
                'Australia/Melbourne' => 'Australia/Melbourne',
                'Australia/Sydney' => 'Australia/Sydney',
                'Australia/Perth' => 'Australia/Perth',
                'Australia/West' => 'Australia/West',
                'Europe/Amsterdam' => 'Europe/Amsterdam',
                'Europe/Berlin' => 'Europe/Berlin',
                'Europe/Budapest' => 'Europe/Budapest',
                'Europe/Dublin' => 'Europe/Dublin',
                'Europe/Zurich' => 'Europe/Zurich',
            );
            $view->with('timezones', $timezones);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
