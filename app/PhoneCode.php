<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneCode extends Model
{
	protected $table = 'vicidial_phone_codes';
	protected $primaryKey = null;

	public $timestamps = false;
	public $incrementing = false;
}