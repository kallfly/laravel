<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Campaign;
use App\UserGroup;
use App\KallflyList;

class Lists extends Model
{
    protected $table 	  = 'vicidial_lists';
    protected $primaryKey = 'list_id';

    protected $fillable = [
        'list_name', 'list_description', 'campaign_id', 'active'
    ];
    
    public $timestamps = false;


    public function campaign()
    {
    	return $this->belongsTo(Campaign::class, 'campaign_id', 'campaign_id');
    }





    public static function getUserList()
    {
    	$campaignIds = UserGroup::getAllowedCampaignIds();
    	$lists = \DB::table('vicidial_campaigns as c')
    		->selectRaw('c.campaign_id, c.campaign_name, 0 as leads_count, list.list_id, list.list_name, list.active')
            ->join('vicidial_lists as list', 'c.campaign_id', 'list.campaign_id')
    		->whereIn('list.campaign_id', $campaignIds)
            ->groupBy('list.list_id')
    		->get();

    	return $lists;
    }





    public function kallflyList()
    {
        return $this->hasMany(KallflyList::class, 'list_id', 'list_id');
    }

    
}