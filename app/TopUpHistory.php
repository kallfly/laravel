<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customer;

class TopUpHistory extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'top_up_history';

    public $timestamps = false;





    public static function userHistory()
    {
    	$user = auth()->user();
    	$accountId = str_pad($user->user_id, 6, '0', STR_PAD_LEFT);

    	$topUpList = self::where('account_id', $accountId)
    					->orderByRaw('id desc')
    					->get();

    	return $topUpList;
    }





    public function customer()
    {
    	return $this->belongsTo(\App\Customer::class, 'account_id', 'account_id');
    }





    public static function taxRate()
    {
        $tax = .025;

        return $tax;
    }

}
