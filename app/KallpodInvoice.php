<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class KallpodInvoice extends Model
{
    public function vicidialUser(){
        return $this->belongsTo(VicidialUser::class, 'customer_id', 'user_id');
    }

    public static function onDueCustomers(){
    	$customers = CustomerDetail::where('monthly_invoice_cycle', date('j'))
						->whereNotIn('customer_id', function($query){
							$query->select('customer_id')
							->from(with(new KallpodInvoice)->getTable())
							->whereDate('invoice_date', '=', Carbon::today()->toDateString());
						})->get();

		return $customers;
    }


    public static function generateInvoiceNumber(){
    	$lastInvoice = self::latest()->first();

    	// add 1 from the last invoice number id, set to 1 if no record yet.
    	$incrementedValue = ($lastInvoice) ? $lastInvoice->id+1 : 1;

    	$invoiceNumber = str_pad($incrementedValue, 6, "0", STR_PAD_LEFT);
    	$invoiceNumber = date('Ymd-') . $invoiceNumber;

		return $invoiceNumber;
    }
}
