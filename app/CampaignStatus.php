<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignStatus extends Model
{
    protected $table 	  = 'vicidial_campaign_statuses';
    protected $primaryKey = null;
    
    public $incrementing = false;
    public $timestamps = false;
}
