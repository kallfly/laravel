<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'vicidial_log';
    protected $primaryKey = 'uniqueid';

    public $incrementing = false;
    public $timestamps = false;





    public function campaign()
    {
    	return $this->belongsTo(\App\Campaign::class, 'campaign_id', 'campaign_id');
    }

}
