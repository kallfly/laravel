<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    public $incrementing = false;
    protected $primaryKey = 'extension';
    public $timestamps = false;
}
