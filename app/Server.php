<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $primaryKey = 'server_id';

    public $timestamps = false;
    public $incrementing = false;
}
