<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentCampaign extends Model
{
    protected $table = 'kf_agent_campaigns';

    public $timestamps = false;
}
