<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cdr extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'cdr';

    public $timestamps = false;

    protected $attributes = ['formatted_date' => ''];
    protected $appends = ['formatted_date'];





    public function getFormattedDateAttribute()
    {
    	return date("M j, Y H:i", strtotime($this->call_date));
    }

}
