<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $table = 'vicidial_user_groups';
    protected $primaryKey = 'user_group';
    public $incrementing = false;
    public $timestamps = false;


    public static function getAllowedCampaignIds()
    {
        $userGroup = auth()->user()->user_group;
        $allowables = self::select('allowed_campaigns')
        									->where('user_group', $userGroup)
        									->first();


        $allowedCampaigns = $allowables->allowed_campaigns;


        $campaignIds = array();

		if( (!preg_match("/-ALL/",$allowedCampaigns)) )
		{
			$strCampaignIds = preg_replace("/ -/",'',$allowedCampaigns);
			
			$campaignIds = explode(' ', $strCampaignIds);
			$campaignIds = array_filter($campaignIds);
		}else return array();


		return $campaignIds;
    }





    public static function addToAllowedCampaignIds($campaignId)
    {
        $campaignIds = self::getAllowedCampaignIds();

        if( $campaignIds!==false )
        {
            $campaignIds[] = $campaignId;
        }
        else
        {
            $campaignIds = array($campaignId);
        }

        $campaignIds[] = '-';
        $strCampaignIds = implode(' ', $campaignIds);

        return $strCampaignIds;
    }

}
