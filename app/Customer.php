<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Customer extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'kf_m_customers';

    public $timestamps = false;





    public static function addBalance($balance)
    {
    	$accountId = User::getAccountId();
    	$customer = self::where('account_id', $accountId)->first();

        if(!$customer)
        {
            $customer->balance += $balance;
            $customer->save();
        }
    }





    public static function getBalance()
    {
    	$accountId = User::getAccountId();
    	$customer = self::where('account_id', $accountId)->first();

    	return ($customer) ? $customer->balance : 0;
    }

}
