<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use \App\Phone;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'vicidial_users';

    public $timestamps = false;
    
    protected $primaryKey = 'user_id';

    protected $fillable = [
        'user', 
        'pass',
        'full_name',
        'user_level',
        'user_group',
        'phone_login',
        'phone_pass'
    ];

   protected $hidden = [
        'password', 
        'remember_token'
    ];


    public function  customerDetail()
    {
        return $this->belongsTo(CustomerDetail::class);
    }


    public function getAuthPassword()
    {
        return $this->pass;
    }


    public static function getAuthUser()
    {
        if(!auth()->check()) return false;

        $user = auth()->user();
        $arr  = explode('_', $user->user_group);
        $userGroupPrefix = $arr[0];

        return $userGroupPrefix;
    }





    public static function getUserGroup($type)
    {
        $authUser = self::getAuthUser();
        return $authUser . '_' . strtoupper($type);
    }





    public static function getAgentPrefix()
    {
        $authUser = self::getAuthUser();
        $user = self::where('user', 'like', $authUser)->first();

        $userId = str_pad($user->user_id, 4, '0', STR_PAD_LEFT);

        return $userId;
    }





    public function phones()
    {
        return $this->belongsTo(Phone::class, 'phone_login', 'extension')
                        ->withDefault(function(){
                            return new Phone;
                        });
    }





    public static function getAgents()
    {
        $agentGroup = User::getUserGroup('agent');
        return User::where('user_group', $agentGroup)
                                ->where('active', 'Y')
                                ->get();
    }





    public static function getAccountId()
    {
        $user = auth()->user();
        $accountId = str_pad($user->user_id, 6, '0', STR_PAD_LEFT);

        return $accountId;
    }
}
