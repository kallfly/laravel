<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentLog extends Model
{
    protected $table = 'vicidial_agent_log';
    protected $primaryKey = 'agent_log_id';

    public $timestamps = false;




    public static function getAgentCallsTotal($startDate, $endDate)
    {
    	$agentGroup = User::getUserGroup('agent');


    	$loggedInAgents = self::loggedInAgents($startDate, $endDate, $agentGroup);
		$data['loggedInAgents'] = count($loggedInAgents);


		$agents = array_column($loggedInAgents->toArray(), 'user');
		$calls = AgentLog::selectRaw('
							count(*) as total,
							COALESCE(sum(case when talk_sec > 0 then 1 else 0 end), 0) as answered,
							COALESCE(sum(case when talk_sec = 0 then 1 else 0 end), 0) as not_answered,
							COALESCE(sum(talk_sec), 0) as talk_sec
							')
					->where('status', '!=', '')
					->where('event_time', '>=', $startDate)
					->where('event_time', '<=', $endDate)
					->orderBy('event_time', 'desc')
					->whereIn('user', $agents)
					->first();

		$data['totalCalls'] = $calls->total;
		$data['answeredCalls'] = $calls->answered;
		$data['notAnsweredCalls'] = $calls->not_answered;
		$data['talkTime'] = gmdate("H:i:s", $calls->talk_sec);

		return $data;
    }





    public static function getAgentCalls($startDate, $endDate, $callType = 'all', $limit = false)
    {
    	$agentGroup = User::getUserGroup('agent');


    	$loggedInAgents = self::loggedInAgents($startDate, $endDate, $agentGroup);


		$agents = array_column($loggedInAgents->toArray(), 'user');
		$calls = \DB::table('vicidial_agent_log as val')
					->selectRaw('
							u.full_name,
							agent_log_id,
							c.campaign_name,
							val.user,
							event_time,
							val.lead_id,
							vl.phone_code,
							vl.phone_number,
							talk_epoch,
							talk_sec,
							val.status,
							val.user_group,
							val.comments,
							sub_status')
					->where('val.status', '!=', '')
					->where('event_time', '>=', $startDate)
					->where('event_time', '<=', $endDate)
					->whereIn('val.user', $agents)
					->orderBy('event_time', 'desc')
					->join('vicidial_list as vl', 'val.lead_id', 'vl.lead_id')
					->join('vicidial_users as u', 'val.user', 'u.user')
					->join('vicidial_campaigns as c', 'val.campaign_id', 'c.campaign_id');

		if($callType == 'yes') $calls->where('talk_sec', '>', 0);
		if($callType == 'no') $calls->where('talk_sec', '=', 0);
		if($limit) $calls->limit($limit);

		return $calls->get();
    }





    public static function loggedInAgents($startDate, $endDate, $agentGroup)
    {
    	return self::selectRaw('user, count(*) as total')
    				->where('status', '!=', '')
    				->where('event_time', '>=', $startDate)
    				->where('event_time', '<=', $endDate)
					->where('user_group', $agentGroup)
    				->groupBy('user')
    				->get();
    }

}
