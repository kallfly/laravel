<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVicidialCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vicidial_campaigns', function (Blueprint $table) {
            $table->string('campaign_id', 8)->primary();
            $table->string('campaign_name', 8)->nullable();
            $table->enum('active', ['Y', 'N'])->nullable();
            $table->string('dial_status_a', 6)->nullable();
            $table->string('dial_status_b', 6)->nullable();
            $table->string('dial_status_c', 6)->nullable();
            $table->string('dial_status_d', 6)->nullable();
            $table->string('dial_status_e', 6)->nullable();
            $table->string('lead_order', 30)->nullable();
            $table->string('park_ext', 10)->nullable();
            $table->string('park_file_name', 100)->nullable()->default('default');
            $table->text('web_form_address')->nullable();
            $table->enum('allow_closers', ['Y','N'])->nullable();
            $table->int('hopper_level', 8)->nullable()->unsigned()->default(1);
            $table->string('auto_dial_level', 6)->nullable()->default(0);
            $table->enum('next_agent_call', ['random','oldest_call_start','oldest_call_finish','campaign_rank','overall_user_level','fewest_calls','longest_wait_time'])->nullable()->default('longest_wait_time');
            $table->string('local_call_time', 10)->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vicidial_campaigns');
    }
}
