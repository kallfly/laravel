<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKallpodInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kallpod_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_number');
            $table->integer('customer_id');
            $table->integer('num_users');
            $table->integer('total_price');
            $table->dateTime('invoice_date');
            $table->boolean('paid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kallpod_invoices');
    }
}
