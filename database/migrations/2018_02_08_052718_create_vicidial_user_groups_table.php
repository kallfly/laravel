<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVicidialUserGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vicidial_user_groups', function (Blueprint $table) {
            $table->string('user_group', 20);
            $table->string('group_name', 40)->nullable();
            $table->text('allowed_campaigns')->nullable();
            $table->text('qc_allowed_campaigns')->nullable();
            $table->text('qc_allowed_inbound_groups')->nullable();
            $table->text('group_shifts')->nullable();
            $table->enum('forced_timeclock_login', ['Y','N','ADMIN_EXEMPT'])->nullable()->default('N');
            $table->enum('shift_enforcement', ['OFF','START','ALL'])->nullable()->default('OFF');
            $table->text('agent_status_viewable_groups')->nullable();
            $table->enum('agent_status_view_time', ['Y', 'N'])->nullable()->default('N');
            $table->enum('agent_call_log_view', ['Y', 'N'])->nullable()->default('N');
            $table->enum('agent_xfer_consultative', ['Y', 'N'])->nullable()->default('Y');
            $table->enum('agent_xfer_dial_override', ['Y', 'N'])->nullable()->default('Y');
            $table->enum('agent_xfer_vm_transfer', ['Y', 'N'])->nullable()->default('Y');
            $table->enum('agent_xfer_blind_transfer', ['Y', 'N'])->nullable()->default('Y');
            $table->enum('agent_xfer_dial_with_customer', ['Y', 'N'])->nullable()->default('Y');
            $table->enum('agent_xfer_park_customer_dial', ['Y', 'N'])->nullable()->default('Y');
            $table->enum('agent_fullscreen', ['Y', 'N'])->nullable()->default('N');
            $table->string('allowed_reports', 2000)->nullable()->default('ALL REPORTS');
            $table->string('webphone_url_override', 255)->nullable();
            $table->string('webphone_systemkey_override', 100)->nullable();
            $table->enum('webphone_dialpad_override', ['DISABLED','Y','N','TOGGLE','TOGGLE_OFF'])->nullable()->default('DISABLED');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vicidial_user_groups');
    }
}
