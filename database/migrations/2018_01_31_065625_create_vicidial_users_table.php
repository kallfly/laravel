<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVicidialUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vicidial_users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('user', 20);
            $table->text('pass');
            $table->string('full_name', 50)->nullable();
            $table->tinyInteger('user_level')->nullable()->default(1);
            $table->string('user_group', 20)->nullable();
            $table->string('phone_login', 20)->nullable();
            $table->string('phone_pass', 20)->nullable();
            $table->enum('delete_users', ['0','1'])->nullable()->default(0);
            $table->enum('delete_user_groups', ['0','1'])->nullable()->default(0);
            $table->enum('delete_lists', ['0','1'])->nullable()->default(0);
            $table->enum('delete_campaigns', ['0','1'])->nullable()->default(0);
            $table->enum('delete_ingroups', ['0','1'])->nullable()->default(0);
            $table->enum('delete_remote_agents', ['0','1'])->nullable()->default(0);
            $table->enum('load_leads', ['0','1'])->nullable()->default(0);
            $table->enum('campaign_detail', ['0','1'])->nullable()->default(0);
            $table->enum('ast_admin_access', ['0','1'])->nullable()->default(0);
            $table->enum('ast_delete_phones', ['0','1'])->nullable()->default(0);
            $table->enum('delete_scripts', ['0','1'])->nullable()->default(0);
            $table->enum('modify_leads', ['0','1'])->nullable()->default(0);
            $table->enum('hotkeys_active', ['0','1'])->nullable()->default(0);
            $table->enum('change_agent_campaign', ['0','1'])->nullable()->default(0);
            $table->enum('agent_choose_ingroups', ['0','1'])->nullable()->default('1');
            $table->text('closer_campaigns')->nullable();
            $table->enum('scheduled_callbacks', ['0','1'])->nullable()->default(0);
            $table->enum('agentonly_callbacks', ['0','1'])->nullable()->default(0);
            $table->enum('agentcall_manual', ['0','1'])->nullable()->default(0);
            $table->enum('vicidial_recording', ['0','1'])->nullable()->default('1');
            $table->enum('vicidial_transfers', ['0','1'])->nullable()->default('1');
            $table->enum('delete_filters', ['0','1'])->nullable()->default(0);
            $table->enum('alter_agent_interface_options', ['0','1'])->nullable()->default(0);
            $table->enum('closer_default_blended', ['0','1'])->nullable()->default(0);
            $table->enum('delete_call_times', ['0','1'])->nullable()->default(0);
            $table->enum('modify_call_times', ['0','1'])->nullable()->default(0);
            $table->enum('modify_users', ['0','1'])->nullable()->default(0);
            $table->enum('modify_campaigns', ['0','1'])->nullable()->default(0);
            $table->enum('modify_lists', ['0','1'])->nullable()->default(0);
            $table->enum('modify_scripts', ['0','1'])->nullable()->default(0);
            $table->enum('modify_filters', ['0','1'])->nullable()->default(0);
            $table->enum('modify_ingroups', ['0','1'])->nullable()->default(0);
            $table->enum('modify_usergroups', ['0','1'])->nullable()->default(0);
            $table->enum('modify_remoteagents', ['0','1'])->nullable()->default(0);
            $table->enum('modify_servers', ['0','1'])->nullable()->default(0);
            $table->enum('view_reports', ['0','1'])->nullable()->default(0);
            $table->enum('vicidial_recording_override', ['DISABLED','NEVER','ONDEMAND','ALLCALLS','ALLFORCE'])->nullable()->default('DISABLED');
            $table->enum('alter_custdata_override', ['NOT_ACTIVE','ALLOW_ALTER'])->nullable()->default('NOT_ACTIVE');
            $table->enum('qc_enabled', ['1','0'])->nullable()->default(0);
            $table->integer('qc_user_level')->nullable()->default(1);
            $table->enum('qc_pass', ['1','0'])->nullable()->default(0);
            $table->enum('qc_finish', ['1','0'])->nullable()->default(0);
            $table->enum('qc_commit', ['1','0'])->nullable()->default(0);
            $table->enum('add_timeclock_log', ['1','0'])->nullable()->default(0);
            $table->enum('modify_timeclock_log', ['1','0'])->nullable()->default(0);
            $table->enum('delete_timeclock_log', ['1','0'])->nullable()->default(0);
            $table->enum('alter_custphone_override', ['NOT_ACTIVE','ALLOW_ALTER'])->nullable()->default('NOT_ACTIVE');
            $table->enum('vdc_agent_api_access', ['0','1'])->nullable()->default(0);
            $table->enum('modify_inbound_dids', ['1','0'])->nullable()->default(0);
            $table->enum('delete_inbound_dids', ['1','0'])->nullable()->default(0);
            $table->enum('active', ['Y', 'N'])->nullable()->default('Y');
            $table->enum('alert_enabled', ['1', '0'])->nullable()->default(0);
            $table->enum('download_lists', ['1', '0'])->nullable()->default(0);
            $table->enum('agent_shift_enforcement_override', ['DISABLED','OFF','START','ALL'])->nullable()->default('DISABLED');
            $table->enum('manager_shift_enforcement_override', ['0','1'])->nullable()->default(0);
            $table->enum('shift_override_flag', ['0','1'])->nullable()->default(0);
            $table->enum('export_reports', ['1','0'])->nullable()->default(0);
            $table->enum('delete_from_dnc', ['0','1'])->nullable()->default(0);
            $table->text('email', 100)->nullable();
            $table->text('user_code', 100)->nullable();
            $table->text('territory', 100)->nullable();
            $table->enum('allow_alerts', ['0','1'])->nullable()->default(0);
            $table->enum('agent_choose_territories', ['0','1'])->nullable()->default('1');
            $table->text('custom_one', 100)->nullable();
            $table->text('custom_two', 100)->nullable();
            $table->text('custom_three', 100)->nullable();
            $table->text('custom_four', 100)->nullable();
            $table->text('custom_five', 100)->nullable();
            $table->string('voicemail_id', 10)->nullable();
            $table->enum('agent_call_log_view_override', ['DISABLED','Y','N'])->nullable()->default('DISABLED');
            $table->enum('callcard_admin', ['1','0'])->nullable()->default(0);
            $table->enum('agent_choose_blended', ['0','1'])->nullable()->default('1');
            $table->enum('realtime_block_user_info', ['0','1'])->nullable()->default(0);
            $table->enum('custom_fields_modify', ['0','1'])->nullable()->default(0);
            $table->enum('force_change_password', ['Y','N'])->nullable()->default('N');
            $table->enum('agent_lead_search_override', ['NOT_ACTIVE','ENABLED','DISABLED'])->nullable()->default('NOT_ACTIVE');
            
            
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vicidial_users');
    }
}
