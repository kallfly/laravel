
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/my-account', function () {
	$title = 'My Account';
    return view('profile', compact('title'));
});


Route::get('/', function () {
	return redirect('/home');
});





Route::get('/home', 'DashboardController@index');
Route::post('/dashboard/search', 'DashboardController@search')->name('dashboard');
Route::post('/dashboard/call-activities', 'DashboardController@callActivities');
Route::post('/dashboard/call-graph-data', 'DashboardController@callGraphData');
Route::post('/dashboard/realtime-agents', 'DashboardController@realtimeAgents');
Route::get('/dashboard/status-count/{status}', 'DashboardController@statusCount');




// SESSION HANDLER CONTROLLER ROUTES
Route::get('/login', 'SessionHandlerController@login')->name('login');
Route::post('/login', 'SessionHandlerController@attempt');
Route::get('/logout', 'SessionHandlerController@logout');





// USER CONTROLLER ROUTES
Route::get('/user/admin/{user}/edit', 'UserController@adminEdit');
Route::get('/user/admin', 'UserController@admin');

Route::get('/user/agent', 'UserController@agent');
Route::get('/user/agent/{user}/edit', 'UserController@agentEdit');

Route::post('/user', 'UserController@store');
Route::patch('/user/{user}/edit', 'UserController@update');
Route::get('/user/autocomplete', 'UserController@autocomplete');




// KALLPOD INVOICE
Route::get('/invoice', 'KallpodInvoiceController@index');
Route::get('/invoice/generate', 'KallpodInvoiceController@generate');





// VICIDIAL CAMPAIGN CONTROLLER ROUTES
Route::get('/campaigns', 'CampaignController@index');
Route::post('/campaigns', 'CampaignController@store');
Route::get('/campaigns/{campaign}/edit', 'CampaignController@edit');
Route::patch('/campaigns/{campaign}', 'CampaignController@update');

Route::post('/campaigns/{campaign}/add-status', 'CampaignController@addStatus');
Route::delete('/campaigns/{campaign}/delete-status', 'CampaignController@deleteStatus');





// LIST CONTROLLER ROUTES
Route::get('/list/{campaign}/ajax/dialables', 'ListAjaxController@dialables');
Route::get('/list/{listsData}/ajax/total-dialables', 'ListAjaxController@totalDialables');
Route::get('/list/{listsData}/ajax/total-count', 'ListAjaxController@totalCount');
Route::get('/list/{listsData}/ajax/lead-list', 'ListAjaxController@leadList');
Route::get('/list/{lead}/ajax/call-recordings', 'ListAjaxController@getLeadRecordings');
Route::get('/list/{lead}/ajax/logs', 'ListAjaxController@getLogs');

Route::post('/list/{listsData}/reset', 'ListController@reset');
Route::get('/list/{listsData}/download-lead-summary', 'ListController@downloadLeadSummary');
Route::get('/list/{listsData}/edit', 'ListController@edit');
Route::patch('/list/{listsData}', 'ListController@update');
Route::get('/list', 'ListController@index');
Route::post('/list', 'ListController@store');






// LIST CONTROLLER ROUTES
Route::get('/leads/search', 'LeadsController@search');
Route::get('/leads/search/ajax', 'LeadsController@searchAjax');
Route::get('/leads/{lead}', 'LeadsController@view');
Route::patch('/leads/{lead}', 'LeadsController@update');





Route::get('/leads-loader', 'LeadsLoaderController@index');
Route::post('/leads-loader/upload', 'LeadsLoaderController@upload');
Route::post('/leads-loader/get-file-columns', 'LeadsLoaderController@getFileColumns');
Route::post('/leads-loader/process-leads', 'LeadsLoaderController@processLeads');





// AGENT HOURS CONTROLLER ROUTES
Route::get('/reports', 'ReportsController@index');
Route::post('/agent-hours/ajax', 'AgentHoursController@ajax');
Route::post('/agent-call-report/ajax', 'AgentCallReportController@ajax');
Route::post('/agent-call-report/ajax-graph', 'AgentCallReportController@ajaxGraph');
Route::post('/call-recordings/ajax', 'CallRecordingsController@ajax');
Route::post('/performance/ajax-average', 'PerformanceController@ajaxAverage');
Route::post('/performance/ajax', 'PerformanceController@ajax');
Route::post('/disposition/ajax', 'DispositionController@ajax');





// TOP UP CONTROLLER ROUTES
Route::get('/top-up', 'TopUpController@index');
Route::get('/top-up/print/{topUpHistory}', 'TopUpController@print');
Route::post('/top-up/paypal', 'PaypalController@topUp');
Route::get('/top-up/paypal/payment-status', 'PaypalController@paymentStatus');
Route::post('/top-up/stripe', 'StripeController@topUp');





// TIMEZONE CONTROLLER
Route::post('/timezone/update', 'TimezoneController@update');





// CDR CONTROLLER
Route::get('/consumption-report', 'CdrController@index');
Route::get('/cdr/ajax-report', 'CdrController@ajaxReport');




// Route::get('/customer-detail', 'CustomerDetailController@index');
// Route::get('/customer-detail/{customer}/edit', 'CustomerDetailController@edit');
// Route::post('/customer-detail', 'CustomerDetailController@store');
// Route::patch('/customer-detail/{customer}', 'CustomerDetailController@update');
// Route::delete('/customer-detail/{customer}', 'CustomerDetailController@delete');